# RELEASE NOTES

This document provides an overview of changes, updates, and improvements for the Guideline Repository. Each release includes enhancements to documentation, automation workflows, and operational best practices.

---

## Version History

### [1.0.0] - 2024-12-09
#### Added
- Comprehensive `HOWTO` guide for preparing weekly meetings with the Service Delivery Manager:
  - Focuses on reviewing KPIs, SOP updates, and iteration workload distribution.
- Detailed test plans for infrastructure changes, including as example:
  - Pexip conference calls.
  - BGP feature validation with Node-RED and Ansible.
  - Load Balancer, DNS, and Firewall configurations.
- Automation KPIs review, tracking:
  - Number of Ansible playbooks, roles, and collections stored in GitLab vs. legacy systems.
  - CI/CD pipeline adoption for automation efforts.
- `HOWTO` guide for setting up Visual Studio Code for repository management:
  - Includes proxy configuration for GitLab.
  - Explains Git commands and basic GitLab CI/CD concepts.
- Mermaid diagrams for:
  - Organizational structure of teams.
  - Software client-to-service request workflows.
- Initial guidelines for:
  - Change Management.
  - Event Management.
  - Incident Management.
  - Request Fulfillment.
- Basic `HOWTO` templates for creating RFCs and test plans.

#### Changed
- Enhanced `README.md` templates for clear onboarding and usability.
- Improved Mermaid diagrams for visualizing workflows and organizational structures.

#### Fixed
- Corrected typos and inconsistencies in existing `HOWTO` guides, including:
  - RFC creation steps.
  - Documentation standards and styles.

