Defining collaboration and culture in an organization requires addressing critical questions that clarify values, behaviors, expectations, and systems that guide how employees work together. Here are the key questions to consider:

### **Collaboration and culture:**

1. **Cross-Functional Teamwork**:
   - Encourage open communication and collaboration among all squad members to leverage diverse skills.
   - Promote collective ownership of tasks and shared responsibility for delivering the sprint goal.

2. **Transparency**:
   - Maintain transparency about progress, challenges, and priorities through tools like the Scrum Board and daily stand-ups.
   - Share information openly to ensure all team members have a clear understanding of the project's status.

3. **Respect Squad Autonomy**:
   - Avoid bypassing the squad structure or assigning tasks directly to members outside their squads.
   - Empower squads to self-organize and make decisions collaboratively.

4. **Effective Communication**:
   - Use Scrum ceremonies (for example, sprint planning, stand-ups, sprint reviews, and retrospectives) to foster structured, frequent communication.
   - Listen actively to all team members and stakeholders to address issues collaboratively.

5. **Commitment to Roles**:
   - Respect the boundaries and responsibilities of each Scrum role (Product Owner, Proxy Product Owner, Scrum Master, Squad Team).
   - Facilitate collaboration between roles without overstepping responsibilities.

6. **Alignment with the Sprint Goal**:
   - Focus team efforts on the sprint goal and ensure that all tasks contribute to its achievement.
   - Collaborate to manage scope changes effectively, ensuring alignment with agreed priorities.

7. **Continuous Improvement**:
   - Use retrospectives to collaborate on identifying and resolving process inefficiencies.
   - Encourage feedback loops and iterative improvements in team collaboration and communication.

8. **Stakeholder Collaboration**:
   - Foster regular engagement between the Product Owner, Proxy Product Owner, and the squad to align priorities and gather feedback.
   - Ensure stakeholders respect the sprint boundaries and avoid mid-sprint disruptions.

9. **Adaptability and Empathy**:
   - Be open to suggestions and flexible in accommodating changes as long as they align with Scrum values.
   - Respect diverse perspectives and work styles to build a culture of trust and collaboration.

10. **Decision-Making:**
   - Local decision must be taken the squad autonomously
   - Cross Squad decision must be validated by the design committee
