# NMS III Documentation Overview

This repository contains essential documentation related to the **Network Managed Services III (NMS III)** project for the European Commission. The content includes the official call for tender and the corresponding strategy responses for technical and compliancy services, submitted by the vendor. Below is an overview of the documents:

---
## **1. Collaboration and culture**
### File: [`Collaboration_and_culture.md`](Collaboration_and_culture.md)
- **Description:** 
  - Cross-Functional Teamwork.
  - Transparency.
  - Respect Squad Autonomy.
  - Effective Communication
  - Commitment to Roles
  - Alignment with the Sprint Goal
  - Continuous Improvement
  - Stakeholder Collaboration
  - Adaptability and Empathy
  - Decision-Making

---
## **2. Onboarding meetings**
### File: [`Collaboration_and_culture.md`](Onboarding_meetings.md)
- **Description:** 
  - As part of the onboarding process, newcomers engage with key stakeholders to understand organizational roles, strategies, and workflows
- **Key Highlights:**
  - **Service Delivery Manager**: Overview of the organization, objectives, SLAs, and automation frameworks.  
  - **Lead Architect**: Insights into architectural strategy, technology roadmap, and governance.  
  - **Operation Quality Manager**: Operational workflows, tools, and Level 1 team structure.  
  - **Compliance Manager**: Compliance objectives, integration strategies, and key performance metrics.  

---
## **3. Call for Tender**
### File: [`T1.8.2_Technical_Specifications.pdf`](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII/Shared%20Documents/General/CFT%20v7%20-%20Tendering%20specifications/1.8.2%20Technical%20Specifications.pdf?csf=1&web=1&e=SZMTAa) and [`CallForTender.md`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/blob/main/guidelines/01_Welcome/CallForTender.md)
- **Description:** 
  - The official call for tender outlining the scope, objectives, and requirements for NMS III.
  - Details the expected services, including network operations, telephony, videoconferencing, and security.
  - Emphasizes metrics, automation, and compliance standards for service delivery.

- **Key Highlights:**
  - Core network services (backbone, Wi-Fi, cloud connectivity).
  - Telephony and videoconferencing solutions aligned with modern communication standards.
  - Security services ensuring compliance and robustness against threats.
  - Metrics and KPIs for evaluating vendor performance.

---

## **4. Technical Services Strategy**
### File: [`T1.1_Technical_Services_Strategy.pdf`](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII/Shared%20Documents/General/NTX%20consortium%20offer/T1.1.Technical%20Services%20-%20Strategy.pdf?csf=1&web=1&e=9fiz1d)  and [`TechnicalStrategyOffer.md`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/blob/main/guidelines/01_Welcome/TechnicalStrategyOffer.md)
- **Description:**
  - A detailed vendor response addressing the technical strategy for NMS III.
  - Describes the methodologies, frameworks, and tools to meet the requirements of the tender.
  - Integrates ITIL 4 principles, Agile practices, and DevOps for efficient service delivery.

- **Key Highlights:**
  - Vision for infrastructure-as-code, CI/CD pipelines, and modular design.
  - Organizational structure promoting collaboration and innovation.
  - Metrics and performance monitoring for sustained service quality.
  - Emphasis on automation, training, and continual service improvement.

---

## **5. Compliancy Services Strategy**
### File: [`T2.1_Compliancy_Services_Strategy.pdf`](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII/Shared%20Documents/General/NTX%20consortium%20offer/T2.1.Compliancy%20Services%20-%20Strategy.pdf?csf=1&web=1&e=IbV3yY) and [`CompliancyStrategyOffer.md`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/blob/main/guidelines/01_Welcome/CompliancyStrategyOffer.md)
- **Description:**
  - Vendor response detailing the approach for ensuring compliance with NMS III requirements.
  - Focuses on achieving process maturity through structured frameworks and continuous improvements.
  - Aligns with data protection regulations and ISO standards.

- **Key Highlights:**
  - Centralized Configuration Management System (CMS) for compliance and security.
  - Automated workflows for deployment, request fulfillment, and lifecycle management.
  - Workforce development through training, knowledge sharing, and innovation.
  - Business continuity plans and data protection strategies.

---
## **6. Introduction to GIT and CI/CD**
### File: [`git_basics.md`](git_basics.md) and [`gitlab_ci_cd_basics.md`](gitlab_ci_cd_basics.md)
- **Description:**
  - an introduction to git command useful to manage the content of your repository.
  - an introduction to gitlab CI/CD concept to understand of the repository content can be tested and deployed.

- **Key Highlights:**
  - git command basic options.
  - CI/CD description file and stages. 

---
## **7. Introduction to setup Visual Studio Code**
### File: [`vscode_setup.md`](vscode_setup.md) 
- **Description:**
  - an introduction to configure Visual Studio Code with proxy.
  

## **How to Use This Repository**
1. **Understand the Requirements:** Begin with the call for tender to grasp the objectives and service expectations of NMS III.
2. **Explore NTX Strategies:** Dive into the technical and compliancy strategies to learn how NTX plans to meet these requirements.
3. **Compare and Analyze:** Use the documents to evaluate the alignment between the tender and the proposed solutions.