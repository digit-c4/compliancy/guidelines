# Onboarding Process for Newcomers

As part of the onboarding process, newcomers will meet key individuals within the organization to gain a comprehensive understanding of roles, responsibilities, and operational guidelines. Below are the key stakeholders and the topics they will present:

## **1. Service Delivery Manager**
The Service Delivery Manager will provide an overview of:
- **Customer and NTX Organization**: Introduction to the customer landscape and NTX structure.
- **High-Level Objectives and KPIs**: Key performance indicators and strategic objectives.
- **Service Level Agreements (SLA)**: Overview of service commitments and expectations.
- **Automation with the Blueprint**: High-level introduction to automation initiatives and frameworks.
- **Guidelines Repository**: Location and usage of organizational guidelines.
- **Weekly Meetings and Monthly Reporting**: Processes and schedules for recurring meetings and reporting activities.
- **Squad interaction**: Services proposed per squad

## **2. Lead Architect**
The Lead Architect will deep-dive into:
- **Architectural vision and Strategy**:Explain how architecture aligns with the organization's business and operational goals.
- **Technology Roadmap**: Share the long-term vision for technology adoption, innovation, and scalability.
- **Key Principles**: Highlight core principles such as modularity, reusability, security, and performance.
- **Automation Blueprint Details**: Technical insights into the organization's automation strategies and frameworks. Key Components: Detail the main components of the blueprint, such as orchestration layers, CI/CD pipelines, and tooling integrations.
- **Collaboration and Governance**: Interdisciplinary Collaboration: Explain how the architecture team works with delivery, operations, and compliance squads. Governance Frameworks: Discuss how architecture decisions are made, reviewed, and communicated.


## **3. Operation Quality Manager**
The Operation Quality Manager will explain: 
- **Operational Processes**: Standardized workflows and operational guidelines.
- **Tooling**: Overview of tools and platforms used to support operations.
- **Level 1 Organization**: Structure and responsibilities of the Level 1 support team.

## **4. Compliance Manager**
The Compliance Manager will cover:
- **Compliance Objectives**: Strategic goals related to compliance and regulatory adherence.
- **Compliance by Design**: Methodologies to integrate compliance into services from inception.
- **Key Performance Indicators (KPIs)**: Metrics to track compliance performance and improvements.
- **MoSCow approach**: integration of compliancy by Design key parameters in requirements.
