# Git Basics for Newbies

Welcome to Git. This document provides a brief introduction to the most commonly used Git commands and concepts. Don’t worry—you’ll be comfortable in no time.

---

## Key Concepts

### What is Git?
Git is a version control system that helps you track changes in code and collaborate with others.

### Repository
A "repo" is like a folder for your project. It contains your files and the history of changes made to them.

### Branch
A branch is like a parallel universe for your code. Use branches to work on features or fixes without affecting the main code.

---

## Common Commands

### 1. Clone a Repository
To copy a repository from GitLab to your local machine:
```bash
git clone <repository_url>
```

### 2. Check the Status
See what has changed in your working directory:
```bash
git status
```

### 3. Add Files
Stage files to be included in the next commit:
```bash
git add <file_name>
# Add all changes:
git add .
```

### 4. Commit Changes
Save your changes with a meaningful message:
```bash
git commit -m "Your commit message here"
```

### 5. Push Changes
Send your changes to the remote repository:
```bash
git push origin <branch_name>
```

### 6. Pull Updates
Update your local repository with changes from the remote:
```bash
git pull
```

---

## Best Practices
- Write clear commit messages.
- Use meaningful branch names (for example, `feature/add-login`, `bugfix/fix-typo`).
- Sync regularly with the main branch to avoid conflicts.

---

For more details, check the [Git documentation](https://git-scm.com/doc).