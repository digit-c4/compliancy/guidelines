### **Roles and Responsibilities in NMS III**

1. **Service Delivery Manager (SDM)**:  
   The SDM holds overall leadership for the services and serves as the primary point of contact between the Commission and the contractor for the daily provision of NMS III services. The SDM ensures the contract's implementation and execution in compliance with the applicable SLA.

2. **Lead Architect**:  
   Responsible for proposing strategies and maintaining the architectural foundation of the services provided. This includes ensuring coherence, evolution, and managing the architects. The contractor must assign experienced and technically skilled personnel to this role to ensure high-quality team deliveries.

3. **Operation Quality Manager**:  
   Oversees overall service operations, acts as the incident manager, and ensures the expected quality of service as defined by the NMT.

4. **Proxy Product Owner**:  
   Responsible for roadmap preparation with DIGIT C4 PO and the execution of service strategies and ensuring the quality of service delivery.

5. **Program Manager**:  
   For projects related to designing or reviewing services, the contractor provides the number of project managers specified in **M3HLP1 of Annex 6 - Financial Evaluation of the Tender Specifications**. For transition or operational projects, the contractor proposes the required number of project managers based on domain expertise, activity volume, compliance requirements, and team availability.

6. **Compliance Manager**:  
   Leads the compliance team (ISMS, BCMS, and SMS) to ensure that the NMS III organization’s services achieve the expected maturity levels.

7. **Software Architect**:  
   Designs and oversees software-related solutions for the services provided.

8. **NNDOE (Network/Network Security DevOps Engineer)**:  
   Responsible for automating tasks using Ansible and maintaining technical services.

9. **Software Engineer**:  
   For projects defined by the NMT during the execution of basic services, the contractor provides the number of software engineers specified in **M3HLP2 of Annex 6 - Financial Evaluation of the Tender Specifications**.

10. **NOCA (Network Operations Center Agent)**:  
    Provides service desk, network monitoring, management, and control services.  
    - Acts as a proxy between service users within the NMS III scope and the NMS III organization.  
    - Serves as the point of entry for support calls related to all NMS III services.  
    - Operates closely with local service desks in Directorates, the Digital Workplace Services service desk, the Data Centre Services service desk, the Central Helpdesk, and similar entities.  
    - Oversees complex environments operated by the NMS III contractor.

