## Organisation
- [Board Overview](#board-overview)
- [Service Delivery Manager Teams](#service-delivery-manager-teams)
- [Operation Quality Manager Teams](#operation-quality-manager-teams)
- [Lead Architect Teams](#lead-architect-teams)


### 1. board overview
```mermaid
graph TD
    Board["Board"]
    
    Board --> SDM["Service Delivery Manager"]
    Board --> OQM["Operation Quality Manager"]
    Board --> LA["Lead Architect"]
```

### 2. squad overview
```mermaid
graph TD
    %% Service Delivery Manager Branch
    SDM --> TC_DC["Proxy Product Owner - TC-DC"]
    SDM --> RPS["Proxy Product Owner - RPS"]
    SDM --> RAS["Proxy Product Owner - RAS"]
    SDM --> FW["Proxy Product Owner - FW"]
    SDM --> LB_DNS_PROXY["Proxy Product Owner - LB-DNS-PROXY"]
    SDM --> WORKPLACE["Proxy Product Owner - WORKPLACE"]
    SDM --> DEV["Proxy Product Owner - DEV"]
    SDM --> CMDB["Proxy Product Owner - CMDB"]
    SDM --> VC["Proxy Product Owner - VC"]
    SDM --> TELEPHONY["Proxy Product Owner - TELEPHONY"]
```
### 3. squad zoom
```mermaid
graph TD
    %% Squads under Product Owners
    TC_DC --> TC_DC_Squad["Squad (Architect + NNDOE)"]
    RPS --> RPS_Squad["Squad (Architect + NNDOE)"]
    RAS --> RAS_Squad["Squad (Architect + NNDOE)"]
    FW --> FW_Squad["Squad (Architect + NNDOE)"]
    LB_DNS_PROXY --> LB_DNS_PROXY_Squad["Squad (Architect + NNDOE)"]
    WORKPLACE --> WORKPLACE_Squad["Squad (Architect + NNDOE)"]
    DEV --> DEV_Squad["Squad (Architect + NNDOE)"]
    CMDB --> CMDB_Squad["Squad (Architect + NNDOE)"]
    VC --> VC_Squad["Squad (Architect + NNDOE)"]
    TELEPHONY --> TELEPHONY_Squad["Squad (Architect + NNDOE)"]
```
### 4. network operating center agent Overview
```mermaid
graph TD
    %% Operation Quality Manager Branch
    OQM --> NOC["Network Operating Center Squad"]
    NOC --> NOC_Agents["Network Operating Center Agents"]

```
### 4. architecture overview

```mermaid
graph TD
    %% Lead Architect Branch
    LA --> Architects["All Architects in Squads"]
```
