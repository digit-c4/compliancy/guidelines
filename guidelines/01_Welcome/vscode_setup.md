
---

## HOW TO: Set Up Visual Studio Code to Manage Repository Content Behind a Proxy

Here is a detailed **HOW TO** guide for setting up Visual Studio Code (VS Code) to manage content in a repository when working behind a proxy:

This guide helps you configure Visual Studio Code (VS Code) to work with repositories in an environment where proxy settings are required to access GitLab.

---

### Prerequisites

1. **Install Visual Studio Code (VS Code):**
   Download and install VS Code from `1-DWP EC Store`, search for it in the windows search bar.

2. **Install Git:**
   Ensure Git is installed and available in your system. Download it from `1-DWP EC Store`.

3. **Access to Proxy Settings:**
   Confirm the proxy server’s address, port, and credentials (if required). You can ask help to the squad "Proxy" or architects. Depending on you zone and your environment the proxy address may vary.

---

### Step 1: Configure Proxy for Git

To go further you have to comply all the requirements first.

1. Launch VS Code and open a terminal and configure the proxy settings for Git:

   ```bash
   git config --global http.proxy http://<proxy_user>:<proxy_password>@proxy:8012
   git config --global https.proxy http://<proxy_user>:<proxy_password>@proxy:8012
   ```

   Replace:

   - `<proxy_user>`: Your proxy username (if applicable).
   - `<proxy_password>`: Your proxy password (if applicable).

   Control that the command is successful by entering

   ```bash
   git config --global --get http.proxy
   ```

   and then

   ```bash
   git config --global --get https.proxy
   ```

   the result of each command should display what you enter previously.

2. Control that the configuration is properly set. Always using VS Code, open the file `.gitconfig` (located in `c:\Users\<User_ID>`if you are a Windows user):
   the file must contain something like:

   ```bash
      "..."
      [https]
         proxy = "http://<proxy_user>:<proxy_password>@proxy:8012"
      [http]
         proxy = "http://<proxy_user>:<proxy_password>@proxy:8012"
      [credential "http://proxy:8012"]
         provider = generic
      "..."
   ```

3. Check also the effectiveness of the configuration by trying to clone a repository
   Go in Gitlab and Choose you repository "in which" you want to work

   Use the button "Code" and select the URL of the repository with `Clone with HTTPS` to copy the URL (see screenshot):

   ![Repository_clone_from_GitLab](./Static_images/Repository_clone_from_GitLab.png)

   In VS Code, open a terminal and use the following command line

   ```bash
   git clone <past here the URL you have copied just before>
   ```

   Note, the URL is something like `https://domain_name/repository_name/project/sub_folder/.../name.git`
   If successful (no error message), Git is properly configured to work through the proxy.
   *If not, you shall check misspells and controls that you have well replaced `proxy_user` by your user ID, `proxy_password` by your internet password and `proxy` by the real proxy address.*

---

### Step 2: Configure Proxy for VS Code Extensions

1. Open **Settings** in VS Code:

   - Go to `File` > `Preferences` > `Settings` (Windows/Linux) or `Code` > `Preferences` > `Settings` (Mac).

     ![Configuration_Panel_VSCODE](./Static_images/Configuration_Panel_VSCODE.png)

2. Search for "Proxy" in the settings search bar.

3. Set the `http.proxy` configuration:

   - Click the pencil icon next to **`http.proxy`** and enter your proxy URL:

     ```bash
     http://<proxy_user>:<proxy_password>@proxy:8012
     ```
     
     ![Proxy_Section_Conf_Panel_VSCODE](./Static_images/Proxy_Section_Conf_Panel_VSCODE.png)

4. Save your settings (or close the configuration panel).

---

### Step 3: Install and Configure Extensions

1. **Install GitLens Extension:**

   - Open the Extensions view in VS Code (`Ctrl+Shift+X` or `Cmd+Shift+X`).
   - Search for "GitLens" and click **Install**.

   `Note:`*If this step doesn't work (none extension displayed or falure to download the extension), that means that you proxy is not properly configured in VS Code, in this case try to check the step 2 or relaunch VS Code.*
2. **Install Other Useful Extensions:**

   - Markdown Editor (for example, `Markdown All in One`).
   - YAML Validator (for example, `YAML` by Red Hat).
   - Docker and Ansible extensions (if applicable).

`Note`: *on your windows PC extension will partially work, binaries are not for installation... Simulate you CI/CD will not be possible on Windows Welcome PC.*

---

## Step 4: Set Up access token from GitLab

1. **gitlab repository (web portal)**

   **>>> BE CAREFUL DO NOT CLOSE THIS GITLAB WINDOW UNTIL THE END OF THIS WHOLE STEP <<<**

   - Open the user configuration menu in GitLab and use on `Edit Profile`:

   ![GitLab_Edit_Profile_Menu](./Static_images/GitLab_Edit_Profile_Menu.png)

   - In the menu displayed on the right click on `Access token`
   - The panel of "Personal access tokens appears" displaying the list of tokens already generated (if it your first time the list is empty)
   - Use the button `Add new token` on the top of this list and complete the form
   - Name the new token with relevant and self-standing name and check the option `api`

   ![GitLab_Create_Token](./Static_images/GitLab_Create_Token.png)

   - And so validate with `Create Personal Access token` button

   ![GitLab_Access_Token_Created](./Static_images/GitLab_Access_Token_Created.png)

2. **Configure your GIT environment**

   - Now, open a terminal in VS Code and use the command line `git config --global credential.helper manager`
   - When you press "enter" nothing happens, this is normal

   **>>> Then follow carefully the next instructions <<<**

A) Open an empty text file in VS Code (menu file > new text file) and in this text file put inside:

```bash
protocol=https
host=<The domaine name of the repository on which you generated the token> it can be sdlc. <or> code_europa <or> "the one on which you generated the token"
username=< your User_ID of gitlab >
password=< the value of the access token you have to copy/paste from GitLab >
```

B) launch the following command `git credential-manager store`, an empty line appear

C) Copy the 4 lines of your text file and right-click on the empty line to paste them

D) Press "enter" 2 times, if no error is displayed this step is finished, else you should check that you have no mistakes in the previous actions


`HINT:If you change the token you can reuse this procedure to update it.`

`Note`:*If you forgot to change it manually and so the token configure is no more valid, this popup will appear requesting a new value, so you will have to regenerate one:*

 ![GIT_Renew_Credential](./Static_images/GIT_Renew_Credential.png)

---

### Step 5: Set Up the Repository in VS Code

1. **Open Repository in VS Code:**

   - Open VS Code and click on `File > Open Folder`.
   - Select the folder where your repository is located.

2. **Initialize Git Integration:**

   - Open the Source Control view (`Ctrl+Shift+G` or `Cmd+Shift+G`).
   - VS Code will detect the repository and show Git operations (for example, commit, push, pull).

---

### Step 6: Configure Environment Variables for Proxy in Linux environment

1. **Set Proxy Variables:**
   In a terminal, export proxy variables for the current session:

   ```bash
   export http_proxy=http://<proxy_user>:<proxy_password>@proxy:8012
   export https_proxy=http://<proxy_user>:<proxy_password>@proxy:8012
   ```

2. **Make Proxy Settings Persistent:**
   Add the proxy variables to your shell configuration file (for example, `.bashrc`, `.zshrc`):

   ```bash
   echo 'export http_proxy=http://<proxy_user>:<proxy_password>@proxy:8012' >> ~/.bashrc
   echo 'export https_proxy=http://<proxy_user>:<proxy_password>@proxy:8012 >> ~/.bashrc
   source ~/.bashrc
   ```

3. **Verify Proxy Variables:**
   Check if the proxy settings are applied:

   ```bash
   echo $http_proxy
   echo $https_proxy
   ```

---

### Step 7: Troubleshooting

1. **Proxy Authentication Errors:**If your proxy requires credentials but they aren't working, check for special characters in your username or password and URL-encode them. For example:

   - Replace `@` with `%40`, `:` with `%3A`, etc.

2. **Timeout Issues:**
   If operations hang, check your proxy server connectivity and credentials.

3. **VS Code Extension Issues:**
   Some extensions may not respect the proxy settings. Use environment variables as a fallback.

---

### Step 8: Test and Verify

- Clone a GitLab repository in VS Code.
- Open the repository and make a small change.
- Commit the change using the Source Control view.
- Push the change to the remote repository.

If these steps succeed, your setup is complete.
