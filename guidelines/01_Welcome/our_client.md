

The Directorate-General for Digital Services (DG DIGIT) is the European Commission department responsible for providing digital services that support other Commission departments, EU institutions, and public administrations in EU member countries. Formerly known as the Directorate-General for Informatics, it was renamed to DG DIGIT in November 2023 to better reflect its role in driving the Commission's digital transformation. 

DG DIGIT is structured into several directorates, each focusing on specific aspects of digital services:

- **Directorate A: Digital Transformation**
- **Directorate B: Digital Enablers & Innovation**
- **Directorate C: Digital Workplace & Infrastructure**
- **Directorate R: Change Management & Resources**
- **Directorate S: Cybersecurity**

Directorate C, "Digital Workplace & Infrastructure," is further divided into units responsible for various facets of the Commission's digital infrastructure. One of these units is DIGIT C.4, "Network & Telecommunications." This unit provides the Commission with secure, reliable, and high-performance corporate network and telecommunication infrastructures and services. Its responsibilities include:

- **Data Network Services**: Supporting internal and external data communications.
- **Data Network Security Services**: Managing secure access to web applications, teleworking remote access services, firewall services, and more.
- **Telephony Services**: Overseeing fixed and mobile telephony services.
- **IP TV and Videoconference Services**: Providing infrastructure for internal broadcasting and virtual meetings.

These services are essential for the daily operations of the European Commission, ensuring seamless communication and collaboration across departments and with external partners. 

