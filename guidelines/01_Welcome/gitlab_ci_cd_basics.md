# gitLab ci/cd basics for newbies

GitLab ci/cd automates testing, building, and deploying your code. This guide introduces you to the basics so you can start using it confidently.

---

## What's ci/cd?

- **CI Continuous Integration:** Automatically tests and integrates code changes.
- **CD Continuous Deployment/Delivery:** Automatically deploys your app to an environment.

---

## `.gitlab-ci.yml`

The heart of GitLab ci/cd is the `.gitlab-ci.yml` file. This file defines the pipelines and jobs.

### Basic example
```yaml
stages:
  - test
  - deploy

test_job:
  stage: test
  script:
    - echo "Running tests..."
    - ./run-tests.sh

deploy_job:
  stage: deploy
  script:
    - echo "Deploying application..."
    - ./deploy.sh
```

---

## Common ci/cd concepts

### 1. pipeline
A pipeline is a set of jobs that run in stages.

### 2. job
A job is a task that runs in the pipeline. Example: running tests or deploying code.

### 3. stages
Stages organize jobs. Jobs in the same stage run in parallel.

---

## Running a pipeline

1. Push changes to the repository.
2. GitLab automatically starts the pipeline if `.gitlab-ci.yml` exists.
3. Check the **Pipelines** section in GitLab to view progress.

---

## Tips for beginners
- Start with small pipelines, for example, one test job.
- Use GitLab’s ci/cd editor to validate `.gitlab-ci.yml`.
- Review pipeline logs to debug issues.

---

For more details, visit the [GitLab ci/cd documentation](https://docs.gitlab.com/ee/ci/).