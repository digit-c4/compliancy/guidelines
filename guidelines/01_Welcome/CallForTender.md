### **Updated Summary: Technical Specifications for Network Managed Services III (NMS III)**

#### **1. Scope of NMS III Services**
   - **Operational Processes:** Any activities required to perform day to day operations are delivered under ITIL Processes best practices.
   - **Design:** Structured processes to design new or review existing services are also used.
   - **Transition:** Any transformation must be deployed carefully to minimize impact, by following ITIL practices.
   - **DEVOPS:** Development best practices are also used to run operations.

#### **2. Key Network Services**
   - **Backbone Network Services:** Core MPLS-based connectivity linking EC sites, offering Layer 2 and Layer 3 Virtual Private Networks.
   - **Building Network Services:** Internal network connectivity for EC buildings, including access and distribution switches.
   - **Wired Network Access Services:** End-user device connectivity through structured cabling and managed switches.
   - **Wi-Fi Network Access Services:** Wireless connectivity with profiles for guests, corporate users, and internal workplace access.
   - **LSU Network Services:** Specialized connectivity for server rooms within EC office buildings.
   - **Data Centre Network Services:** Manages modern (ACI-based) and legacy data centre networks, emphasizing dual connectivity for reliability.
   - **Telecom Centre Network Services:** Ensures EC network interconnections with external systems (for example, Internet, European Networks) and robust perimeter security.
   - **Cloud Connectivity Services:** Manages hybrid network infrastructure connecting EC networks with public/private cloud services.

#### **3. Telephony Services**
   - **Service Description:** Transitioning from analog to IP telephony using Cisco Call Manager (CUCM), with plans to integrate Microsoft Teams as a replacement for Skype for Business.
   - **Core Components:** CUCM, CUBE for PSTN integration, analog gateways for legacy support, and pilot use of Flexcom for monitoring and reporting.
   - **Metrics:** Manages over 5,000 IP phones and 10,000 analog devices, aiming to enhance provisioning through automation.
   - **Integration:** Supports emergency phones, VIP devices, and connects with external collaboration platforms like Teams and Skype.

#### **4. Videoconference Services**
   - **Service Description:** Provides seamless audio-visual communication via Cisco endpoints (personal systems, conference rooms, large studios).
   - **Core Infrastructure:** Cisco Unified Communication Manager (CUCM), Expressway, Cisco Conductor, and ISDN gateways. Interoperability is extended via Pexip services for Skype and Teams.
   - **Future Plans:** Replacement of cloud-managed Pexip with on-premises infrastructure.
   - **Metrics:** Ensures high availability and performance while transitioning legacy systems to cloud-based solutions.

#### **5. Security Services**
   - **Firewall Services:**
     - Manages EC's firewall infrastructure, ensuring segregation of network zones to protect against unauthorized access and threats.
     - Supports activities such as DMZ creation, rule management, and traffic monitoring.
     - Infrastructure includes Fortinet, Checkpoint, and virtualized firewalls with automated change management tools like Algosec.
   - **Proxy Services:**
     - Provides secure internet access, logging, and monitoring traffic for operational and security purposes.
     - Includes website categorization and filtering to block malicious sites.
     - Transitioning towards differentiated proxy services for specific use cases.
   - **Network Access Control (NAC):**
     - Implements dynamic network access policies based on authentication, securing access to wired, wireless, and remote networks.
     - Enhances visibility and management of network access points.
   - **Remote Access Services:**
     - SSL/IPSec VPN gateways to support secure remote connectivity.
     - Integrated with multi-factor authentication and supports various device platforms.

#### **6. Metrics and Performance**
   - Service-specific metrics detail connected sites, devices, or interfaces, and expected change/incident frequencies.
   - Telephony, videoconference, and security services track automation progress and system enhancements.

#### **7. Design and Innovation**
   - Encourages adopting new technologies, automation, and service improvements across all domains.
   - Focuses on cloud-native designs and transitioning to software-defined networking for scalability and resilience.

#### **8. Future Evolution**
   - Consolidation of data centre and network infrastructure.
   - Migration to multi-cloud connectivity services for enhanced interoperability and business continuity.
   - Enhanced security with automated provisioning and monitoring tools to adapt to evolving threats.