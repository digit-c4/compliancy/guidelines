
### **Low level design template**

---

#### **1. document overview**

- **Purpose**: Clearly define the purpose of this low level design document. State that the objective is to provide operational teams with a comprehensive guide to manage, deploy, restore, and maintain the service autonomously.
- **Audience**: Primarily for operational teams, technical leads, and DevOps engineers.

---

#### **2. asset list**

- **Inventory of Assets**:
  - List each asset involved in the service for example , servers, databases, network devices.
  - For each asset, specify:
    - **Asset Name**
    - **Type** for example , virtual machine, container, physical device
    - **Role** for example , database server, web server
    - **Location** for example , data center or cloud region
    - **Configuration Details** CPU, memory, disk space, IP addresses
    - **Unique Identifier in configuration management database** ensuring it links back to the source of truth
    - **Common platform enumeration** ensuring it links back to any new Common Vulnerability Enumeration by respecting the National Institute of Standards and Telephony dictionary.

---

#### **3. network diagram**

- **Interconnection diagram**:
  - Create a detailed network diagram that includes:
    - Asset interconnections, such as IP addresses, subnet information, and firewall rules.
    - Network segmentation, VLANs, and isolation zones if applicable.
    - Connection protocols and ports used between components.
    - Redundancy paths for failover support.
  - **Tools**: Use draw.io for version-controlled diagrams stored in GitLab.

---

#### **4. dependencies**

- **Dependency list**:
  - List all dependencies, including both internal assets and external services.
  - For each dependency, specify:
    - **Type** for example , ESX cluster, Physical device,Netbox instance,  Gitlab Repository, AWX instance, API, database, authentication service
    - **Purpose** what capability it supports
    - **Service/Asset Name**
    - **Criticality** high, medium, low
    - **configuration management database Reference** for tracking and validation.

---

#### **5. rights management design**

- **Access Control and Permissions**:
  - Define the access levels required for different roles, for example , read-only for operations, full access for a specific squad or role such as on-call.
  - Map roles to assets/services with specific permissions for each.
  - Detail any integration with centralized identity management, for example , RADIUS, TACACS, Certificate, Lightweight Directory Access Protocol, single sign on for authentication.

---

#### **6. setup guide**

- **Deployment steps**:
  - **Pre-deployment validation**: Outline prerequisites and checks in configuration management database to confirm the availability and configuration of all assets.
  - **Automated deployment/upgrade instructions**:
    - Detail the steps for deploying the service via configuration management (for example , Ansible playbooks).
    - Specify commands, configuration templates, and configuration management database queries as the source of truth.
    - **Tests**: List tests to validate the deployment, for example , configuration integrity, connectivity checks, endpoint availability, features.
  - **Avoid manual activities**: Emphasize on automated deployment without manual intervention.

---

#### **7. restore procedures**

- **Restore instructions**:
  - **Automated restore**: Outline the process for automated restoration using configuration management database information.
  - **Validation tests**: Include tests to ensure components restoration, such as checking database integrity or service availability.

---

#### **8. Failover design**

- **Failover mechanism**:
  - Describe the setup for failover across two sites:
    - Redundancy strategy, for example , active-passive, active-active
    - Triggers for failover, for example , health checks, load balancer configurations
    - Timing, amount of time to perform a failover
    - Failover impact, packet loss, session loss, data loss
  - **Failover activation**: Step-by-step instructions for triggering failover.
  - **Testing**: Outline tests to validate failover capability, including periodic failover drills.

---

#### **9. request fulfillment**

- **Service fulfillment process**:
  - Detailed steps for handling common requests, for example , adding a user, updating a configuration. Any requests must comply with the smith service definition produced earlier
  - Standard operational procedure
  - **Automated scripts**: Link to scripts or commands stored in GitLab for request fulfillment.
  - **Validation tests**: Define tests to verify request success, ensuring each fulfills the expected outcome.

---

#### **10. integration guide**

- **Monitoring and logging**:
  - **Prometheus**: Describe how's Prometheus set up to monitor this service, including key metrics, alert thresholds, and custom queries.
  - **Grafana**: Provide configuration details for Grafana dashboards related to this service, along with dashboard URLs for easy access.
  - **Event management**:
    - **Syslog and simple network management protocol traps**: Set up details for syslog forwarding and trap configurations, specifying destination servers and event types.
    - **Alerting**: Detail any alerting configurations and escalation paths.
  - **Secrets management**:
    - **Vault**: Describe integration with HashiCorp Vault or another secrets management tool, including which stored secrets, access control policies, and rotation schedules.
  - **Backup**: Describe how are data/configuration back up set, where's it stored, Backup Service used, system squad or commission backup service.
  - **Data type**: Describe the type of data in Transit or at Rest managed by the service, requirements applied, inline with Compliancy By Design Framework

---

#### **11. testing and validation plan**

- **Deployment tests**:
  - List tests to confirm deployment success, such as service accessibility, endpoint validation, and data consistency checks.
- **Restore and failover tests**:
  - Test procedures for both restore and failover scenarios, including periodic testing schedules to validate redundancy and failover readiness.
- **Monitoring tests**:
  - Validate integration with monitoring tools by verifying that all critical metrics and logs collections are OK.
- **Alert tests**:
  - Trigger synthetic alerts to confirm that they exist and present in the monitoring console.

---

#### **12. operational handover checklist**

- **Documentation verification**:
  - Confirm that all documentation is up-to-date, accurate, and version-controlled in GitLab.
- **Configuration management data base synchronization**:
  - Ensure all assets, dependencies, and configurations are accurately recorded in the configuration management database and match the low level design.
- **Access control validation**:
  - Verify that all role-based access controls are correctly configured and tested.
- **Knowledge transfer**:
  - Conduct a knowledge transfer session with operational teams, highlighting key procedures and testing outcomes.

---

This low level design template offers a complete view of the technical, procedural, and operational aspects of the service, empowering operational teams with the information and tools needed for autonomous management. It minimizes architect involvement in routine tasks while ensuring reliability, resilience, and alignment with DevOps practices.