# Service definition guidelines

Welcome to the **Service Definition** directory. This directory provides resources, templates, and methodologies to guide teams in defining and documenting IT services. Whether you are facilitating a workshop, drafting service catalog entries, or creating design documents, these materials helps ensure consistency and clarity across all service-related documentation.

---

## Directory contents

### 1. **Canvas Workshop Guide**
- A step-by-step guide for conducting a collaborative workshop to discuss and capture various facets of a service.  
- Includes prompts and techniques to help teams:
  - Define service purpose and scope.
  - Identify key stakeholders and dependencies.
  - Plan service lifecycle and delivery strategies.

---

### 2. **Service Catalogue Template**
- A reusable template for documenting services within a service catalog.  
- Helps teams describe:
  - Service name and purpose.
  - Key features and benefits.
  - Service levels and dependencies.

---

### 3. **High level design template**
- A structured template for creating High-Level Design documentation.  
- Focuses on:
  - Architecture overview.
  - Key components and their interactions.
  - Dependencies and constraints.
  - High-level workflows.

---

### 4. **Low level design template**
- A detailed template to document Low-Level Design for services.  
- Provides guidance on:
  - Specific configurations and parameters.
  - Step-by-step implementation instructions.
  - Testing and validation requirements.

---

### 5. **MoSCoW Table Template**
- A template for prioritizing features and requirements using the MoSCoW method: Must have, Should have, Could have, Won’t have.  
- Helps align teams on deliverables and manage scope effectively.

---

### 6. **Overview of Automation**
- A document offering an overview of automation opportunities for services.  
- Explores:
  - Benefits of automating service delivery and management.
  - Common automation tools and practices.
  - Examples of automated workflows.

---

## How to use this directory

1. **Plan Your Service Definition**
   - Start with the **Canvas Workshop Guide** to collaboratively define the service's key aspects.

2. **Document the Service**
   - Use the **Service Catalogue Template** to describe the service for inclusion in the catalog.
   - Create the **high level design** and **low level design** documents to provide architectural and implementation details.

3. **Prioritize Requirements**
   - Use the **MoSCoW Table Template** to prioritize features and requirements.

4. **Explore Automation**
   - Refer to the **Overview of Automation** to identify areas where automation can streamline service delivery and management.

---

## Contribution guidelines

Please contribute improvements or additional templates to this directory. If you have suggestions or new materials, please create a merge request or issue. Refer to the repository’s main [CONTRIBUTING.md](../CONTRIBUTING.md) file for detailed instructions.

---

## Support

If you have questions about the templates or need guidance on service definition, feel free to reach out to the Compliance Squad, Operation Quality Manager, or Service Delivery Manager.

Let’s define and deliver services efficiently and effectively.
