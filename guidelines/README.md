# Guidelines documentation

This directory provides a structured collection of materials related to the **several** initiatives. Three main subdirectories, each focusing on a specific aspect: context and responses to the call for tender, methodology for service design, and an automation approach.

---

## **1. Call for tender and winning answer**
### Subdirectory: [`01_Welcome`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/01_Welcome)
- **Description:**
  - Contains the official context of the Network Managed Services 3 call for tender and the detailed winning proposal submitted by NTT.
  - Documents highlight the scope, objectives, and services required by the European Commission, as well as comprehensive response to meet these needs.

- **Contents:**
  - **Call for Tender Overview:** Specifications, service requirements, and evaluation criteria.
  - **Winning Proposal:** Strategy documents outlining technical solutions and compliance approaches.

---

## **2. service design methodology**
### Subdirectory: [`02_Service_Design`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/02_Service_Design)
- **Description:**
  - Focuses on a structured methodology for describing IT services from initial workshops to detailed low-level designs.
  - Provides tools, templates, and best practices for designing, documenting, and implementing services efficiently.

- **Contents:**
  - **Workshop Frameworks:** Guides for facilitating stakeholder workshops to gather requirements.
  - **Service Design Templates:** High-level and low-level design documentation standards.
  - **MoSCoW Prioritization:** Methodology for defining critical service features and deliverables.

---

## **3. automation and workflow guidelines**
### Subdirectory: [`03_Automation_and_Workflow_Guidelines`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/03_Automation_and_Workflow_Guidelines)
- **Description:**
  - Explains approach to automation, focusing on workflows and processes to streamline service delivery.
  - Includes an overview of workflows and practical guidelines for implementing automation in IT environments.

- **Contents:**
  - **Workflow Overview:** Detailed diagrams and explanations of automated service delivery processes.
  - **Automation Guidelines:** Best practices for using tools like Ansible, CI/CD pipelines, and configuration management systems.
  - **Case Studies:** Examples of successful automation projects to serve as reference implementations.

---
## **3. compliancy guidelines**
### Subdirectory: [`99_Compliancy`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/99_Compliancy)
- **Description:**
  - Explains approach to various processes such as Event Management, Incident Management, Request Fulfilment, focusing on practical aspects to streamline service delivery.
  - Includes an detailed way of working.

- **Contents:**
  - **How to:** Detailed how to a various processes, describing efficient and practical processes activities execution.
  - **Reporting instructions:** how to read Service now reports, and question to answer to refine analysis. Conclusion of refined analysis can initiate cards in scrum boards, and also specific section in the monthly report.
  - **Inventories:** List of providers and the procedure to contact each of them.

---
## **How to navigate this directory**
1. **Start with Context:** Explore [`01_Welcome`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/01_Welcome) to understand the foundation of the Network Managed Services 3 project.
2. **Learn the Methodology:** Dive into [`02_Service_Design`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/02_Service_Design) for insights into service design and documentation.
3. **Implement Automation:** Use [`03_Automation_and_Workflow_Guidelines`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/03_Automation_and_Workflow_Guidelines) to guide your automation initiatives.
4. **Compliancy:** Use [`99_Compliancy`](https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/99_Compliancy) to guide your be compliant.
## **Feedback and collaboration**
For any questions or feedback, please go for an Issue or a Merge Request.

---
