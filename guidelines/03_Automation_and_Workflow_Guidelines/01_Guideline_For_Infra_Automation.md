### **Guideline for Infrastructure Automation**

#### **1. General Principles**
1. **Adopt DevOps Mindset**: Embrace collaboration, automation, and continuous improvement. Avoid manual, repetitive tasks.
2. **Design First, Code Second**: Begin with a clear design of workflows and sequences. Do not write Ansible code without a defined plan.
3. **Use GitLab for Everything**: Store all scripts, workflows, and documentation in GitLab. Maintain version control and enable collaboration.
4. **The use of Python** is prohibited unless you have personally mastered development best practices, including modularity, code styling, dependency management, error handling, avoiding hard coded value, and implementing unit tests.
5. **High level Users's Request Journey** 
     ```mermaid
     stateDiagram-v2
     direction LR

     state "Users" as Users
     state "C4 Gateway" as C4G
     state "NetBox" as NetBox
     state "AWX" as AWX
     state "Ansible" as Ansible
     state "Infrastructure" as Infra
     state "Service Now Portal" as C4
     
     Users --> SNOW
     Users --> JASSPR
     SNOW --> C4G
     JASSPR --> C4G
     C4G --> NetBox
     NetBox --> AWX
     AWX --> Ansible
     Ansible --> Infra

     state "DIGIT C4" as SNOW {
         C4
     }
     state "DIGIT B4" as JASSPR {
         JASSPR_Portal
     }
     state "ALL SQUADS" as C4_API {
         C4G
     }
     state "CMDB/DEV squad" as Archi {
         NetBox
     }

     state "SYS squad" as SAS {
         AWX
     }

     state "ALL SQUADS" as AllSquads {
         Ansible
         Infra
     }
     ```


---

#### **2. Workflow Design Process**
1. **Define Objectives**:
   - Identify the infrastructure task to automate.
   - Clearly specify input parameters, output parameters, and dependencies.

2. **Design Workflow**:
   - Use text-based descriptions of sequences (for example, YAML, Markdown).
   - Include activities, decisions, and transitions.

3. **Generate Workflow Diagram**:
   - Use CI pipelines in GitLab to convert text-based workflows into diagrams. Tools like [Mermaid](https://mermaid-js.github.io/mermaid/) must be used.
   - Example text description (Mermaid format):
     ```mermaid
     graph TD
     direction LR
     A[Start] --> B[Validate Input Parameters InputA, InputB]
     B --> C[Configure Device with Ansible]
     C --> D[List of Output]
     D --> E[end]
     ```
   - Integrate diagram generation in GitLab pipelines.

4. **Review and Refine**:
   - Conduct peer reviews of workflows with the help of Merge Requests.
   - Validate that workflows align with best practices and avoid hard coding.
---
#### **3. Ansible Development Standards**
1. **Follow Structure**:
   - Use Ansible roles for reusability.
   - Store variables in a structured format, such as "group_vars" and "host_vars" as input from NETBOX.

2. **Parameter Management**:
   - Use Netbox as the single source of truth for all parameters.
   - Access Netbox dynamically using APIs in playbooks.
   - Use HashiCorp VAULT for key to be used.

3. **Avoid Direct Device Interaction**:
   - Always use Ansible for infrastructure changes.
   - Ensure idempotency in playbooks.

4. **Document Roles**:
   - Include README files in each role, detailing purpose, inputs, outputs, and dependencies.

---

#### **4. GitLab Repository Organization**
1. **Directory Structure**:
   - `workflows/`: Store text-based workflow descriptions.
   - `ansible/` : Store Ansible related files
        - `playbooks/`: Store Ansible playbooks.
        - `roles/`: Store Ansible roles.
        - `collections/`: Store Ansible collections.
   - `docs/`: Store detailed documentation and diagrams.

2. **Branching Strategy**:
   - Use feature branches for new workflows or changes.
   - Require merge requests with peer review for approvals.

3. **CI/CD Integration**:
   - Automate linting of Ansible code with tools like `ansible-lint`.
   - Generate diagrams for workflows in CI pipelines.
   - Run tests in development environments before deploying.

---

#### **5. Continuous Improvement**
1. **Feedback Loops**:
   - Encourage regular retrospectives on automation processes.
   - Implement improvements iteratively.

2. **Training and Knowledge Sharing**:
   - Conduct workshops on Ansible, Netbox, and GitLab.
   - Create a shared library of best practices and templates.

3. **Monitor and Evolve**:
   - Use KPIs to track automation adoption. (Number of playbooks, Number of roles, Number of scripts (bash, python))
   - Regularly evaluate and refine the guideline.

---

#### **6. Workflow and Ansible Quality Checklist**
- [ ] Does the workflow define clear inputs and outputs?
- [ ] Are dependencies and sequences documented?
- [ ] Is the Ansible role/playbook structured and idempotent?
- [ ] Are parameters sourced dynamically from Netbox?
- [ ] Is the code and documentation stored in GitLab?
- [ ] Has the workflow been reviewed and approved?

#### **7. Tooling**
- https://intragate.ec.europa.eu/snet/wiki/index.php/Personal_VM

---
