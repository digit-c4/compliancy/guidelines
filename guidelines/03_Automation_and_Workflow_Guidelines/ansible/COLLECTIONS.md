# Collections

Collections are used in ansible as "packages" which can be shared across multiple projects. Example: community.hashi_vault

Those collections will be installed automatically via awx or the gitlab runner, by adding a requirements.yml file inside the root directory collections in your project.

Example file:
```
# collections/requirements.yml
collections:
  - name: community.docker
    version: 3.4.8
  - name: netbox.netbox
    version: 3.14.0
  - name: community.hashi_vault
    version: 5.0.0
  - name: community.general
    version: 7.4.0
    # Required to run molecule tests (ec2 driver)
  - name: amazon.aws
    version: 6.4.0
  - name: community.aws
    version: 6.3.0
  - name: community.crypto
    version: 2.15.1
  - name: ansible.utils
    version: ">=2.11.0"
  - name: awx.awx
    version: ">=22.7.0"
```
