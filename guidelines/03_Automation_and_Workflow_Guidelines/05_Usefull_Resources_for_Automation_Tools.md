# **Useful Resources for Automation Tools**

This section provides curated links and resources for the tools central to our automation workflows: **NetBox**, **AWX**, and **GitLab**. These resources include official documentation, tutorials, plugins, and community forums to assist team members in effectively using and extending these tools.

---

## **1. NetBox**
### **Official Resources**
- **NetBox Documentation**: [https://netbox.readthedocs.io](https://netbox.readthedocs.io)
- **NetBox GitHub Repository**: [https://github.com/netbox-community/netbox](https://github.com/netbox-community/netbox)

### **Tutorials and Guides**
- **Getting Started with NetBox**: [https://docs.networktocode.com/netbox/](https://docs.networktocode.com/netbox/)
- **NetBox Plugins Guide**: [https://github.com/netbox-community/plugins](https://github.com/netbox-community/plugins)

### **Community and Support**
- **NetBox Slack Channel**: [Join here](https://join.slack.com/t/netdev-community/shared_invite/zt-oij8ipap-4K~h6oHDplJIR3wDg3aqGw)
- **NetBox Discourse Forum**: [https://discourse.netbox.dev](https://discourse.netbox.dev)

---

## **2. AWX**
### **Official Resources**
- **AWX Documentation**: [https://docs.ansible.com/ansible-tower/](https://docs.ansible.com/ansible-tower/)
- **AWX GitHub Repository**: [https://github.com/ansible/awx](https://github.com/ansible/awx)

### **Tutorials and Guides**
- **AWX Quick Start Guide**: [https://github.com/ansible/awx/blob/devel/INSTALL.md](https://github.com/ansible/awx/blob/devel/INSTALL.md)
- **Automating with AWX**: [YouTube Playlist](https://www.youtube.com/playlist?list=PL2KXbZ9-EY9ZXErV8Jgv8wZSzvtjMu4_e)

### **Community and Support**
- **AWX Mailing List**: [https://groups.google.com/g/awx-project](https://groups.google.com/g/awx-project)
- **Reddit Ansible Community**: [https://www.reddit.com/r/ansible/](https://www.reddit.com/r/ansible/)

---

## **3. GitLab**
### **Official Resources**
- **GitLab Documentation**: [https://docs.gitlab.com](https://docs.gitlab.com)
- **GitLab CI/CD Guide**: [https://docs.gitlab.com/ee/ci/](https://docs.gitlab.com/ee/ci/)
- **GitLab APIs**: [https://docs.gitlab.com/ee/api/](https://docs.gitlab.com/ee/api/)

### **Tutorials and Guides**
- **Getting Started with GitLab CI/CD**: [https://docs.gitlab.com/ee/ci/quick_start/](https://docs.gitlab.com/ee/ci/quick_start/)
- **GitLab Best Practices**: [https://about.gitlab.com/blog/tags/best-practices/](https://about.gitlab.com/blog/tags/best-practices/)
- **GitLab for DevOps**: [YouTube Channel](https://www.youtube.com/user/gitlabhq)

### **Community and Support**
- **GitLab Forum**: [https://forum.gitlab.com](https://forum.gitlab.com)
- **GitLab Reddit**: [https://www.reddit.com/r/gitlab/](https://www.reddit.com/r/gitlab/)

---

## **4. General Automation Resources**
### **Books and eBooks**
- **"Infrastructure as Code" by Kief Morris**: [Amazon Link](https://www.amazon.com)
- **"Ansible for DevOps" by Jeff Geerling**: [https://www.ansiblefordevops.com](https://www.ansiblefordevops.com)

### **Blogs and News**
- **DevOps Weekly Newsletter**: [https://devopsweekly.com](https://devopsweekly.com)
- **Ansible Blog**: [https://www.ansible.com/blog](https://www.ansible.com/blog)

### **Communities**
- **DevOps Slack Channel**: [https://devopschat.slack.com](https://devopschat.slack.com)
- **Stack Overflow**: [https://stackoverflow.com](https://stackoverflow.com) (Search tags like #netbox, #awx, #gitlab, #ansible)