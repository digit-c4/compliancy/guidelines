

**Automated Service Deployment Workflow**

This document describes the process of defining and deploying automated services, starting from a service definition in **Smithy format** and proceeding through **OpenAPI**, **Netbox**, and **AWX** to execute **Ansible** playbooks. Each step in the flow ensures a standardized and consistent approach to managing services.



## **Workflow overview**
1. **Service Definition**:
   - Use Smithy format to define the service.
   - Specify inputs, outputs, and dependencies.

2. **Generate OpenAPI Specification**:
   - Convert the Smithy definition into an OpenAPI specification for API documentation and integration
   - Deploy Openapi into your _gitlab Pages_ to expose it

3. **Update Netbox**:
   - Populate Netbox with service parameters and dependencies.
   - Use Netbox as the source of truth.

4. **Trigger AWX Workflow**:
   - Use AWX to run Ansible playbooks based on parameters retrieved from Netbox.
   - Execute infrastructure changes and service configurations.

---

## **Step-by-Step process**

### **1. Service definition in smithy format**
Smithy is a flexible, model-based interface definition language for describing services and their operations.

- Create a Smithy model file `service.smithy` to describe the service.
- Example:
  ```smithy
  namespace com.example

  service MyService {
      version: "2024-01-01",
      operations: [CreateService, UpdateService]
  }

  operation CreateService {
      input: CreateServiceInput,
      output: CreateServiceOutput
  }

  structure CreateServiceInput {
      @required
      name: string
      type: string
      parameters: map<string, string>
  }

  structure CreateServiceOutput {
      id: string
      status: string
  }
  ```

### **2. convert smithy to openapi**
Use a tool like [Smithy command-line tool](https://smithy.io/) to generate an OpenAPI specification.

- Command:
  ```bash
  smithy build
  smithy-openapi --input-model model/service.smithy --output openapi/service.openapi.json
  ```

- Verify the generated `service.openapi.json` file:
  ```json
  {
      "openapi": "3.0.0",
      "info": {
          "title": "MyService",
          "version": "2024-01-01"
      },
      "paths": {
          "/create-service": {
              "post": {
                  "summary": "Create a new service",
                  "requestBody": {
                      "content": {
                          "application/json": {
                              "schema": {
                                  "$ref": "#/components/schemas/CreateServiceInput"
                              }
                          }
                      }
                  },
                  "responses": {
                      "200": {
                          "description": "Success",
                          "content": {
                              "application/json": {
                                  "schema": {
                                      "$ref": "#/components/schemas/CreateServiceOutput"
                                  }
                              }
                          }
                      }
                  }
              }
          }
      }
  }
  ```

### **3. populate netbox**
Use the OpenAPI specification to extract parameters and populate Netbox.

- Write a script, optional, to extract parameters and push them to Netbox via its API.
- Example Python code:
  ```python
  import requests

  netbox_url = "http://netbox.example.com/api/"
  headers = {"Authorization": "Token your_api_token"}

  data = {
      "name": "MyService",
      "type": "custom-service",
      "parameters": {
          "param1": "value1",
          "param2": "value2"
      }
  }

  response = requests.post(f"{netbox_url}/custom-fields/", json=data, headers=headers)
  print(response.status_code, response.json())
  ```

### **4. configure AWX workflow**
Define an AWX workflow template that uses Ansible playbooks to deploy the service.

- Example Workflow Steps:
  1. Retrieve parameters from Netbox using Netbox API modules.
  2. Configure infrastructure and deploy services using Ansible roles.

- Example Playbook:
  ```yaml
  ---
  - name: Deploy MyService
    hosts: all
    tasks:
      - name: Retrieve parameters from Netbox
        uri:
          url: "http://netbox.example.com/api/custom-fields/MyService"
          method: GET
          headers:
            Authorization: "Token your_api_token"
        register: service_data

      - name: Configure service
        include_role:
          name: my_service_role
        vars:
          service_params: "{{ service_data.json }}"
  ```

### **5. Automate workflow with gitLab CI**
Integrate the entire workflow into a GitLab CI pipeline to ensure automation and consistency.

- Example `.gitlab-ci.yml`:
  ```yaml
  stages:
    - validate
    - generate
    - deploy

  validate_smithy:
    stage: validate
    script:
      - smithy validate service.smithy

  generate_openapi:
    stage: generate
    script:
      - smithy build
      - smithy-openapi --input-model model/service.smithy --output openapi/service.openapi.json
    artifacts:
      paths:
        - openapi/service.openapi.json

  deploy_service:
    stage: deploy
    script:
      - ansible-playbook -i inventory deploy.yml
  ```

---

## **Best practices**
1. **Keep Models Updated**: Always start changes from the Smithy model.
2. **Use Netbox as Source of Truth**: Ensure all parameters and relationships go in Netbox.
3. **Enforce Peer Reviews**: Use GitLab Merge Requests to validate changes.
4. **Test Playbooks**: Test all playbooks in a staging environment before running them in production.

---

## **Troubleshooting**
1. **OpenAPI Validation Errors**:
   - Check for syntax errors in Smithy or OpenAPI models.
   - Use tools like `openapi-lint` for validation.

2. **Netbox API Issues**:
   - Verify API token and permissions.
   - Check logs for detailed error messages.

3. **AWX Task Failures**:
   - Check the AWX job output for detailed logs.
   - Ensure connectivity between AWX and infrastructure.

---