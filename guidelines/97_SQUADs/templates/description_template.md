### **Template for Describing Squad Capabilities**

Here’s a common template to describe the capabilities of your squads, followed by a series of guiding questions to help each squad fill out the template effectively.

---

#### **Squad Capabilities Template**

1. **Squad Name and Purpose**
   - **Squad Name**:  
     Clearly state the squad's name.
   - **Primary Purpose**:  
     Describe the main focus or mission of the squad (for example, maintaining CMDB accuracy, providing workplace network solutions).

2. **Key Responsibilities**
   - **Core Activities**:  
     List the primary tasks the squad handles (for example, automation, configuration management, incident resolution).
   - **Ownership Areas**:  
     Specify the systems, tools, or processes the squad owns and manages.

### **Core Capabilities**
1. **Technical Expertise**:  
     Detail the squad's technical skills or knowledge areas (for example, Python for the DEV squad, SIP protocols for the telephony squad).

