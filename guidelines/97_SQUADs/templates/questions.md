### **Guiding Questions to describe SQUAD capabilities**

To help squads effectively complete the template, use the following questions for each section:

1. **Squad Name and Purpose**
   - What is the official name of your squad?
   - In one sentence, how would you explain the squad's role to a new team member?

2. **Key Responsibilities**
   - What are the main tasks or functions your squad performs daily, weekly, or monthly?
   - What systems, tools, or processes do you manage or own end-to-end?

3. **Core Capabilities**
   - What are the most important skills or areas of expertise within your squad?
   - What are the main deliverables you produce, and who are the primary consumers?

4. **Collaboration and Dependencies**
   - Which other squads, teams, or external partners do you interact with most frequently?
   - Are there any specific tools, platforms, or services that your work depends on?

5. **Operational Metrics**
   - How do you measure success for your squad’s activities or services?
   - Are there any defined SLAs you are responsible for meeting?

6. **Challenges and Improvement Areas**
   - What are the biggest challenges or obstacles your squad faces today?
   - Are there any ongoing initiatives or plans to address these challenges?

7. **Tools and Technologies**
   - What are the key tools and technologies your squad relies on?
   - Are there any recent automation efforts or integrations that have improved your workflow?

8. **Future Goals**
   - What are your squad’s immediate priorities for the next 6-12 months?
   - How does your squad envision its role evolving over the next few years?

---

### **How to Use the Template**
1. Share the template and questions with each squad leader or representative.
2. Schedule workshops or brainstorming sessions where squad members collaboratively fill out the template.
3. Collect and standardize the information into a shared document repository for easy reference and updates.
4. Use the completed templates to identify overlaps, gaps, and opportunities for collaboration across squads. 

This structured approach ensures consistency and clarity while capturing the unique capabilities and challenges of each squad.