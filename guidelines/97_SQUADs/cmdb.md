#### **CMDB Squad**

1. **Squad Name and Purpose**
   - **Squad Name**:  
     CMDB.
   - **Primary Purpose**:  
     The CMDB squad is responsible for creating and maintaining the CMDB product to serve as a reliable source of truth for the organization. They collaborate with other squads to gather requirements, analyze needs, and model data, ensuring the CMDB aligns with operational and business needs. When necessary, they request and guide the development squad in creating custom NetBox plugins. Additionally, they support service definition efforts by providing expertise in languages such as Smithy, enabling clear and structured service documentation.

2. **Key Responsibilities**
   - **Core Activities**:  
     1. Design CMDB Service;
     2. Publish CMDB releases;
     3. Analyse Source of truth needs;
     4. Provide Data Model for source of truth.
     
   - **Ownership Areas**:  
     1. Netbox;
     2. Netbox plugins.

#### **Core Capabilities**

1. **NetBox Service Management**
   - **System Administration**:
     - Coherence in "NetBox as a Service" Product definition, in terms of configuration, deployment and maintenance.
     - Ability to ensure high availability, scalability, and performance of the NetBox instance.
   - **Version Control and Upgrades**:
     - Knowledge of NetBox's release cycle to manage upgrades seamlessly.
     - Ability to integrate NetBox with CI/CD pipelines for automated deployments.
   - **Integration Management**:
     - Experience in simulating the integration of NetBox with other systems (for example, monitoring tools, configuration management platforms) using APIs or custom plugins.

2. **Data Modeling and Schema Design**
   - **Data Modeling Expertise**:
     - Proficiency in designing logical and physical data models to represent complex infrastructure accurately.
     - Capability to extend NetBox's data schema while maintaining consistency and integrity.
   - **Understanding Dependencies**:
     - Ability to map relationships between data elements, ensuring the model reflects the dependencies of infrastructure components.
   - **Customization and Extensions**:
     - Skills to customize NetBox’s data model via configuration or plugin development to meet unique business needs.

3. **Business Requirement Analysis**
   - **Requirement Gathering**:
     - Strong communication skills to engage with other squads and gather detailed requirements.
     - Ability to translate business needs into technical specifications for new NetBox plugins.
   - **Feasibility Analysis**:
     - Expertise in evaluating the feasibility of requested features within NetBox's framework.
     - Assessing the impact of proposed changes on the existing data model and operations.
   - **Specification Writing**:
     - Ability to draft clear and actionable specifications for plugin development, ensuring alignment with business goals.

4. **Service Definition Support (Smithy Language)**
   - **Smithy Language Proficiency**:
     - Knowledge of Smithy as a service definition language to model APIs and services in a structured, consistent manner.
   - **Guidance and Collaboration**:
     - Ability to assist other squads in writing Smithy files, providing best practices and ensuring alignment with organizational standards.
   - **Validation and Review**:
     - Expertise in reviewing Smithy definitions to ensure they are accurate, complete, and consistent with service requirements.

5. **Collaboration and Knowledge Sharing**
   - **Cross-Squad Collaboration**:
     - Ability to work closely with development squads for plugin implementation and operational squads for data accuracy.
   - **Training and Support**:
     - Providing training or resources to help squads effectively use NetBox and write Smithy files.
   - **Documentation**:
     - Creating and maintaining comprehensive documentation for NetBox management, data models, and Smithy-related service definition.