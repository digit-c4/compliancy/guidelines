# HOW TO: How to handle a Loss of building

This document is a How To file to help everybody to understand his role regarding a Loss of building Business continuity plan (BCP).

---
<br>The Business continuity plans covering Loss of building scenarios are available here: 
- [NMS-III - BCP - Loss of Building - Unavailability of building in which NMT's services are provided](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20Building%20-%20Unavailability%20of%20building%20in%20which%20NMT%27s%20services%20are%20provided.docx?d=w05c591d0434c42efa44b7b4d762b74f3&csf=1&web=1&e=60M4sq).
- [NMS-III - BCP - Loss of Building - Unavailability of NMS III staff offices](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20Building%20-%20Unavailability%20of%20NMS%20III%20staff%20offices.docx?d=we40153156fb54dd6843088f5dab41c18&csf=1&web=1&e=jNvJ9J).
- [NMS-III - BCP - Loss of Building - Unavailability of storehouses under the NMSIII contractor’s responsibility](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20Building%20-%20Unavailability%20of%20storehouses%20under%20the%20NMSIII%20contractor%E2%80%99s%20responsibility.docx?d=w27b5dd63f73445d8b51e7d8d97616da5&csf=1&web=1&e=zcbCMz).


## **<br>1. How to know if you are in this situation**

<br>

><br>

> If a continuity plan is invoked, you will be involved according to the needs expressed by your PPO.<br>
> However, you must report any situation that you consider abnormal to your line manager, or to the OQM and the SDM.<br>
> The following sections will enable you to understand the scope of this continuity scenario and to assess the conditions that could lead to escalation.
> <br><br>


### **<br>1.1. Scope of the Plans**
The BCP covers the duration aspects (an unavailability of one day is different from an unavailability of several days, weeks or months).<br>
- Unavailability of one or several storehouses under the NMSIII contractor’s responsibility due to building related reasons, as unavailability of power or other accessibility. A Storehouse is not accessible or destructed.<br>
This concerns Storehouses areas used to store the NMT’s equipment.

- Unavailability of one or several NMS III staff offices (offices located in Commission buildings or elsewhere) due to building related reason (unavailability of power, fire or other accessibility problems).<br>
This concerns physical access to one or several office areas used by NMSIII organisation, not available or unusable (offices located in Commission’s building or elsewhere).

- Unavailability of one or several buildings in which NMT’s services are provided for building related (power problem, fire, etc.) or external reasons (blockage, etc.).<br>
This concerns physical access by EC staff to NMT’s services.<br>A building is not accessible for the EC staff (incl. contractors), due to:<br>
    - a blockage (strike), human welfare reasons or bad weather conditions
    - a problem with the building making access impossible

<br>

### **1.2. Exclusions**

><br>

> - Data center and Telecom center are excluded from these scenarios
> - as well as relocation of EC staff
> <br><br>


### **<br>1.3. Triggers to activate the Continuity Plan**

There can be several triggers to activate a Business continuity plan. Here are the selected scenarios with their trigger criteria.

><br>

> When one of these situations is reached or about to be reached, you must notify the OQM or SDM, so that they can assess the situation and decide whether it is necessary to activate the continuity plan.
><br><br>

<br>

| **Description** | **Impacts** | **Cases** |
|-----------------|-------------|-----------|
| Loss or partial or complete unavailability of a building.<br>It may impact the access to NMSIII **storehouses<br>where spare assets are stored**. | Possible impacts (one or more):<br>- Loss of access to spare assets<br>-	Unavailability of spare parts to provide<br> the required level of service continuity and quality | This unavailability / loss can be due to circumstances such as:<br>- Blockage (for example, strike, bad weather conditions, terrorism…)<br>- Issue with the building (for example, security gates not working)<br>-	Technical incident (for example, power outage, AC outage…)<br>-	Weather condition<br>-	Major accident in the neighborhood (for example, fire, plane crash…) | 
| Loss or partial/complete unavailability of a building.<br>The unavailability/loss may impact the **access to<br> workplaces where NMSIII services are provided**. | Possible impacts (one or more):<br>-	Loss of access to working places<br>- Inability to perform tasks requiring a physical<br> presence on the premises |This unavailability / loss can be due to circumstances such as:<br>- Blockage (for example, strike, bad weather conditions, terrorism…)<br>- Issue with the building (for example, security gates not working)<br>-	Technical incident (for example, power outage, AC outage…)<br>-	Weather condition<br>-	Major accident in the neighborhood (for example, fire, plane crash…) |
| Loss or partial/complete unavailability of a building.<br>The unavailability/loss may impact one or several<br> **buildings in which NMT services are provided**. | Possible impacts (one or more):<br>-	Loss of access to working places<br>- Inability to perform tasks requiring a physical<br> presence on the premises |This unavailability / loss can be due to circumstances such as:<br>- Blockage (for example, strike, bad weather conditions, terrorism…)<br>- Issue with the building (for example, security gates not working)<br>-	Technical incident (for example, power outage, AC outage…)<br>-	Weather condition<br>-	Major accident in the neighborhood (for example, fire, plane crash…) |

<br>


## **<br>2. How to be better prepared for such an occurrence**

In order to reduce the impact of such an occurrence, at your level, you have to implement or participate in the implementation of the measures listed here under.<br>
Your Squad's PPO is the main contact for implementing these measures.<br>
Keep in mind that developing these measures will help us to reduce the impact of a Loss of building.

<br>

| **Measure** | **Description** |
|-------------|-----------------|
| **Keeping the NMSIII storehouse inventories up to date** | This is important to keep spare material to the highest level of technology and make sure old technology spare parts will not be used in case of BCP |
| **Keeping a balanced level of distribution between the storehouses where appropriate** | This must be taken in consideration that all spare parts cannot be replicated everywhere.<br>So it might happen that in case of emergency we could face delays in the replacement due to the site where part is stored |


<br>


## **<br>3. How to improve your skills on Loss of staff continuity management**

First of all, you can read the Business continuity plans covering Loss of building scenarios: 
- [NMS-III - BCP - Loss of Building - Unavailability of building in which NMT services are provided](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20Building%20-%20Unavailability%20of%20building%20in%20which%20NMT%27s%20services%20are%20provided.docx?d=w05c591d0434c42efa44b7b4d762b74f3&csf=1&web=1&e=60M4sq).
- [NMS-III - BCP - Loss of Building - Unavailability of NMS III staff offices](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20Building%20-%20Unavailability%20of%20NMS%20III%20staff%20offices.docx?d=we40153156fb54dd6843088f5dab41c18&csf=1&web=1&e=jNvJ9J).
- [NMS-III - BCP - Loss of Building - Unavailability of storehouses under the NMSIII contractor’s responsibility](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20Building%20-%20Unavailability%20of%20storehouses%20under%20the%20NMSIII%20contractor%E2%80%99s%20responsibility.docx?d=w27b5dd63f73445d8b51e7d8d97616da5&csf=1&web=1&e=zcbCMz).

<br>

Secondly, to perfect your knowledge, you can use BCP and Crisis management documents related to Awareness and training, to improve your knowledge or for a refresher course:

- Crisis & BCM Awareness: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-Awareness_2024.mp4?csf=1&web=1&e=Mfw5JE), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20Awareness.pptx?d=wddfe31ada96a485693db7759d933a043&csf=1&web=1&e=Uv7v9w)

- Crisis & BCM walk-though exercise: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-walkthough-exercise.mp4?csf=1&web=1&e=ihtIhv), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20walkthough%20exercise.pptx?d=w2b27216ef7194eb781fe875855b433ab&csf=1&web=1&e=rMhR5P)