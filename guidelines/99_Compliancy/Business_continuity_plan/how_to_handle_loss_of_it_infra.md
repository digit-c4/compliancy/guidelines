# HOW TO: How to handle a Loss of IT infrastructure

This document is a How To file to help everybody to understand his role regarding a Loss of staff IT infrastructure continuity plan (BCP).

---
<br>The Business continuity plan covering Loss of IT infrastructure scenarios is available here: 
- [NMS-III - BCP - Loss of IT Infrastructure](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20IT%20Infrastructure.docx?d=we3d3cbe3f41d4791a6d4c039083eca86&csf=1&web=1&e=tsOTMi).
- [NMS-III - BCP - Loss of IT Infrastructure -  Unavailability of services due to DDoS activities](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20IT%20Infrastructure%20-%20%20Unavailability%20of%20services%20due%20to%20DDoS%20activities.docx?d=w933ad9f5fb1143c3a2e7c15c256e8d74&csf=1&web=1&e=E7JF13).
- [NMS-III - BCP - Loss of IT Infrastructure -  Unavailability of services due to uncontrollable behaviour of infra](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20IT%20Infrastructure%20-%20%20Unavailability%20of%20services%20due%20to%20uncontrollable%20behaviour%20of%20infra.docx?d=we016d95fd2d240b190b8954fdcc1d821&csf=1&web=1&e=LPYLFt).


## **<br>1. How to know if you are in this situation**

<br>

><br>

> If a continuity plan is invoked, you will be involved according to the needs expressed by your PPO.<br>
> However, you must report any situation that you consider abnormal to your line manager, or to the OQM and the SDM.<br>
> The following criteria will enable you to assess the conditions that could lead to escalation.
> <br><br>


### **<br>1.1. Scope of the Plan**
Unavailability of a part of the NMS-III services due to IT infrastructure related reasons (major network failure, ransomware, DDoS, unexplained or uncontrollable behaviour, etc.)

- Unavailability of services under the responsibility of NMSIII contractor due to `DDoS` or `DoS` activities.<br>
This concerns services or tools in the scope of NMSIII and under the responsibility of NMSIII contractor, in case of a long lasting `DDoS` activities impacting the availability or performance of a service.

- Unavailability of services under the responsibility of the NMSIII contractor due to an unexplained or `uncontrolable` behaviour of an infrastructure.

- And a global unavailability of IT infrastructure.

 
This concerns services or tools in the scope of NMSIII and under the responsibility of NMSIII.


### **1.2. Exclusions**

><br>

> - For `uncontrolable` behaviour, activities of a short duration shall be managed through the operational service continuity procedures (SOP)
> - `DoS` or `DDoS` with minor impact on availability are excluded from this scenario
> <br><br>


### **<br>1.3. Triggers to activate the Continuity Plan**

There can be several triggers to activate a Business continuity plan. Here are the selected scenarios with their trigger criteria:

><br>

> When one of these situations is reached or about to be reached, you must notify the OQM or SDM, so that they can assess the situation and decide whether it is necessary to activate the continuity plan.
><br><br>

<br>

| **Description** | **Impacts** | **Cases** |
|-----------------|-------------|-----------|
| Unavailability of services due to an unexplained<br>or uncontrollable behavior of an infrastructure. | Impacts (one or more):<br>- Loss of services provided by NMSIII<br>- Loss of services provided by EC<br>- Performance downgraded | This unavailability can be due to circumstances such as (non-exhaustive list):<br>- Malfunction of a vendor's technology<br>- Changes (multiple and also external to DIGIT)<br>- Duration: from 60 min | 
| Unavailability of services due to DDoS or DoS activities. | Impacts (one or more):<br>- Loss of services provided by NMSIII<br>- Loss of services provided by EC | This unavailability / loss can be due to circumstances such as:<br>- Denial of Service impacting `Telco` providers used by EC<br>- Denial of Service impacting all `Telco` providers in Europe<br>- Specific EC services affected<br>- Duration: from 15 min |
| Loss, unavailability of IT Infrastructure. | Impacts (one or more):<br>- Loss of services provided by NMSIII<br>- Loss of services provided by EC | This unavailability / loss can be due to circumstances such as:<br>- System or software failure<br>- Network failure (internal or external)<br>- Ransomware (data corruption)<br>- Major impact on premises<br>- Duration: from 15 min |

<br>


## **<br>2. How to be better prepared for such an occurrence**

In order to reduce the impact of such an occurrence, at your level, you have to implement or participate in the implementation of these measures here under.<br>
Your Squad's PPO is the main contact for implementing the measures listed here under.<br>
Keep in mind that developing these measures will help us to reduce the impact of a Loss of IT infrastructure.

<br>

| **Measure** | **Description** |
|-------------|-----------------|
| **Modular architecture** | Create a modular architecture that can be segmented or isolated if needed, and so, more flexible and adaptable to changing needs<br>Useful in situations where a part of the infrastructure needs to be isolated due to a malfunction or other issues |
| **Release notes availability** | All releases must be systematically documented in release notes, which can be accessed in the event of an incident |
| **Hardening** | Vulnerability management to address vulnerabilities that can lead to DDoS |
| **Recovery architecture** | For addressing recovery time objectives, prefer mirrored on dedicated infra, or hot replication on dedicated infra, or better<br>Mirrored on dedicated infra requires a 'resilient operation' (dual-site operations and continuous availability solutions)<br>In addition, in order to be a viable recovery strategy, this configuration should have no single point of failure and an appropriate  geographical separation and diversity of the two or more sites |
| **Recovery architecture** | For addressing recovery time objectives:<br>- Prefer synchronous replication of data or asynchronous replication<br>- Else, asynchronous replication of data or batch replication |
| **Backup architecture** | Comply to [Backup & Restore guidelines](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Compliancy%20by%20Design%20--%20Security%20by%20Design/00-Validated/04-Guidelines/NMS-III%20-%20CBD_Guidelines_BAR.docx?d=wb9677b3441ff473eafd62023ad441ce4&csf=1&web=1&e=Kcn94o) |

<br>


## **<br>3. How to improve your skills on Loss of staff continuity management**

First of all, you can read the Business continuity plans covering Loss of IT infrastructure scenarios:
- [NMS-III - BCP - Loss of IT Infrastructure](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20IT%20Infrastructure.docx?d=we3d3cbe3f41d4791a6d4c039083eca86&csf=1&web=1&e=tsOTMi).
- [NMS-III - BCP - Loss of IT Infrastructure -  Unavailability of services due to DDoS activities](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20IT%20Infrastructure%20-%20%20Unavailability%20of%20services%20due%20to%20DDoS%20activities.docx?d=w933ad9f5fb1143c3a2e7c15c256e8d74&csf=1&web=1&e=E7JF13).
- [NMS-III - BCP - Loss of IT Infrastructure -  Unavailability of services due to uncontrollable behaviour of infra](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20IT%20Infrastructure%20-%20%20Unavailability%20of%20services%20due%20to%20uncontrollable%20behaviour%20of%20infra.docx?d=we016d95fd2d240b190b8954fdcc1d821&csf=1&web=1&e=LPYLFt).


<br>

Secondly, to perfect your knowledge, you can use BCP and Crisis management documents related to Awareness and training, to improve your knowledge or for a refresher course:

- Crisis & BCM Awareness: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-Awareness_2024.mp4?csf=1&web=1&e=Mfw5JE), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20Awareness.pptx?d=wddfe31ada96a485693db7759d933a043&csf=1&web=1&e=Uv7v9w)

- Crisis & BCM walk-though exercise: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-walkthough-exercise.mp4?csf=1&web=1&e=ihtIhv), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20walkthough%20exercise.pptx?d=w2b27216ef7194eb781fe875855b433ab&csf=1&web=1&e=rMhR5P)