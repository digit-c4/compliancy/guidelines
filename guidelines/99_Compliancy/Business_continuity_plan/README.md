# Business Continuity Management Documentation

Welcome to the Business Continuity Management How To section. This repository contains . 

Below is an overview of the structure and content of this repository.

<br>


## Contents

### 1. **HOW TO:[`UNDERSTAND YOUR INVOLMENT IN CONTINUITY MANAGEMENT`](./how_to_understand_CM_involvement.md)**
- **Description**: This document outlines what is expected from you in continuity management, particularly in the event of a continuity plan being triggered.
  Includes:
  - Coverage of Business Continuity Management
  - Workflow of business continuity stages.
  - Responsibilities.
  - Awareness and trainings.
  - Testing.

### 2. **HOW TO:[`HANDLE A LOSS OF BUILDING`](./how_to_handle_loss_of_building.md)**
- **Description**: This How-To describes the scenarios that can lead to the triggering of a Loss of building continuity plan, as well as the preventive measures that should be known or implemented to prepare for such an eventuality.  
  Includes:
  - Description of scenarios.
  - Exclusions.
  - Triggers.
  - Preventive measures.

### 3. **HOW TO:[`HANDLE A LOSS OF STAFF`](./how_to_handle_loss_of_staff.md)**
- **Description**: This How-To describes the scenarios that can lead to the triggering of a Loss of staff continuity plan, as well as the preventive measures that should be known or implemented to prepare for such an eventuality.  
  Includes:
  - Description of scenarios.
  - Exclusions.
  - Triggers.
  - Preventive measures.

### 4. **HOW TO:[`HANDLE A LOSS OF IT INFRASTRUCTURE`](./how_to_handle_loss_of_it_infra.md)**
- **Description**: This How-To describes the scenarios that can lead to the triggering of a Loss of IT infrastructure continuity plan, as well as the preventive measures that should be known or implemented to prepare for such an eventuality.  
  Includes:
  - Description of scenarios.
  - Exclusions.
  - Triggers.
  - Preventive measures.

<br>

## How to Use This Repository
1. **Familiarize yourself with Business continuity management**  
   Start by reviewing [`How to understand your involvement in Continuity Management`](how_to_understand_CM_involvement.md) 

2. **Have a better understanding of triggers and prevention measures**  
   Consult continuity plans to understand the triggers and think about the preventive measures you could put in place: [`How to handle Loss of building`](how_to_handle_loss_of_building.md) , [`How to handle Loss of staff`](how_to_handle_loss_of_staff.md) , [`How to handle Loss of IT infrastructure`](how_to_handle_loss_of_it_infra.md)

3. **Improve your knowledge**
   consulting the documents located in the Teams directory dedicated to crisis management and continuity: [GRP-NMS III - Crisis Kit](https://eceuropaeu.sharepoint.com/:f:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General?csf=1&web=1&e=TjblaU)

## <br>Contributions
Suggestions for improving these guides are welcome. Please create issues or Merge Request.
