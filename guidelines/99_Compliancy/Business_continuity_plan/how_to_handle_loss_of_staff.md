# HOW TO: How to handle a Loss of staff

This document is a How To file to help everybody to understand his role regarding a Loss of staff Business continuity plan (BCP).

---
<br>The Business continuity plan covering Loss of staff scenarios is available here: [NMS-III - BCP - Loss of staff](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20staff.docx?d=w50ceb10e98ce41f88eca953be7c8d66a&csf=1&web=1&e=0wSTE3).


## **<br>1. How to know if you are in this situation**

<br>

><br>

> If a continuity plan is invoked, you will be involved according to the needs expressed by your PPO.<br>
> However, you must report any situation that you consider abnormal to your line manager, or to the OQM and the SDM.<br>
> The following criteria will enable you to assess the conditions that could lead to escalation.
> <br><br>


### **<br>1.1. Scope of the Plan**
Unavailability of a part of the NMS-III staff due to staff related reasons (pandemic, turnover, etc.)

This can be unavailability of single point of knowledge or part of the team. The BCP covers the duration aspects (an unavailability of one day is different from an unavailability of several days, weeks or months)


### **<br>1.2. Triggers to activate the Continuity Plan**

There can be several triggers to activate a Business continuity plan. Here are the selected scenarios with their trigger criteria:

><br>

> When one of these situations is reached or about to be reached, you must notify the OQM or SDM, so that they can assess the situation and decide whether it is necessary to activate the continuity plan.
><br><br>

<br>

| **Scenarios** | **Source** | **Duration** | **Staff<br>affected** | **Skill<br>affected** | **Anticipation** | **Knowledge<br>transfer** |
|---------------|:----------:|:------------:|:------------------:|:------------------:|:----------------:|:----------------------:|
| **Sudden leave:**<br>A substantial number of members of NTX are unavailable (sickness) for a significant duration (more than 4 weeks) and unable to make a transfer of ongoing tasks. | Internal   | Month        | >30%               | >50%               | None             | None                   | 
| **Announced multiple unavailability:**<br>A substantial number of members of NTX are unavailable for a duration know in advance or not (for example, internal strike). | Internal   | Weeks        | >30%               | >50%               | Days             | Possible               | 
| **Large turnover:**<br>Definitive leave of a substantial number of members of NTX over a short period inducing consistent diminution of the work capacity of one or more squads/teams. | Internal   | Unlimited    | >30%               | >50%               | Month            | Possible               | 
| **Definitive leave:**<br>Unavailability of a substantial number of NTX due to a severe accident (deadly or incapacitant), job abandonment or any other case not predictable and definitive. | Internal   | Unlimited    | >30%               | >50%               | None             | None                   | 
| **Failure of a company member of the consortium:**<br>Failure of a one of the companies regrouped under the consortium NTX resulting in the unavailability of members of NTX. | External   | Unlimited    | >50%               | >50%               | Quarter          | Possible               | 
| **External constraints:**<br>Unavailability of substantial number of NTX members resulting from external factors like pandemic, lock down, forced unemployment, armed conflict in a country… | External   | Unlimited    | 100%               | 100%               | Days             | Possible               | 

<br>


## **<br>2. How to be better prepared for such an occurrence**

In order to reduce the impact of such an occurrence, at your level, you have to implement or participate in the implementation of these measures here under.<br>
Your Squad's PPO is the main contact for implementing the measures listed here under.<br>
Keep in mind that developing these measures will help us to reduce the impact of a Loss of staff.

<br>

| **Measure** | **Description** |
|-------------|-----------------|
| **Staff planning and monitoring** | Forecast of the presence of team members to avoid crisis due to lack of resources because of days off and maintain a tolerance against loss of staff |
| **Skill matrix and Hierarchical matrix** | Identify for each major skills the level of mastery for each member (or manager) of NTX to identify potential substitutes |
| **Training and awareness** | Setup internal training paths to share and transmit knowledge and to help staff members to improve their level of mastery |
| **Automation** | Automatized activities and operations can completely prevent or reduce the impact of a loss of staff |
| **Documentation** | Produce and maintain procedures and SOP to be played by any member of a team even if the level of knowledge is low |
| **“Reservists”** | For specific skills, Identify and maintain a list of potential substitutes out of the contract but employees of the company’s signatory of the consortium NTX |

<br>


## **<br>3. How to improve your skills on Loss of staff continuity management**

First of all, you can read the Business continuity plan covering Loss of staff scenarios: [NMS-III - BCP - Loss of staff](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCP%20-%20Loss%20of%20staff.docx?d=w50ceb10e98ce41f88eca953be7c8d66a&csf=1&web=1&e=0wSTE3).

<br>

Secondly, to perfect your knowledge, you can use BCP and Crisis management documents related to Awareness and training, to improve your knowledge or for a refresher course:

- Crisis & BCM Awareness: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-Awareness_2024.mp4?csf=1&web=1&e=Mfw5JE), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20Awareness.pptx?d=wddfe31ada96a485693db7759d933a043&csf=1&web=1&e=Uv7v9w)

- Crisis & BCM walk-though exercise: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-walkthough-exercise.mp4?csf=1&web=1&e=ihtIhv), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20walkthough%20exercise.pptx?d=w2b27216ef7194eb781fe875855b433ab&csf=1&web=1&e=rMhR5P)