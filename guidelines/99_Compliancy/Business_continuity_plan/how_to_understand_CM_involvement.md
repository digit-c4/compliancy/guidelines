# HOW TO: How to understand your involvement in Continuity management

This document is a How To file to help everybody to understand his role regarding continuity management topics.

---
<br>Business continuity management framework is accessible here: [NMS-III - BCM Framework](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/Framework/NMS-III%20-%20BCM%20Framework.pdf?csf=1&web=1&e=UXUrCp).


## **<br>1. How continuity management works and who is involved**

Here's how continuity management works in the event of a major disaster:

```mermaid

flowchart LR

Phase_0[Phase 0<br>-----------------------------
Crisis declaration<br>-----------------------------
Crisis Team -TCMT]

Phase_1[Phase 1<br>-----------------------------
Ascertainment<br>-----------------------------
Crisis Team -TCMT]

Phase_2[Phase 2<br>-----------------------------
Definition of Recovery team and specific plan to apply<br>-----------------------------
Crisis Team -TCMT]

Phase_3[Phase 3<br>-----------------------------
Execution of the specific plan<br>-----------------------------
Crisis Team -TCMT & Recovery Team -RT]

Phase_4[Phase 4<br>-----------------------------
Normalisation and lessons learned<br>-----------------------------
Crisis Team -TCMT & Recovery Team -RT]

Phase_e([Nominal status])
Phase_i(["Crisis"])

Phase_i --> Phase_0 --> Phase_1 --> Phase_2 --> Phase_3 --> Phase_4 --> Phase_e

```

<br>

- The **Tactical Crisis Management Team** (TCMT or Crisis team) made up of :
  - Service Delivery Manager (SDM)
  - Operation and Quality Manager (OQM)
  - The PPO and PO involved in the major incident
  - other guests if needed and decided by this war room
  - and NMT Head of Unit (HoU) or substitute

  <br>
- The **Recovery team** (RT):

  - Made up of the teams needed to analyse, find and implement solutions (NNDOE, Architects, etc.)
  - In charge of assessing the situation, proposition of remediation plans and implementation of the selected remediation plan

<br>

> <br>

> If you have to be directly involved in a crisis, it will be through and at the request of one of these two bodies: *Tactical Crisis Management Team* or *Recovery team*.
> <br><br>

Note: the declaration of a crisis and the activation of a crisis unit are described in the directory dealing with [NMS-III - BCM Framework](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/Framework/NMS-III%20-%20BCM%20Framework.pdf?csf=1&web=1&e=WYukjs)



## **<br>2. How you are involved in the preparation of continuity management**

To keep the Information System operational during disruptive events, it is necessary to have action plans also called Business Continuity Plans or BCP, to keep critical activities up and running.

Continuity plans were developed for each of these threat scenarios:

| **Threats**                           | **Scenarios**                                                                   |
|---------------------------------------|---------------------------------------------------------------------------------|
|                                       | Unavailability of one or several buildings in which NMT’s services are provided | 
| Building related threats              | Unavailability of one or several NMSIII’s staff offices                         | 
|                                       | Unavailability of one or several storehouses under NMSIII’s responsibility      |
| |
| Staff related threats                 | Loss of staff, unavailability of staff scenario                                 |
| |
|                                       | Loss, unavailability of IT infrastructure                                       |       
| IT Services related threats           | Unavailability of services due to uncontrollable behaviour of infrastructure    | 
|                                       | Unavailability of services due to DDoS activities                               | 
| |
| Third-party suppliers related threats | Loss, unavailability of third-party suppliers’ scenario                         | 
| |


<br>

><br>

>The responsibilities for developing continuity measures are as follows:<br>
>
>- **Architects** are responsible for **designing the continuity and recovery measures** to be implemented to meet the continuity objectives (*).<br>Additional requirements to be included in the design of the services are imposed by the Commission and described in the Continuity & Recovery guidelines ([NMS-III - CBD_Guidelines_BCP](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Compliancy%20by%20Design%20--%20Security%20by%20Design/00-Validated/04-Guidelines/NMS-III%20-%20CBD_Guidelines_BCP.docx?d=w65c06472be8e425398f4990bc9110b34&csf=1&web=1&e=CDSQ2O)).<br>All these continuity and recovery measures **must be described in the SDP**, in specific chapters and paragraphs.
><br>
>
>- **PPO** is responsible and **guarantees implementation and documentation** of Design requirements and **ensure that documentation is up to date**.<br>For all services and capabilities managed by the Squad, the SOPs must document the measures implemented in compliancy with the Design requirements described above, for these domains:
>    - Service continuity.
>    - Disaster recovery.
>    - and Backup & Restore (see [Backup & Restore guidelines](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Compliancy%20by%20Design%20--%20Security%20by%20Design/00-Validated/04-Guidelines/NMS-III%20-%20CBD_Guidelines_BAR.docx?d=wb9677b3441ff473eafd62023ad441ce4&csf=1&web=1&e=a9HODw)).
><br>
><br>

<br>

Documents related to Continuity measures development: [NMS-III - CBD_Guidelines_BCP](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Compliancy%20by%20Design%20--%20Security%20by%20Design/00-Validated/04-Guidelines/NMS-III%20-%20CBD_Guidelines_BCP.docx?d=w65c06472be8e425398f4990bc9110b34&csf=1&web=1&e=CDSQ2O).

(*) Continuity objectives are described in the [NMS-III - BCM Framework](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/Framework/NMS-III%20-%20BCM%20Framework.pdf?csf=1&web=1&e=sYa2U8).



## **<br>3. How to improve your knowledge on Continuity and Crisis management**

Documents related to Awareness and training are available in PowerPoint and video format.

You can use them to improve your knowledge or for a refresher course.

- Crisis & BCM Awareness: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-Awareness_2024.mp4?csf=1&web=1&e=Mfw5JE), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20Awareness.pptx?d=wddfe31ada96a485693db7759d933a043&csf=1&web=1&e=Uv7v9w)

- Crisis & BCM walk-though exercise: [video](https://eceuropaeu.sharepoint.com/:v:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS-III---Crisis-BCM-walkthough-exercise.mp4?csf=1&web=1&e=ihtIhv), and [PowerPoint](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/CheatSheets/NMS%20III%20-%20Crisis%20%26%20BCM%20walkthough%20exercise.pptx?d=w2b27216ef7194eb781fe875855b433ab&csf=1&web=1&e=rMhR5P)



## **<br>4. How to test continuity management at your level**

The Compliancy team is responsible for organising all the tests (walk-through tests, functional tests, real simulations), **except**:

> <br>

>**PPO is responsible for organising the following tests**:
>- Desktop check or walk-through tests:<br>
> **Objective**: to check the understanding and effectiveness of continuity and recovery procedures:
>    - by simulating a major incident in theory
>    - testing that team members are familiar with the steps to follow in the event of a crisis
>    - checking that the procedures are understandable and unambiguously applicable
>    - detecting gaps in procedures or inconsistencies in the allocation of tasks
>
>   **Minimal periodicity**: 
>    - annually
>
>   **Example 1**:
>    - simulate a major component failure by going through the continuity and recovery procedures available, step by step with the team
>    - each member plays their role and describes what they would do<br>
>
>   **Example 2**: 
>    - give the team a critical server breakdown scenario and ask them to describe in what order and how they would activate the failover procedure
>    - what they would check, how they will manage the situation
>
>   Each test must be followed by a **lesson learned** session to **address the issues** observed and identify **areas for improvement**
> <br><br>

<br>

**Live tests must always be approved by NMT**.
