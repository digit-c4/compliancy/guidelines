# User Access Management (UAM) Documentation

Welcome to the User Access Management (UAM) repository. This repository contains essential documentation and resources about UAM operations. Below is an overview of the structure and content of this repository.

## Contents

### 1. Document [`Intorduction to UAM at SNET`](./01-Intorduction_to_UAM_at_SNET.md)

**Description**: Document providing the key information about the context, the concepts and the way to manage the accesses and privileges within the scope of SNET.

**Content**:

- Basic notions
- About PUMA
- How are build the privileges for SNET / NMS
- Workflow of privilege management via PUMA
- Implementation of UAM & PUAC for SNET

### 2. Document [`How to manage privileges of users`](./02-HOWTO_Manage_User_Privileges.md)

**Description**: Set of sequences of operations or SOP to manage the accesses of users within PUMA

**Content**:
Procedures available:

- Manage newcomers
- Change privileges of a user (add or remove)
- Manage the leave/off-boarding of a user or remove of all the privileges

### 3. Document [`How to control privileges of users`](./03-HOWTO_Control_User_Privileges.md)

**Description**: Procedure useful for the team leaders and the management to control the completeness and accuracy of the privileges of the users.

**Content**:

- Ensure that the process of on-boarding or changes are finished in PUMA
- Validate the structure and list of the privileges of a user
- Take action after check

### 4. Folder [`Static_images`](./Static_images/)

**Description**: contains the library of static images like screenshots or pictures which are not build neither possible to change without redoing it.

**Content**: collection of screenshots used by the several documents of this repository

### 5. Additional content

- [`Guidelines`](https://eceuropaeu.sharepoint.com/:f:/t/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Ev2TgjKa6htCteYeqty8XAEB-cn4au57oGdwKnR0Ft4bXg)
- [`Awareness`](https://eceuropaeu.sharepoint.com/:p:/t/GRP-NMSIII-Phaseinactivities2-compliancyISMS/EQTS67CNeVxKms8x8VAv0AQB27Q8MNR-ACuSSEQQ4mMHWQ)
- [`IAM Architecture diagrams`](./CbD-New_IAM_Archi.vsdx)

## Contributing

Suggestions for improving these guides are welcome. Please create issues or Merge Request.
