# Set of actions to manage accesses and privileges of SNET Users

## Preamble

This procedure is only applicable for privileges declared into PUMA.

All the privileges managed "manually" or in a "grey mode" are not in the scope of this SOP (012-2024 e.g.: NetBox, Git, AWX, ...)

To be able to use this procedure you need to have access to [PUMA](https://puma-hub19.net1.cec.eu.int/SitePages/puma.aspx)

![PUMA homepage](./Static_images/PUMA_homepage.png)

To control the implementation or the current privileges of users please refers to the [dedicated procedure](./HOWTO_Control_User_Privileges.md)

## A- Manage newcomers

This section of the document is the step by step procedure to manage the privileges of newcomers

### 1. Initiate a demand

In PUMA (left menu), go to the section Views > UAR > Add Privileges to display the list of users

![Views UAR Add Privileges](./Static_images/Views_-_UAR_-_Add_Privileges_.png)

In the list displayed click on the `+` corresponding to the user to manage

![List of newcomers](./Static_images/List_of_newcomers.png)

### 2. Configure and launch the demand

Complete the form displayed as explained below

![New privilege part 1](./Static_images/New_privilege_part_1.png)

Ⓐ In the field Implementation "Target Date" select the date to start the implementation

Ⓑ click on the empty field "Job Function" and add the following job functions :

- SNET-Operations_jobposition

- SNET-Squad_Project

![New privilege part 2](./Static_images/New_privilege_part_2.png)

when selected, the roles of a job function are listed under the form as below:

| SNET-Operations_jobposition:<BR>Roles for the operational user<BR>(also considered as job position) | SNET-Squad_Project:<BR>Squads of the user | SNET-NON_Ops_jobposition:<BR>Roles for the NON operational use<BR>(user who are not supposed to access infrastructure) |SNET-EU_IXP:<BR>specific roles for the team EU IXP|
| --- | --- | --- | --- |
| ![List of ops roles v1.1](./Static_images/List_of_ops_roles_v1.1.png) | ![List of squad roles-v1.1](./Static_images/List_of_squad_roles-v1.1.png) | ![List of NON ops roles](./Static_images/List_of_NON_ops_roles.png) | ![List of EU-IXP roles](./Static_images/List_of_EU_IXP_roles.png) |

**`NOTE: Do not forget to select the role: SNET-Ops-0Common-NTX`**

### 3. When the selection of roles is done, control the privileges granted via the roles

At the bottom of the page click on each roles listed to control the privileges granted and ensure that the privileges in the list are coherent with user profile and position

![Privileges to check](./Static_images/Privileges_to_check.png)

Modify the selection of roles if need and then if everything is correct validate the demand.

## B- Change privileges of a user (add or remove)

This section of the document is the step by step procedure to manage the privileges of an existing user

### 1. Initiate a change

Go to the section Views > UAR > Current Privileges to display the list of users

![Views UAR Current Privileges](./Static_images/Views_UAR_Current_Privileges_.png)

In the list displayed click on the ✎ corresponding to the user to manage

![Change privilege](./Static_images/Change_privilege.png)

### 2. Configure and launch the change

Complete the form displayed as explained below

![Change privilege form](./Static_images/Change_privilege_form.png)

- The job functions already provisioned Ⓐ;

- A field to add potential new job position Ⓑ;

- Roles already provisioned (according to each job functions already provisioned) Ⓒ;

- List of the role of possible for each job functions Ⓓ with roles provisioned ☑ and roles available for provisioning ❑;

Select the roles of each job function corresponding to the user
For additional accesses select the roles needed if it is relevant (for example: the user is part of several squads or accumulates several operational roles)

**`NOTE: to add a specific privilege it is recommended to select-it via the job function SNET-Global_catalog-Exceptions_Unitary`**

To remove of accesses just un-select existing roles ☑ => ❑;.

| SNET-Operations_jobposition: <BR>Roles for the operational user <BR>(also considered as job position) | SNET-Squad_Project: <BR>Squads of the user |
| --- | --- |
| ![List of ops roles v1.1](./Static_images/List_of_ops_roles_v1.1.png) | ![List of squad roles v1.1](./Static_images/List_of_squad_roles-v1.1.png) |

**`NOTE: The role 0Common-NTX is mandatory`**

### 2. When the selection of roles is done, control the privileges granted via the roles

At the bottom of the page click on each roles listed to control the privileges granted and ensure that the privileges in the list are coherent with user profile and position

![Privileges to check](./Static_images/Privileges_to_check.png)

Modify the selection of roles if need and then if everything is correct validate the demand.

## C- Manage the leave/off-boarding of a user or remove of all the privileges

This section of the document is the step by step procedure to delete all the privileges an existing user via a single demand.

### 1. Initiate a removal

Go to the section Views > UAR > Current Privileges to display the list of users

![Views UAR Current Privileges](./Static_images/Views_UAR_Current_Privileges_.png)

In the list displayed click on the 🗑️ corresponding to the user to remove

![List removable user](./Static_images/List_removable_user.png)

### 2. Configure and launch the removal

![Form of user removal](./Static_images/Form_of_user_removal.png)

Complete the implementation target date and then validate the demand.

-----------------------

This is the end of the procedures

`Reminder to control the implementation or the curent privileges of users please refers to the [dedicated proceure](./HOWTO_Control_User_Privileges.md)`
