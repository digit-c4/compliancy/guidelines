# Introduction to User Access Management (UAM) for SNET

## Preamble

Within the scope SNET, privileges are managed via the tool named PUMA.
It is managed by DIGIT C1 and accessible via the following [URL](https://puma-hub19.net1.cec.eu.int/SitePages/puma.aspx)

## Basic notions for this document

- `Access rights` define what a user can do within an IT resource (system, application, service, etc.), e.g.: a user can read the content of a folder.
- `Privileges` specific family of `access rights` allowing users to perform actions with high impact or security implications or access critical information, e.g.: manage the settings of system, modify the configuration of an asset, create users, etc.
- `Roles` a coherent and consistent assembly of `access rights` or `privileges` enabling a user to perform the task of its job function.
- `Access management` set of operations of managing users, defining and implementing roles or accesses and granting them to users.
- `Privileged user` user who has "elevated" or "highly impacting" access rights also called `privileges`.
- `Role Based Access control` access rights and privileges are granted and so controlled according to the role of the user inside the SNET scope.
- `UAM` stands for `User and Access Management` this is a process managing the provisioning of users privileges or access and granting accesses or privileges to users
- `IAM`  stands for `Identity and Access Management` this process include a step before `UAM` by also managing identities of users (specifically human users)
- `PUAC` or `Privileged User Access Control` in the context of SNET this is the management of privileged users and controls of their access on systems.

## About PUMA

PUMA is a tool to manage accesses of privileged users (PUAC) at the onboarding, during the employment and at leave.
It helps to request all the privileges needed by SNET teams including privileges outside of the scope SNET (like SNOW, LDAP EC or distribution lists).
Puma is automatically triggering Service Now to request tasks of provisioning (removal) of accesses.

## How are build the privileges for SNET / NMS

Each privilege represents unitary technical account/access/right or a monolithic set of it.

Into PUMA:

- The privileges are grouped into roles according to a job position or the membership to a team.
- Roles are grouped into “job function” representing an organisational attribute of a person.

![Privilege Matrix](./Static_images/Privilege_Matrix.png)

To provision a user, a complete profile is composed of:

**NTX users:**

- The common operational role which is a mandatory bundle of privileges
- An Operational role (or more) like TDA, SEC, NOCA, SYS, PPO, etc.
- And if relevant a SQUAD role (or more) like RA, WP, VC, RPS, etc…

**Official users:**

- The common operational role which is a mandatory bundle of privileges
- An Operational role (or more) like QPC, SEC or TEL_VC
- And if relevant a SQUAD role (or more) like RA, WP, VC, RPS, etc…

It exists specific roles like NON-OPS or EU_IXP which don't have squad roles but only `job function` and `roles`.

## Workflow of privilege management via PUMA

Managing privileges via PUMA goes through 5 main steps, it involves SNOW to realize the provisioning / implementation of privileges.

![PUMA's workflow](./Static_images/PUMA's_workflow_v1.1.png)

|                        | Manage privileges for a newcomer | Validate a demand in PUMA | Change privileges of a user | Remove a user | Follow the demand of privileges |
| ------------------------ | ---------------------------------- | --------------------------- | ----------------------------- | --------------- | --------------------------------- |
| End user / Beneficiary | <span style="color:#8B0000;">Forbidden</span>                        | <span style="color:#8B0000;">Forbidden</span>                  | <span style="color:#8B0000;">Forbidden</span>                    | <span style="color:#8B0000;">Forbidden</span>      | Not authorised in PUMA           |
| HR NTX | <span style="color:green;"><b>Can do</b></span>                        | <span style="color:#8B0000;">Forbidden</span>                  | <span style="color:green;"><b>Can do</b></span>                    | <span style="color:green;"><b>Can do</b></span>      | <span style="color:green;"><b>Can do</b></span>           |
| Official | N/A                      | <span style="color:green;"><b>Can do</b></span>                  | N/A                    | N/A      | N/A           |
| HR NTX | <span style="color:green;"><b>Can do</b></span>                       | <span style="color:#8B0000;">Forbidden</span>                  | <span style="color:green;"><b>Can do</b></span>                   | <span style="color:green;"><b>Can do</b></span>      | <span style="color:green;"><b>Can do</b></span>           |

For each privilege a request is generated into SNOW and the scheduling of requests in SNOW is handled via PUMA.

The scheduling is mandatory when there are dependencies between privileges to respect.

## Implementation of UAM & PUAC for SNET

### The teams in PUMA

From a PUMA point of view, people are dispatched between SQUADS (called team in PUMA):

- C4 - SNET - CERT
- C4 - SNET - CMDB
- C4 - SNET - DCTC
- C4 - SNET - DEV
- C4 - SNET - FW
- C4 - SNET - GRC
- C4 - SNET - LB_DNS_PROXY
- C4 - SNET - MGT
- C4 - SNET - OIS
- C4 - SNET - RAS
- C4 - SNET - RPS
- C4 - SNET - SYS
- C4 - SNET - TEL
- C4 - SNET - VC
- C4 - SNET - WP (which is now the merged with WIFI)

Or by default people is allocated to the team C4 - SNET.

<b><span style="color:#8B0000;"> IMPORTANT:</span> Managers (SDM and OQM) are able to manage the privileges of all the organisation teams and PPO are able to manage privileges of their own “organisational team” and the members the organisational team C4 - SNET.</b>

### Structure of privileges

During the configuration of a demand in PUMA you must know the following rules to grants accesses properly to a user.

PUMA use this structure, "Privileges" are regrouped into "roles" which are regrouped into "job function":

- A privilege is a unitary/monolithic access like LDAP account, AD membership, RO access on Checkpoint etc ...

- A role is a coherent set of privileges needed by a user to perform daily tasks, in the existing context users must be associated to several roles to have the full set of privileges. Those roles have been designed to ensure a simple and modular provisioning of accesses

- A job function is a set of roles representing an aspect of the organisational structure of SNET like SNET-Ops-NOCA, SNET-Squad-SYS, SNET-NON_Ops-MGT, etc...

The table bellow gives the template configuration of complete profile

<table>
  <caption></caption>
  <tbody>
    <tr><th></th><th colspan="11">Category of users</th></tr>
    <tr><th rowspan="2">Job function</th><th colspan="2">Non operational user</th><th colspan="8">Operational user</th><th rowspan="2">Officials</th></tr>
    <tr><th>HR</th><th>Manager (contract manager, ...)</th><th>Manager (SDM, OQM)</th><th>PPO</th><th>SCRUM</th><th>NNDOE</th><th>Architect</th><th>NOCA</th><th>OIS</th><th>Other</th></tr>
    <tr><td>SNET-NON_Ops_jobposition</td><td>Role: SNET-NON_Ops-HR_NTX</td><td>Role: SNET-NON_Ops-HR_NTX<br>Role: SNET-NON_Ops-MGT</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr>
    <tr><td>SNET-Officials</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>Role: SNET-Officials-0Common<br>Role: SNET-Officials-*</td></tr>
    <tr><td>SNET-Officials-SQUAD_PROJECT</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>Role: SNET-Officials-Squad-*</td></tr>
    <tr><td>SNET-Operations_jobposition</td><td>N/A</td><td>N/A</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-MGT</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-PPO</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-Scrum_M</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-*</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-TDA</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-NOCA</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-OIS</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-*</td><td>N/A</td></tr>
    <tr><td>SNET-SQUAD_PROJECT</td><td>N/A</td><td>N/A</td><td colspan="8">Role: SNET-Squad-*</td><td>N/A</td></tr>
  </tbody>
</table>

(*)choose the role according to the scope
