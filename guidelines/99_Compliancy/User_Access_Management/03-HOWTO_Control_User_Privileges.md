# Set of actions to control and certify accesses and privileges of SNET Users

## Preamble

This procedure aims to help the team leaders to control and certify the privileges and access rights of the members of their team.

This procedure is only applicable for privileges declared into PUMA.

All the privileges managed "manually" or in a "grey mode" are not in the scope of this SOP (012-2024 e.g.: NetBox, Git, AWX, ...)

To be able to use this procedure you need to have access to [PUMA](https://puma-hub19.net1.cec.eu.int/SitePages/puma.aspx)

![PUMA homepage](./Static_images/PUMA_homepage.png)

To manage the privileges of users please refers to the [dedicated procedure](./HOWTO_Manage_User_Privileges.md)

## 1) Ensure that the process of on-boarding or changes are finished in PUMA

![Pending requests or privileges](./Static_images/Views_-_UAR_-_Pending_Privileges.png)

The users to be checked should not be in this list

## 2) Validate the structure and list of the privileges of a user

Use the View on Current Privileges of PUMA

![Current Privileges](./Static_images/Views_UAR_Current_Privileges_.png)

In a first stage, check that the structure of the privileges granted complies with the "standard and compliant" structure.
The table bellow gives the template configuration of complete profile

<table>
  <caption></caption>
  <tbody>
    <tr><th></th><th colspan="11">Category of users</th></tr>
    <tr><th rowspan="2">Job function</th><th colspan="2">Non operational user</th><th colspan="8">Operational user</th><th rowspan="2">Officials</th></tr>
    <tr><th>HR</th><th>Manager (contract manager, ...)</th><th>Manager (SDM, OQM)</th><th>PPO</th><th>SCRUM</th><th>NNDOE</th><th>Architect</th><th>NOCA</th><th>OIS</th><th>Other</th></tr>
    <tr><td>SNET-NON_Ops_jobposition</td><td>Role: SNET-NON_Ops-HR_NTX</td><td>Role: SNET-NON_Ops-HR_NTX<br>Role: SNET-NON_Ops-MGT</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td></tr>
    <tr><td>SNET-Officials</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>Role: SNET-Officials-0Common<br>Role: SNET-Officials-*</td></tr>
    <tr><td>SNET-Officials-SQUAD_PROJECT</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>N/A</td><td>Role: SNET-Officials-Squad-*</td></tr>
    <tr><td>SNET-Operations_jobposition</td><td>N/A</td><td>N/A</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-MGT</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-PPO</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-Scrum_M</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-*</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-TDA</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-NOCA</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-OIS</td><td>Role: SNET-Ops-0Common-NTX<br>Role: SNET-Ops-*</td><td>N/A</td></tr>
    <tr><td>SNET-SQUAD_PROJECT</td><td>N/A</td><td>N/A</td><td colspan="8">Role: SNET-Squad-*</td><td>N/A</td></tr>
  </tbody>
</table>

(*)choose the role according to the scope

Then ensure that the list of privileges is compliant with the operational needs of the users.

The user should not have privileges / roles of an other squad neither too much unitary/exceptions.

**Important: Make sure to apply the principle of least privilege.**

## 3) Take action after check

Depending on the controls processed 3 different cases are possible:

- <u>Case A:</u> Even if in PUMA the privilege is "granted" or "implemented" the user can't connect to the system or still don't have access, so an additional support may be needed (check the section Case A- Privileges appears as granted but it is not working for more details)

- <u>Case B:</u> Privileges granted are not aligned with the operational needs of the user, it may have too few or too much rights, so its profile must be realigned (check the section Case B- The privileges granted are incorrect and need to be reduced or extended)

- <u>Case C:</u> By comparing the set of privileges of similar users, it appears that the roles are no more aligned with the operational needs, too few or too much privileges are granted by default and the catalog of privileges must be update according to the new situation (check the section Case C- The privileges granted via roles are no more/not aligned with needs of the job function)

### Case A- Privileges appears as granted but it is not working

| Item   | check method  | what to do if there is a problem |
| ---------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------- |
| SECEM 2 certificate| Enrolment done by the user and try to send an encrypted mail| contact the EC help desk|
| Leankit (boards) | Check the link:[https://globalntt.leankit.com/](https://globalntt.leankit.com/) | contact HR NTX (Sabina)|
| UDEMY  | [https://ntt.udemy.com/organization/home/](https://ntt.udemy.com/organization/home/)   | contact HR NTX (Sabina)|
| "PLANNING DES EQUIPES"(time tracking) | Check the link:[https://intragate.ec.europa.eu/snet/web/planning](https://intragate.ec.europa.eu/snet/web/planning)    | contact HR NTX (Sabina)|
| SNET Tools (INTRAGATE)  | Check the link:[https://intragate.ec.europa.eu/snet](https://intragate.ec.europa.eu/snet)and inside this page try to access the tools needed | contact squad SYS and / or DEV   |
| WIKI SNET (INTRAGATE)   | Check the link:[https://intragate.ec.europa.eu/snet/wiki/index.php/Main_Page](https://intragate.ec.europa.eu/snet/wiki/index.php/Main_Page)  | contact squad SYS and / or DEV   |
| VPN SNET   | Ensure that Welcome Poppy network configuration is available on the laptop ![VPN Available](./Static_images/VPNs.png) and try to connect | contact RAS|
| CheckPoint  | [https://fwmgtlu.snmc.cec.eu.int/](https://fwmgtlu.snmc.cec.eu.int/) or [https://fwmgtbx.snmc.cec.eu.int/](https://fwmgtbx.snmc.cec.eu.int/)or use the client **"Checkpoint SmartConsole R81.10"** to connect on the servers **fwmgtlu.snmc.cec.eu.int** or **fwmgtbx.cec.eu.int**(be sure to be connected via POPPY)   | contact NOCA and / or FW squad   |
| SNOW (ITSM) | Check the link:[https://digit.service-now.com/now/sow/home](https://digit.service-now.com/now/sow/home) | contact HR NTX (Sabina)|
| Terminal Server  | Check that AD NET1 account (T1-[UID]) is active and try to connect to a terminal server| contact the EC help desk|
| Distribution list| Check that the user is properly declared into the distribution list of the team/squad***"DIGIT NMS [SQUAD]***| contact squadYS |

### Case B- The privileges granted are incorrect and need to be reduced or extended

This case is applicable if the case is "isolated" meaning that not all the users with the same profile are impacted.
If all the users sharing the same profile have the same problem so apply the Case C instead of the Case B.

#### Case B.1- too much roles have been granted to the users or too much unitary privileges have been added

The profile of the user must be corrected to revoke unneeded privileges, use the appropriate procedure to [manage the privileges of the user](./HOWTO_Manage_User_Privileges.md) at the section `B- Change privileges of a user (add or remove)`

#### Case B.2- privileges are missing to the user

##### Action 1: First ensure that the initial or previous requests have been completed properly

![Previous requests](./Static_images/Views_-_UAR_-_Privileges_Requests.png)

The status of the requests must be `Implemented` to guaranty that all the requests have fully granted the privileges.

If the status is`Partially Implemented` a problem has occurred during the implementation and so one or more privileges may not have been granted.

Check in the privileges "not implemented" those which are missing, list them and contact Compliancy Squad for support about the next actions to engage.

##### Action 2: If everything is fine in the previous request, so complete the profile of the user

The profile of the user must be completed to add the needed privileges, use the appropriate procedure to [manage the privileges of the user](./HOWTO_Manage_User_Privileges.md) at the section `B- Change privileges of a user (add or remove)`

### Case C- The privileges granted via roles are no more/not aligned with needs of the job function

In this case, you clearly assume that the catalogue of privileges must be updated to:

- add or remove a privilege into or from an existing role because all the users sharing this role needs adjustment

- create a new privileges and add it to a specific role because a new service or application or product has been released

So complete this [form](https://eceuropaeu.sharepoint.com/:l:/t/GRP-NMSIII-Phaseinactivities2-compliancyISMS/FBDP3sLMHbxAp51WNENphq4BF_wjYDHlNvntraIbxqBnSg?nav=MWIxNzM0NjMtNjk1OS00NjZjLTg2ZTktMDA1MWFkMzBkMGJi) and contact Compliancy Squad.
