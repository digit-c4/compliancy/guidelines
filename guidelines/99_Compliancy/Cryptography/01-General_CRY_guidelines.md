# General guidelines of cryptography

## Preamble

Cryptography (CRY) regroups management of secrets like keys, pass phrases, passwords, etc... and management of certificates

## Secret management

### Using the vault (`Hashicorp`)

see additional documentation  :

- [`System/HashiCorp Vault namespace administration`](https://intragate.ec.europa.eu/snet/wiki/index.php/System/HashiCorp_Vault_namespace_administration)
- [`System/accessing and managing hashicorp vault`](https://intragate.ec.europa.eu/snet/wiki/index.php/System/accessing_and_managing_hashicorp_vault)
- [`Development/hashicorp vault/hv playbook implementation - Snet`](https://intragate.ec.europa.eu/snet/wiki/index.php/Development/hashicorp_vault/hv_playbook_implementation)

**Important rules:**

- The integration and use of the VAULT by SNET is managed by PO of SYS Squad.
- The Vault is managed by 2 entities DIGIT CX for the global service and DIGIT C4/SNET for the engines (repositories) and the access to the engines (via policies).
- The engines are organised in repositories in which key-values system store secrets.
- The policies are based on the naming convention ruling access according to the repository name.
- The accesses are granted to users according to the policies with which they are associated.

### Creation and storage of secrets

Once created by the asset owners,services owners or users, the secrets are store associated to a label and indexed into the engine and repository.

Here is a sample of engines and repositories

| Engine        | Directory  | Key-value label          |
| --------------- | ------------ | -------------------------- |
| DIGIT-C4-SNET | ...      | ...                   |
| DIGIT-C4-SNET | AUTOMATION | snet vs*************_285 |
| DIGIT-C4-SNET | LABO       | lab-w************-4      |
| DIGIT-C4-SNET | LABO       | ma************rs         |
| DIGIT-C4-SNET | PWD        | SU************lve        |
| DIGIT-C4-SNET | PWD        | Use************ce        |
| DIGIT-C4-SNET | SEC/NET    | ac************p          |
| DIGIT-C4-SNET | SEC/NET    | ************2isp         |
| DIGIT-C4-SNET | SEC/SUP    | esx************radm      |
| DIGIT-C4-SNET | SEC/SUP    | es************in         |
| DIGIT-C4-SNET | ...      | ...                    |

Consult the PO of the squad SYS to know which one to use or how to create a new engine or repository if needed.

### Accessing the vault

3 different cases are possible:

- Via the "GUI": the GUI is reachable behind the bastion and users authenticated can access to the vault and engines according to the policies
- Via the API with an user token: first the user has to authenticate via the GUI and use the same token to use the API and access to the vault and engines according to the policies
- Via the API with an AppRole: it is a predetermined access to a specific set of secrets and operation usually granted for a M2M use / automation

3 kind of access:

- RO: used for share passwords or by M2M / automation
- RW: used by humans and permuting operations on secrets like creation, renewal, deletion, etc ...
- Management: this kind of access do not permit to see or manipulate secrets but only to configure the service

## Specific implementation guidelines

### DNS Secure

| **Key type (for DNS Secure)** | Level of compliance | Digital signature Algorithm suite | Key or Prime field Size (in bits) | Life duration |
| ------------------------------- | --------------------- | ----------------------------------- | ----------------------------------- | --------------- |
| **Key-Signing Key (KSK)**     | Acceptable          | RSA-SHA256                        | 4096                              | 24 months     |
|                               | preferred            | ECDSA P-284                       | 255                               | 24 months     |
| **Zone-Signing Key (ZSK)**    | Acceptable          | RSA-SHA256                        | 2048                              | 3 months      |
|                               | preferred            | ECDSA P-256                       | 224                               | 12 months     |

### Open VPN

**Additional parameters**

| Level of compliance                            | Cipher      | Authentication           | Key or Prime field Sise(in bits) | DH GROUP or 
Key exchange parameter                                   |
| ------------------------------------------------ | ------------- | -------------------------- | ---------------------------------- | --------------------------------------------------------------------- |
| Forbidden (any of the following parameters ->) | *DES-*      | SHA1 (or older like MD5) |                                  |                                                                     |
| End of life                                    | AES-128-*   | SHA2 (256)               | 128                              | 14 (DH2048) or 19 (secp256r1 / P-256)                               |
| Acceptable                                     | AES-256-CBC | SHA2 (256)               | 256                              | 14 (DH2048) or 19 (secp256r1 / P-256)                               |
| preferred                                       | AES-256-GCM | SHA2 (256)               | 256                              | 20 (secp384r1 / P-384) or 21 (secp521r1 / P-521) or 31 (Curve25519) |

**Additional parameters**

1. Perfect Forward Secrecy (PFS) enabled
2. TLS Authentication via TLS 1.3
3. Regular Key Rotation

### IPSEC

Configuration Cryptographic parameters

**IKE Policy:**

| **Parameter**        | **preferred**                                                  | **Acceptable**                     |
| ---------------------- | --------------------------------------------------------------- | ------------------------------------ |
| Encryption Algorithm | AES-256                                                       | AES-256                            |
| Authentication       | RSA-SIG                                                       | `PSKs` (Pre-shared Keys)                               |
| Integrity Algorithms | SHA2 (256)                                                    | SHA2 (256)                         |
| Key Exchange Group   | 20 (secp384r1 / P-384) 21 (secp521r1 / P-521) 31 (Curve25519) | 14 (DH2048) 19 (secp256r1 / P-256) |

**ESP (Transformation set):**

| **Parameter**        | **preferred**       | **Acceptable** |
| ---------------------- | -------------------- | ---------------- |
| Encryption Algorithm | AES-256-GCM        | AES-256-CBC    |
| Authentication       | N/A (built-in GCM) | HMAC SHA256    |

**Additional parameters**

1. Use IKE v2 (if possible)
2. Perfect Forward Secrecy (PFS) enabled
3. Anti-Replay Protection enabled
4. Regular Key Rotation

### TLS

<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Classification</th>
      <th>Level of compliance</th>
      <th>TLS protocols allowed</th>
      <th>Ordered cipher-suites allowed</th>
    </tr>
  </thead>
  <tbody>
    <tr><td rowspan="2">Low</td><td rowspan="2">PA: Publicly Available</td><td>Preferred</td><td>TLS v1.3</td><td>TLS_AES_128_GCM_SHA256<br>TLS_CHACHA20_POLY1305_SHA256<br>TLS_AES_256_GCM_SHA384</td></tr>
    <tr><td>Acceptable</td><td>TLS v1.2</td><td>TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256<br>TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256<br>TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384<br>TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA<br>TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA<br>TLS_RSA_WITH_AES_128_CBC_SHA<br>TLS_RSA_WITH_AES_256_CBC_SHA</td></tr>
    <tr><td rowspan="2">Medium</td><td rowspan="2">CU: Commission Use</td><td>Preferred</td><td>TLS v1.3</td><td>TLS_AES_128_GCM_SHA256<br>TLS_CHACHA20_POLY1305_SHA256<br>TLS_AES_256_GCM_SHA384</td></tr>
    <tr><td>Acceptable</td><td>TLS v1.2</td><td>TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256<br>TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256<br>TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384<br>TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA<br>TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA</td></tr>
    <tr><td rowspan="2">High</td><td rowspan="2">SNC: Sensitive Non Classified</td><td>Preferred</td><td>TLS v1.3</td><td>TLS_AES_128_GCM_SHA256<br>TLS_CHACHA20_POLY1305_SHA256<br>TLS_AES_256_GCM_SHA384</td></tr>
    <tr><td>Acceptable</td><td>TLS v1.2</td><td>TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256<br>TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256<br>TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384</td></tr>
    <tr><td>Waiver</td><td>Deprecated / old assets</td><td>Exception only</td><td>TLS1.0 or other SSL</td><td>Specific to each case</td></tr>
  </tbody>
</table>

**Additional parameters:**

1. Server-Preferred Order feature for cipher-suites enabled
2. Regular Key Rotation

### SSH keys

**Key lifecycle:**

<table>
  <thead>
    <tr>
      <th>Parameter</th>
      <th>Level of compliance</th>
      <th>value</th>
      <th>comment</th>
    </tr>
  </thead>
  <tbody>
    <tr><td rowspan="2">Size of the key</td><td>preferred</td><td>ECDSA 256</td><td>or higher</td></tr>
    <tr><td>Acceptable</td><td>RSA 2048</td>or higher</tr>
    <tr><td rowspan="2">Life duration of the key</td><td>preferred For keys used for authentication</td><td>90 days</td><td>like a password</td></tr>
    <tr><td>preferred for keys sued for encryption only</td><td>2 years</td>or less</tr>
  </tbody>
</table>

**Key policy:**

- Use authentication by SSH Key only when no other SSO method is available
- Forbidden to configure authentication via SSH keys for Super User / Root / Full admin accounts
- The access to the configuration file and operation to add / change / remove keys must be restricted/doable only by users with high level of privilege.
- Maintain traceability and belonging of each Key configured for the authentication into the CMDB
- A review of the Authorized keys for authentication must be done at least once a year

### Certificates

A supporting service of management of certificates is available for full automation.
the following schema gives an overview on hos it works.

```mermaid
%%{
    init: {
    'theme': 'base', 
    'themeVariables': { 
        'primaryColor': '#eff7f6', 
        'primaryTextColor': '#000000', 
        'primaryBorderColor': '#000000', 
        'lineColor': '#890102', 
        'secondaryColor': '#f6f9f9', 
        'tertiaryColor': '#f8fcfb' 
        }
    }
}%%
flowchart LR

    classDef Blue_commission_style fill:#034EA2, stroke:white, stroke-width:3px, color:white;
    classDef Ember_commission_style fill:#FFC000, color:black;
    classDef Blue_line_style fill:#FFF, stroke:#034EA2,stroke-width:3px;
    classDef Ember_line_style fill:#FFF, stroke:#FFC000,stroke-width:3px;
    classDef Light_Ember_commission_style fill:#FFF8E5, color:black;
    classDef Light_Blue_commission_style fill:#D3E8F9, stroke:white, stroke-width:3px, color:black;
    classDef ghost_L_Ember_com_style fill:#FFF8E5, stroke:#FFF8E5, color:black;

    %% Nodes definition

    USER(((Operator /<BR> automation))):::Ember_commission_style
    NetBox[(Netbox)]:::Blue_commission_style
    AWX>AWX]:::Blue_commission_style
    WebHook:::ghost_L_Ember_com_style
    NetBox_SID[(Netbox &<BR>SID )]:::Blue_commission_style
    CA_CommiSign[/CommiSign\]:::Blue_commission_style
    CA_LetsEncrypt[/LetsEncrypt\]:::Blue_commission_style
    CA_GlobalSign[/GlobalSign\]:::Blue_commission_style
    Hashicorp[[Hashicorp]]:::Blue_commission_style
    Playbook[[Playbook<BR>Ansible]]:::Light_Blue_commission_style
    IT_R([IT Resources]):::Blue_commission_style
    API((( ))):::Blue_commission_style
    API_R((( ))):::Blue_commission_style
    API_ACME((( ))):::Blue_commission_style

    subgraph CMDB_start[<u>**CMDB**</u>]
        NetBox
    end
    class CMDB_start Light_Ember_commission_style

    subgraph Orchestrator[<u>**Orchestrator**</u>]
        direction TB
        AWX
        WebHook
    end
    class Orchestrator Light_Ember_commission_style

    subgraph Automation[<u>**Automation**</u>]
        Playbook
    end
    class Automation Light_Ember_commission_style

    subgraph IT_G[<u>**IT**</u>]
        IT_R
    end
    class IT_G Light_Ember_commission_style

    subgraph CMDBs[<u>**CMDBs**</u>]
        NetBox_SID
        API
    end
    class CMDBs Light_Ember_commission_style

    subgraph CA_PKI[<u>**Certificate authorities**</u>]
        direction TB
        CA_CommiSign
        CA_GlobalSign
        CA_LetsEncrypt
        API_ACME
    end
    class CA_PKI Light_Ember_commission_style

    subgraph Vault[<u>**Vault**</u>]
        Hashicorp
        API_R
    end
    class Vault Light_Ember_commission_style

    USER==>|1- Change / Create<BR>Configuration|NetBox
    NetBox==>|-2 Trigger|WebHook-..->AWX
    AWX==>|3- Trigger|Playbook
    NetBox_SID<-->|APIs|API
    CA_CommiSign<-->|ACME|API_ACME
    CA_LetsEncrypt<-->|ACME|API_ACME
    CA_GlobalSign<-->|ACME|API_ACME
    Hashicorp<-->|APIs|API_R
    API_ACME<-->|4- Manage Certificates<BR>*create, renew, revoke*|Playbook
    API <-->|5- Manage Configuration<BR>Store Certificate metadata|Playbook
    API_R<-->|6- Manage Secret<BR>*Private key<BR>of the certificate*|Playbook
    Playbook==>|7- Deploy|IT_R
    Playbook==>|7- Deploy|IT_R
    Playbook==>|7- Deploy|IT_R
```

<U>**NOTE:**</U>`Autocert` is accepted for legacy use when the cert service is not integrated but this mode is end of life.
