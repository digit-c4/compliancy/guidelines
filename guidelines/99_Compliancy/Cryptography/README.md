# Cryptography (CRY) Documentation

Welcome to the User Access Management (UAM) repository. This repository contains essential documentation and resources about UAM operations. Below is an overview of the structure and content of this repository.

## Contents

### 1. Document [`General guidelines of cryptography`](./01-General_CRY_guidelines.md)

**Description**: General requirements about the implementation of cryptography into SNET environment

**Content**:

- Secret management (and use of vault)
- Specific implementation guidelines (IPSEC, )

### 2. Additional content

- [`Guidelines`](https://eceuropaeu.sharepoint.com/:f:/t/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Ev2TgjKa6htCteYeqty8XAEB-cn4au57oGdwKnR0Ft4bXg)

## Contributing

Suggestions for improving these guides are welcome. Please create issues or Merge Request.
