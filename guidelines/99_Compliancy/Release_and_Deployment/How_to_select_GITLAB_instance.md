# Guideline for Selecting the Appropriate GitLab Instance

This document provides clear criteria for selecting between the **Private GitLab Instance** and the **Open-Source GitLab Instance** when creating or managing repositories.

---

## **1. Purpose of Each GitLab Instance**

### **Private GitLab Instance**
- **Purpose:**
  - Dedicated to hosting **private repositories** that contain proprietary, confidential, or sensitive information.
- **Use Cases:**
  - Internal tools, scripts, and automation.
  - Proprietary software development.
  - Internal Projects or restricted access.

### **Open-Source GitLab Instance**
- **Purpose:**
  - Dedicated to hosting **open-source repositories** that are intended to be publicly accessible and shared with the community.
- **Use Cases:**
  - Contributions to open-source projects.
  - Public-facing tools, utilities, or templates.
  - Documentation or resources designed for external users.

---

## **2. Decision Criteria**

### **When to Use the Private GitLab Instance**
1. **Confidentiality:**
   - The repository contains proprietary, sensitive, or confidential data.
   - Example: DIGIT-C4 specific tools, infrastructure code, or internal workflows.

2. **Access Control:**
   - The repository should only be accessible to EC account only and authorized members.

3. **Compliance Requirements:**
   - The project must comply with internal security or EC policies.

4. **Internal-Only Usage:**
   - The repository is not intended to be shared publicly.
   - Example: Internal scripts for automation or private libraries.

---

### **When to Use the Open-Source GitLab Instance**
1. **Public Accessibility:**
   - The repository is intended to be publicly available for external users.
   - Example: Open-source libraries, public APIs, or documentation.

2. **Community Contributions:**
   - The project invites contributions or collaboration from the open-source community.
   - Example: Netbox plugins to share and request feedback and merge requests from contributors or netbox developers.

3. **No Confidential Information:**
   - The repository does not contain any proprietary or sensitive information.

4. **Compliance with Open-Source Licensing:**
   - The code is released under an approved open-source license (for example, MIT, GPL, Apache).

---

## **3. Repository Creation Workflow**

### **Step 1: Evaluate the Project**
- Determine if the project contains confidential or sensitive information.
- Assess whether the project is intended for internal use or public distribution.
- Request DIGIT C4 approval for any open-source repository.

### **Step 2: Choose the Appropriate Instance**
- Use the **Private GitLab Instance** for proprietary or internal projects.
- Use the **Open-Source GitLab Instance** for public and community-driven projects.

### **Step 3: Apply Access and Licensing Standards**
- **Private Repositories:**
  - Restrict access to authorized users only.
  - Clearly define the purpose and usage of the repository in the `README.md` file.
- **Open-Source Repositories:**
  - Include an open-source license in the `LICENSE` file.
  - Ensure compliance with community contribution standards.

---

## **4. Examples**

### **Scenario 1: Internal Automation Scripts**
- **Instance:** Private GitLab
- **Reason:** Contains company-specific automation processes.

### **Scenario 2: Open-Source Library**
- **Instance:** Open-Source GitLab
- **Reason:** The library is intended for public use and contributions.
- **Example:** A **NetBox plugin** designed to extend NetBox’s functionality, shared publicly to benefit the community and invite external contributions.

### **Scenario 3: Proprietary Software Development**
- **Instance:** Private GitLab
- **Reason:** The project includes proprietary algorithms and business logic.

### **Scenario 4: Public Documentation Template**
- **Instance:** Open-Source GitLab
- **Reason:** Designed to be shared publicly with external users.

---

## **5. Best Practices**

1. **Verify Access Permissions:**
   - Regularly audit repository access on the private instance.
   - Ensure open-source repositories remain accessible to the public.

2. **Use Standardized Templates:**
   - Include a clear `README.md`, `CONTRIBUTING.md`, and `LICENSE` file in all repositories.
   - Use existing project templates to maintain consistency.

3. **Avoid Mixing Public and Private Data:**
   - Ensure no sensitive information is accidentally committed to open-source repositories.

4. **Compliance and Security:**
   - Review code for compliance with legal and regulatory requirements.
   - Conduct security reviews before publishing repositories to the open-source instance.

---

## **6. Tools and Resources**

- **Internal GitLab Instance:** [Private GitLab Link](https://sdlc.webcloud.ec.europa.eu)
- **Open-Source GitLab Instance:** [Open-Source GitLab Link](https://code.europa.eu)
- **Repository Templates:** Access predefined templates for both private and open-source repositories in your GitLab instances.

---

## **7. Escalation or Questions**

If you are unsure which instance to use, contact:
- **Service Delivery Manager:**
- **Lead Architect:** 

---

By following this guideline, you can ensure appropriate use of the GitLab instances while maintaining security and promoting collaboration where necessary.
