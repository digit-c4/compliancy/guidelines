# Introduction to Compliancy by Design (CbD)

## Preamble

From an IT point of view, Security by Design (SbD) and Data Protection by Design (DPbD) are doing the same thing but in a slightly different way:

- Security by Design (SbD) defines global and industrialised requirements of technical security over every category of assets (data, systems, architectures, …)
- Data Protection by Design (DPbD) defines security controls over personal data and systems processing personal data (these must be translated into technical security requirements).

So, it has been considered to build a “process” regrouping both Security by Design (SbD) and Data Protection by Design (DPbD)  called Compliancy by Design.

This document intend to present what is Compliancy by Design.

## First of all, what is compliancy?

The purpose of compliancy is to adhere to both internal policies and procedures, along with regulations to avoid risks.

**The following frameworks are applicable to NMS III:**

- Contract (CFT)
- EC standards (quality and cybersecurity)
- ITIL
- Regulation 2018/1725
- Complementary (GDPR, ISO27001; ISO27002)

**The points of controls are:**

- Compliance with contract, organizational policies, laws and regulations
- Process compliance
- Consistent information and communication
- Reporting accuracy and consistency across business units and departments
- Documented evidence of compliance or non-compliance

## What is Compliancy by Design

Compliancy by design is a strategy which aims to deploy the compliancy at early stage as an integral part of processes, procedures and assets.

In the same way that the SbD is the engine of application of the ISMS measures and requirements, the CbD is the engine of application of compliancy including "ISMS"(Information Security Management System), "DPbD"(Data Protection by Design) or "BCM"(Business Continuity Management)...

The outputs of the various frameworks (ISMS, DP, BCM…) are regrouped and formalised to standardise by compliancy by design. So the result is a unique and rationalise way to implement all the requirements of compliancy.

![CBD Overview](./Static_images/CBD_Overview.png)

## Benefits targeted by Compliancy by Design

- Consolidation: Rationalisation and standardisation of requirements, guidelines and patterns from the various sources
- Standardisation: The “compliancy” outputs are templates, references configurations, structured guidelines …
- Applicability: Translation of the outputs into technical configuration, checklists, chapters of SDP…
- Industrialisation: Industrialisation of the approach and deployment process of compliancy
- "Mutualization"(Pooling): Rationalisation of the tooling, KPIs and reports
- Rationalisation: Consolidation of the governance and continuous improvement of the compliancy

## Principles applied

In the context of the Compliancy by design, the principles are a set of rules defining the strategy of Compliancy by design and acceptances criteria for the inputs, the processing and the results of this strategy.

- **Proactive not reactive; preventive not remedial:** preventing risk to materialise, or incidents to happen. The Compliancy by Design comes before negative events not after.
- **Compliancy as the default setting:** Services, processes, operations, systems shall be instantiated in such a way that no added operation or control must be engaged after first implementation to ensure the required level of Compliancy.
- **Compliancy embedded into design:** Consider the Compliancy into the workflows, at early stage of a project, at every level of a service or an architecture or inside the system itself as an essential part.
- **Full functionality:** Compliancy by Design tries to ensure Compliancy and functionality not like an opposition but like a win-win goal. The Compliancy shall not be treated like a limitation but like a way to supply a service in a best way considering risks (disruption, disclosure, regulatory …).
- **End-to-end Compliancy:** The efficiency of the Compliancy is related to its weakest link, in this perspective, a consistent level of Compliancy shall be considered not only locally but through all the service, architecture, process…
- **Unitary and self-standing requirements:** To avoid misunderstanding or dilemmas of interpretation a requirement shall be short and respect an INVEST:

  - “I”: Independent (of all others)
  - “N”: Negotiable (not a specific contract for features)
  - “V”: Valuable (or vertical)
  - “E”: Estimable (to a good approximation)
  - “S”: Small (to fit within an iteration)
  - “T”: Testable (in principle, even if there isn’t a test for it yet)

  => The compliance to the requirement shall be answerable by a simple “yes” or “no”.

- **Risk oriented, the perfect is the enemy of the good:** Ensure that the measures to apply are proportionate to the needs of Compliancy. Compliancy aims to mitigate risks and ensure that operations are performed in a trusted and robust environment (from a Compliancy point of view). In addition, every Compliancy measure needs to be controlled, supported and tested and so for each added measure the workload and the risk of inefficiency increase mathematically.
- **Self-assessment and challenge of any change:** To some extent, teams should be able to perform self-assessments on the security “needs” and so rely on guidelines or procedures provided by the security by design and the ISMS shall be involved at early stage to challenge the projects or changes from a cybersecurity perspective
- **Visibility, transparency and lucidity:** Nothing is perfect and there is always a margin of improvement this statement is the best input for the continuous improvement
- **Security at every level of the organisation:** Compliancy is not just a matter of technology or operations it is also a concern to be handled by the management and into the decision processes.

`NOTA: Even if all the requirements shall be considered by the various activities of the Compliancy by Design, each activity of the compliancy by Design has focuses on principles.`

## Activities of the Compliancy by Design

The process is composed of 6 main activities, each activity consume and produce deliverables as illustrated in the following schema

![Compliancy Operations](./Static_images/CBD_Operations.png)

The compliancy by design process is built in order to implement continuous improvement via the 6 activities.

- **Definition:** aims to maintain a basis of compliancy by defining the referential of compliancy requirements, guidelines, patterns and controls.
- **Build:** On the bases of the outputs of the activity of design, this activity supplies operational or technical solutions to implement the compliancy. To ensure the relevance, feasibility and continuity of the solutions, this activity is performed in close collaboration with the architects and the service delivery manager.
- **Deployment:** This activity ensures that the adequate processes (design coordination, project management, change management, release and deployment management, service validation and testing) are engaged to deploy compliancy or that the various processes of design and transition considers the compliancy.
- **Assurance:** The aims of this activities are:

  - Monitoring the status of compliancy
  - Collect the verbatim of the various teams
  - Produce the reports and the KPIs
  => Consolidate a lucid view on the status of compliancy within scope of NMSIII

- **Improvement:** analyse the existing situation of the goal in terms of Compliancy and produce gap assessments.
Gap assessments are then analysed to define actions of remediation and the organised and plan via Improvement plans

- **Governance:** oversees and manages the whole process of compliancy by design via communication of KPIs, lead of committees and framework lifecycle.

## When to consider compliance

![When to consider Compliance](./Static_images/CBD_when_to_consider.png)

## Roles and responsibilities

![Roles and Responsibilities](./Static_images/CBD_Roles_and_Responsibilities.png)

## What to apply and implement

The Moscow table is the application of the Compliancy guidelines

- The guidelines are split in requirements for which the scope is mostly applicable
- Each requirement is qualified in terms of priority Must, Should, Could and Won’t have
- It is also defined if a requirement can be supported by an additional service.

![Sample of Moscow Table](./Static_images/CBD_Moscow_illustration.png)

## Going further

[Compliancy by Design framework document](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Compliancy%20by%20Design%20--%20Security%20by%20Design/00-Validated/01-Framework_Process/NMS-III%20-%20Compliancy%20by%20Design%20-%20Framework%20-%20V1.0.pdf?csf=1&web=1&e=WmAzqs)

[Compliancy by design process document](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Compliancy%20by%20Design%20--%20Security%20by%20Design/00-Validated/01-Framework_Process/NMS-III%20-%20Compliancy%20by%20Design%20-%20Process%20-%20V1.0.pdf?csf=1&web=1&e=DpZyng)

[How to use MOSCOW table?](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/05-MoSCoW/Awareness-CBD-Moscow_HowToUse.pptx?d=wd172b5a6fdab4a49804dde00892d69b1&csf=1&web=1&e=FmH7eA)
