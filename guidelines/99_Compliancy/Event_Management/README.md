
# Event Management Documentation

This directory contains comprehensive **HOW TO** guides to support the classification and management of events detected in the EMC SMARTS monitoring system. The guides are tailored for NOCA (Level 1), NNDOE (Level 2), and Architect (Level 3) teams within the organization.

## Contents

### 1. **HOW TO:[`Write a Comprehensive SOP for EMC SMARTS Events`](how_to_smartdb.md)**
- **Description**: Provides detailed instructions for creating a Standard Operational Procedure (SOP) to handle events efficiently.
- **Key Features**:
  - Explains how to structure SOPs for Exception, Warning, and Information events.
  - Includes templates for initial and final escalation messages.
  - Defines step-by-step activities for NOCA and NNDOE teams to analyze and resolve issues.
- **Use Case**:
  - Accessible by NOCA via the right-click feature in EMC SMARTS to retrieve matching SOPs for detected events.

### 2. **HOW TO:[`Classify an Event by Impact and Priority`](how_to_classify_event.md)**
- **Description**: Details the process for evaluating and classifying events into Exception, Warning, or Information categories.
- **Key Features**:
  - Describes the classification criteria based on impact (service criticality, scope, severity) and priority (time sensitivity, SLA impact, user experience).
  - Includes a classification matrix for consistent decision-making.
  - Provides real-world examples to clarify the classification process.
- **Use Case**:
  - Assists teams in determining the appropriate level of response to events based on their severity and urgency.

  ### 3. **HOW TO:[`Add, modify or remove event thresholds`](How_to_manage_event_thresholds.md)**
- **Description**: Details the process for Add, modify or remove thresholds for monitoring tool.
- **Key Features**:
  - Describes the  criteria to add, modify or remove thresholds for Smarts configuration.
  - Includes instructions with all necessary details for SNMP traps and Syslog-NG configuration.
  - Provides real-world examples to clarify the classification process.
- **Use Case**:
  - Assists teams in determining the appropriate level of response to events based on their severity and urgency.
  
  ### 4. **HOW TO:[`Configure monitoring tools in EC environment`](How_to_configure_monitoring.md)**
- **Description**: Details the process for Add, modify or remove monitoring for a particular device.
- **Key Features**:
  - Describes the criteria to add, modify or remove device to/from monitoring.
  - Includes instructions with all necessary details to manage monitoring for a device.
  - Provides real-world examples to clarify the process.
- **Use Case**:
  - Assists architects and CI owners in the monitoring tools configuration.
  ---
## How to Use
1. **Review the Guides**:
   - Open each HOW TO understand the processes and guidelines for SOP creation and event classification.
2. **Implement in Workflow**:
   - Use the instructions to improve SOP quality and ensure accurate event classification in daily operations.
3. **Adapt to Changes**:
   - Update the HOW TO guides as processes evolve or new use cases emerge.

## Directory Structure
```
.
├── README.md             # Overview of the directory and its contents
├── how_to_smartdb.md  # Guide for writing Standard Operational Procedures
├── HOW_TO_Classify_Events.md # Guide for classifying events by impact and priority
├── HOW_TO_manage_event_thresholds.md # Guide for configuring or modify event's thresholds
├── HOW_TO_configure_monitoring.md # Guide for configuring monitoring tools
```

## Contributions
Suggestions for improving these guides are welcome. Please create issues or Merge Request.

