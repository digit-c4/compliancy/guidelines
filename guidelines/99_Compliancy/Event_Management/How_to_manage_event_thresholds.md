# HOW TO: configure thresholds for an event monitoring? 

This guide outlines the steps to prepare the correct configuration of the monitoring thresholds which are a way of initially filtering the vast amount of monitoring data which can be collected through the monitoring tools. Threshold values should be defined with a degree of care to prevent too many responses being generated and overwhelming the resources, human and machine, ability to respond to them.

---

## **1. Event Classifications**
Each CI needs to have 3 monitoring thresholds levels: 
### **1.1 Exception**
- **Definition**: Represents critical events announcing that a service is down or significantly affected.
- **Key Characteristics**:
  - Immediate action required.
  - A critical event notification must be sent immediately, and incident creation must be triggered (automatically) to restore a service back to the operational level.
  - High impact on users, systems, or business operations.

- **Examples**:
  - A router, a switch, a firewall, a Access Point, a DNS server is offline.
  - Network connectivity is lost to a key service.

---

### **1.2 Warning**
- **Definition**: Indicates that a service is at risk of degradation but still operational.
- **Key Characteristics**:
  - An event is generated when a device or service is approaching an agreed threshold. A warning notification triggers some maintenance actions to prevent a critical event in the nearest future.   
  - Medium priority for action.
  - Potential for impact if no intervention occurs.
  - Proactive monitoring required.
- **Examples**:
  - Disk usage on a critical server exceeds 80%.
  - Network latency is increasing.
  - Network packet loss is increasing
  - Device CPU is increasing

---

### **1.3 Information**
- **Definition**: Provides non-critical updates or insights about the system without indicating stress.
- **Key Characteristics**:
  - Logs should be collected and stored for further analysis, but notification is not required. 
  - No immediate action needed.
  - For awareness only.
  - Often informational logs or minor anomalies.
- **Examples**:
  - A backup completed successfully.
  - A routine system update was performed.

---

## **2. When to trigger notification**
The notification must be triggered every time a **warning** or an **exception** threshold would be reached. 

## **3. Event worth collecting**
Changes of state for services and service components occur continuously in the IT environment. To properly handle and respond to the stream of data, it is necessary to filter and categorize the incoming information. As the indicator’s data worth collecting, we can take: 
1. **Hardware indicators such as:**
   - CPU utilization
   - RAM utilization
   - Disk drive free space
   - Disk read/write speed
   - Network utilization 
   

2. **Software indicators:**
   - Uptime
   - Restart
   - Downtime
   - Errors

3. **Security:**
   - Failed login attempts within a predefined period
   - User behavior

4. **Root cause analysis finding:**   
   - A new indicator must be set 

**NOTE**: All the new indicators must be consulted and approved by the PPO.

---

## **4. Step by step instructions to prepare proper event alert for SNMP Trap**
The kanban-board card with all the necessary data must be prepared for every type of known event in the infrastructure to make sure it is detectable.  
It must have the necessary data to identify the CI, the CI owner, identify the error and the responsible service team. If there is a known fix [the SOP](https://code.europa.eu/digit-c4/compliancy/guidelines/-/blob/main/guidelines/99_Compliancy/Event_Management/how_to_smartdb.md?ref_type=heads) must be also attached.


**1. To add or update the threshold for a SNMP Trap (device -> SMARTS) the dedicated architect must:** 

  - find the [MIB file](https://en.wikipedia.org/wiki/Management_information_base) configuration file on the server or the device
  - review the MIB file and prepare a list of alerts worth to be monitored (as described in point 3.) and collected via monitoring tool
  - create a [kanban-board](https://globalntt.leankit.com/board/31512192876883) card and assign it to the SYS Squad
  - attach the list to the kanban-board card
  - Update the severity of a specific event and/or the threshold level assigned 
  - SYS Squad to prepare, implement and test the monitoring on non-prod instance in case of any unexpected behaviour
  - SYS Squad to configure the monitoring for the device in prod instance
  - SYS Squad to inform the requester and close the kanban-board card.



 **2. To remove the threshold for a SNMP Trap (device -> SMARTS) the dedicated architect must:**
   - Create a kanban-board card 
   - Assign the card to SYS Squad queue
   - Name it _**Remove threshold for SNMP trap**_
   - Provide the event name 
   - Provide the justification why the threshold should be removed

**3. Once applied by the SYS agent obliged to test a new or updated threshold in a lab environment if applicable.**

**4. Once tested a new thresholds can be turned on in PROD environment.**

---
## **5. Step by step instructions to prepare proper event alert for SysLog Messages**
For Syslog-NG three actions are possible: 
  - To add new,
  - To modify, 
  - To remove an alert threshold.

**1. To add the threshold for a Syslog Messages (device -> Syslog-NG -> SMARTS) the dedicated architect must:** 
   - Create a kanban-board card 
   - Assign the card to SYS Squad queue
   - Name it _**New threshold for Syslog-NG**_ 
   - Architect attach to the card an example of logs in Syslog-ng 
   - Describe the event
   - SYS Squad to push the new configuration to the monitoring solution

**2. To update the threshold for Syslog Messages (device -> Syslog-NG -> SMARTS) the dedicated architect must:** 
   - Create a kanban-board card 
   - Assign the card to SYS Squad queue
   - Name it _**Update threshold for Syslog-NG**_
   - provide the event name and the associated severity
   - attach the screenshot of an example of logs in Syslog-ng 
   - SYS Squad to push the new configuration to the monitoring solution
   
**3. To remove the threshold for Syslog Messages (device -> Syslog-NG -> SMARTS) the dedicated architect must:**
   - Create a kanban-board card 
   - Assign the card to SYS Squad queue
   - Name it _**Remove threshold for Syslog-NG**_
   - Provide the event name 
   - attach the screenshot of an example of logs in Syslog-ng 
   - SYS Squad to remove the configuration from the monitoring solution

**4. Once applied by the SYS agent obliged to test a new or updated threshold in a lab environment if applicable.**

**5. Once tested a new thresholds can be turned on in PROD environment.** 

---

### **6. Useful links:**
1. **List of known alerts can be found here: [SMARTS Alerts DB - All Items](http://perceval.net1.cec.eu.int/kate/Lists/SMARTSAlertsDB/AllItems.aspx)**
2. **List of critical alerts is stored here: [Critical events](https://eur01.safelinks.protection.outlook.com/GetUrlReputation)**

