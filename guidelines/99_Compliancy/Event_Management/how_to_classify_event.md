# HOW TO: Classify an Event into Exception, Warning, or Information

This guide outlines the steps to evaluate and classify an event in the event management process by considering its **impact** and **priority**.

---

## **1. Understanding Event Classifications**

### **1.1 Exception**
- **Definition**: Represents critical events announcing that a service is down or significantly affected.
- **Key Characteristics**:
  - Immediate action required.
  - High impact on users, systems, or business operations.

- **Examples**:
  - A router, a switch, a firewall, a Access Point, a DNS server is offline.
  - Network connectivity is lost to a key service.

---

### **1.2 Warning**
- **Definition**: Indicates that a service is at risk of degradation but still operational.
- **Key Characteristics**:
  - Medium priority for action.
  - Potential for impact if no intervention occurs.
  - Proactive monitoring required.
- **Examples**:
  - Disk usage on a critical server exceeds 80%.
  - Network latency is increasing.
  - Network packet loss is increasing
  - Device CPU is increasing

---

### **1.3 Information**
- **Definition**: Provides non-critical updates or insights about the system without indicating stress.
- **Key Characteristics**:
  - No immediate action needed.
  - For awareness only.
  - Often informational logs or minor anomalies.
- **Examples**:
  - A backup completed successfully.
  - A routine system update was performed.

---

## **2. Steps to Classify an Event**

### **2.1 Assess Impact**
1. **Identify the Affected Service**:
   - Which service or system does the event relate to?
   - Is the service critical to business operations or end users?

2. **Evaluate the Scope**:
   - How many users or systems are impacted?
     - **High**: Affects all users or a critical system (Exception).
     - **Medium**: Affects some users or non-critical systems (Warning).
     - **Low**: No significant impact (Information).

3. **Determine the Severity**:
   - **Complete Failure**: Service is down (Exception).
   - **Degraded Performance**: Service is slow or partially available (Warning).
   - **Operational**: Service is functioning as expected (Information).

---

### **2.2 Assess Priority**
1. **Time Sensitivity**:
   - Does the event require immediate action to prevent further damage?
     - **Yes**: High priority (Exception).
     - **No**: Medium or low priority (Warning or Information).

2. **SLA Impact**:
   - Will the event cause a breach of SLA if not addressed quickly?
     - **Yes**: High priority (Exception or Warning).
     - **No**: Low priority (Information).

3. **User Experience**:
   - Is the event directly impacting the end-user experience?
     - **Yes**: Higher priority (Exception or Warning).
     - **No**: Lower priority (Information).

---

### **2.3 Combine Impact and Priority**
Use the following matrix to classify the event:

| **Impact**          | **Priority**          | **Classification** |
|---------------------|-----------------------|---------------------|
| **High**            | **High**             | **Exception**       |
| **Medium**          | **Medium or High**   | **Warning**         |
| **Low**             | **Low**              | **Information**     |

---

## **3. Examples of Event Classification**

### **Example 1**: DNS Offline
- **Impact**: High (Critical service is down for all users).
- **Priority**: High (Immediate action required to restore service).
- **Classification**: **Exception**.

---

### **Example 2**: Disk Usage at 85% on Web Server
- **Impact**: Medium (Risk of service degradation if not addressed).
- **Priority**: Medium (Proactive action required to prevent future issues).
- **Classification**: **Warning**.

---

### **Example 3**: Backup Completed Successfully
- **Impact**: Low (No service impact).
- **Priority**: Low (Informational only).
- **Classification**: **Information**.

---

## **4. Guidelines for Effective Event Classification**
1. **Consistency**: Use the same criteria for all events to ensure uniform classification.
2. **Documentation**: Record the classification reasoning for traceability.
3. **Reassess Regularly**: Reevaluate events as their impact or priority changes.
4. **Automation**: Implement automated rules in the monitoring system to classify common events based on predefined criteria.

By following this HOW TO, team members can classify events accurately, ensuring appropriate attention is given to critical issues while minimizing noise from less important events.
