# How to configure the monitoring 

This guide outlines the steps to prepare the correct configuration of the monitoring in the infrastructure and how to add/remove device from the monitoring


## **1. SNet components**

The SNet Monitoring Solution is composed of multiple parts, all connected to the CMDB.

  The general description of the components and the instruction for service owners or architects describing how to configure these tools can be found [here](https://intragate.ec.europa.eu/snet/wiki/index.php/System/Monitoring_Solution).
 

 ## **2. How to configure monitoring**
To prepare the correct list of messages to be monitored on the device, the dedicated architect or the CI owner must:
  - find the [MIB file](https://en.wikipedia.org/wiki/Management_information_base) configuration file on the server or the device
  - review the MIB file and prepare a list of alerts worth to be monitored (as described in point 3.) and collected via monitoring tool
  - create a [kanban-board](https://globalntt.leankit.com/board/31512192876883) card and assign it to the SYS Squad
  - attach the list to the kanban-board card
  - Update the severity of a specific event and/or the threshold level assigned 
  - SYS Squad to prepare, implement and test the monitoring on non-prod instance in case of any unexpected behaviour
  - SYS Squad to configure the monitoring for the device in prod instance
  - SYS Squad to inform the requester and close the kanban-board card.

## **3. Event worth collecting**
Changes of state for services and service components occur continuously in the IT environment. To properly handle and respond to the stream of data, it is necessary to filter and categorize the incoming information. As the indicator’s data worth collecting, we can take: 

**1. Hardware indicators such as:**
- CPU utilization
- RAM utilization
- Disk drive free space
- Disk read/write speed
- Network utilization 
   
**2. Software indicators:**
- Uptime
- Restart
- Downtime
- Errors

**3. Security:**
- Failed login attempts within a predefined period
- User behaviour

**4. Root cause analysis findings:**   
- A new indicator must be set 


**NOTE**: Ensure monitoring configurations are aligned with organizational priorities to avoid over-monitoring and alert fatigue.

## **4. When to add a device or service to the monitoring**

**1. New deployment**
  - Add devices or services immediately after deployment or go-live to ensure their performance, availability, and health are monitored from the start.

**2. Integration or new dependencies**
  - Add items that serve as dependencies for existing systems to monitor their impact on overall performance.

**3. Regulatory requirements**
  - Add devices to the monitoring for adherence to legal, regulatory, or audit standard. Example: GDPR. In that case all infrastructure that store, process or transit sensitive data must be monitored.

**4. Infrastructure changes**
  - Add devices after upgrades, migration, or changes in architecture that introduce new components.

**5. Incident prevention**
  - Include items identified as potential points of failure during risk assessment or post-incident reviews.

**6. Service level agreements (SLA)**
 - Add devices critical or system critical to meeting SLAs, ensuring performance and availability targets are met.


## **5. How to classify the events**

Each event has to be classified accordingly to the rules described in the guideline [how to classify event](/guidelines/99_Compliancy/Event_Management/how_to_classify_event.md)

## **6. How to remove the device from monitoring**
 
In order to remove a device from the monitoring solution, follow the steps from the [instruction](https://intragate.ec.europa.eu/snet/wiki/index.php/System/Monitoring_Solution), section **Remove a device**.

## **7. When to remove the device from monitoring**
**1. Decommission or retirement**
  - Remove a device from the monitoring when it is permanently decommissioned or retired from use.

**2. Replacement or migration**
  - If a device is replaced or its services are migrated to a new system, ensure its monitoring is disabled after the transition is complete.

**3. Change in scope**
  - Exclude devices that no longer fall within the scope of monitored systems, such as non-critical or unused assets.

**4. Consolidation of monitoring**
  - If monitoring responsibilities are transferred to another system or tool, the original device can be removed to avoid duplicate alerts.

**5. Prolonged disconnection**
  - For the devices that are permanently disconnected or offline without plans for reactivation.
**6. Policy updates**
  - Remove devices based on changes in organizational monitoring policies or compliance requirements.

**NOTE**: Always ensure proper documentation and approval before removing a device from monitoring to avoid unintended gaps in coverage. 

---