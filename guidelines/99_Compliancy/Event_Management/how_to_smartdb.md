# HOW TO: Write a Comprehensive SOP for EMC SMARTS Events

This guide provides instructions for creating a Standard Operational Procedure (SOP), located [here](http://perceval.net1.cec.eu.int/kate/Lists/SMARTSAlertsDB/AllItems.aspx) to handle events detected in the EMC SMARTS monitoring system. The SOP will ensure NOCA (Level 1), NNDOE (Level 2), and Architects (Level 3) can efficiently analyze, resolve, and escalate issues.

## **1. SOP Structure**

### **1.1 Event Details**
- Clearly define the **Event Type** (Exception/Warning/Information).
- Include the **Event Description**: A concise explanation of the issue.
- Mention the **Detected Time**: When the event was first identified.

---

### **1.2 Escalation Message Template (Initial)**
Create a structured escalation message for NOCA to communicate the issue:
- **Start Date/Time**: When the issue was detected.
- **Impact**: Describe the impact on the system, users, or business processes.
  - Example: "The affected system is currently experiencing degraded performance, service XXXX is affected."
- **Escalation Path**:
  - If NOCA requires NNDOE involvement: Provide NNDOE contact details.
  - If NNDOE escalates to Architects: Specify escalation criteria.

---

### **1.3 NOCA Activities**
Provide clear, step-by-step instructions for NOCA to analyze and address the issue:
1. Check the SMARTS console for associated event details and logs.
2. Perform predefined diagnostic steps (for example, search query in splunk, link to grafana graph or any other sources).
3. Perform a set of action to resolve the situation if NOCA as skills and access right
4. Record observations in the ticketing system.
5. Escalate to NNDOE if:
   - Exception events remain unresolved after initial steps.
   - Warnings persist for over the defined SLA time.

---

### **1.4 NNDOE Activities**
Outline activities NNDOE should perform to analyze and resolve the issue:
1. Review NOCA findings and ticket updates.
2. Access system logs, metrics, or configurations related to the event.
3. Perform root cause analysis based on predefined tools and methods.
4. Implement resolution actions or document findings for escalation to Architects.

---

### **1.5 Escalation Message Template (Final)**
Provide a structured template for closing or escalating the issue:
- **Close Message** (when resolved):
  - "Event resolved as of [Time/Date]. Root cause identified as [Root Cause]. Actions taken: [Summary of Actions]."
- **Escalation Message** (if unresolved):
  - "Event requires Level 3 assistance. NNDOE findings: [Summary]. Escalated to Architect at [Time/Date]."

---

## **2. Example SOP**

### **2.1 Event Details**
- **Event Type**: Exception
- **Event Description**: Disk space threshold exceeded on Server-X.
- **Detected Time**: 03-Dec-2024, 14:00.

### **2.2 Initial Escalation Message**
- **Start Date/Time**: 03-Dec-2024, 14:00.
- **Impact**: Disk space on Server-X is critically low. Users may experience service-X downtime.
- **Escalation Path**: Escalate to NNDOE if disk space is not cleared within 1 hour.

### **2.3 NOCA Activities**
1. Verify the current disk space status using SMARTS metrics.
2. Notify application owners of potential impact.
3. Attempt to clear temporary files (use playbook `ansible-playbook -i inventory disk_usage.yml` and `ansible-playbook -i inventory clean_disk.yml`).
4. If disk space remains below 5%, escalate to NNDOE.

### **2.4 NNDOE Activities**
1. Check SMARTS for historical disk usage trends.
2. Log into Server-X and review large files or processes consuming disk space.
3. Clear non-essential files or move data to secondary storage.
4. Update the ticket with findings and resolution steps.

### **2.5 Final Escalation Message**
- **Close Message**:
  - "Disk space issue on Server-X resolved as of 03-Dec-2024, 15:30. Cleared 50 GB of temporary files. Root cause: unmonitored log growth."
- **Escalation Message** (if unresolved):
  - "Unable to resolve disk space issue on Server-X. Escalating to Architect at 03-Dec-2024, 15:30. NNDOE findings: Temporary files cleared, but additional space required."

---

## **3. Guidelines for SOP Excellence**
- **Clarity**: Use concise language and clear step-by-step instructions.
- **Accessibility**: Ensure the SOP is accessible via right-click in SMARTS.
- **Relevance**: Tailor each SOP to the specific event type and priority.
- **Review**: Update SOPs regularly to reflect system changes or improved methods.

## **3. Where to store SOPs linked to Event management and monitoring**
- **Dedicated Location**: [SMARTS Alert DB](http://perceval.net1.cec.eu.int/kate/Lists/SMARTSAlertsDB/AllItems.aspx).


By following this guide, Level 2 and Level 3 personnel can create SOPs that enable NOCA to handle events efficiently and escalate when needed.
