# HOW TO: Create a Test Plan for Infrastructure Changes

This document outlines the steps to create a robust test plan that ensures features delivered to clients remain operational after changes to infrastructure components, such as Reverse Proxies, DNS, Load Balancers, Routers, Switches, Cisco CUBEs, Audiocode SBCs, Firewalls, Pexip, or Expressway systems etc.

---

## Objectives of the Test Plan

1. Validate that changes do not disrupt existing functionality.
2. Ensure features and services are operational post-change.
3. Minimize downtime by automating tests where possible.
4. Provide a repeatable, structured testing framework for all changes.

---

## Test Plan Components

### 1. **Pre-Test Preparation**
- **Identify Services and Features:**
  - List the services/features impacted by the change.
  - Example: Reverse proxy routing rules, DNS resolution, firewall rule updates.
  Here's the updated **HOW TO: Create a Test Plan for Infrastructure Changes**, with an expanded **Baseline Collection** section:

---

# HOW TO: Create a Test Plan for Infrastructure Changes

This document outlines the steps to create a robust test plan that ensures features delivered to clients remain operational after changes to infrastructure components, such as Reverse Proxies, DNS, Load Balancers, Routers, Switches, Cisco CUBEs, SBCs, Firewalls, Pexip, or Expressway systems. Automation through tools like Ansible and APIs is emphasized to streamline testing.

---

## Objectives of the Test Plan

1. Validate that changes do not disrupt existing functionality.
2. Ensure features and services are operational post-change.
3. Minimize downtime by automating tests where possible.
4. Provide a repeatable, structured testing framework for all changes.

---

## Test Plan Components

### **Baseline Collection**
Baseline collection is a critical step to capture the system’s state and functionality before applying changes. This ensures there is a reference point for validating success or identifying issues after the change.

#### **Key Steps:**

1. **Identify Metrics to Collect:**
   - Define the data to collect based on the component being updated. Examples:
     - **Reverse Proxy:** Active routes, SSL certificates, and backend server status.
     - **DNS:** Current records, TTL values, and server response times.
     - **Load Balancers:** Active pools, backend health, session persistence settings.
     - **Routers/Switches:** Current routing tables, VLAN configurations, and interface statuses.
     - **Cisco CUBE/SBCs:** Active call flows, registered endpoints, and SIP trunk statuses.
     - **Firewalls:** Current rules, NAT translations, and VPN sessions.
     - **Pexip/Expressway:** Registered endpoints, active conferences, and traversal rules.

2. **Use Automation to Collect Data:**
   - Create Ansible playbooks or scripts to collect relevant baseline data.
   - Examples:
     - Reverse Proxy (Nginx/Apache):
       ```yaml
       - name: Fetch reverse proxy configurations
         command: cat /etc/nginx/nginx.conf
         register: nginx_conf
       - debug:
           msg: "{{ nginx_conf.stdout }}"
       ```
     - DNS Records:
       ```bash
       dig +nocmd example.com any +multiline +noall +answer > dns_baseline.txt
       ```
     - Load Balancer Pools:
       ```yaml
       - name: Fetch backend pool statuses
         uri:
           url: "https://<load-balancer-api>/v1/pools"
           method: GET
           headers:
             Authorization: "Bearer <token>"
         register: pool_status
       - debug:
           msg: "Pool Status: {{ pool_status.json }}"
       ```

3. **Store Baseline Data:**
   - Save collected baselines in a secure, accessible location (for example, GitLab repository).
   - Organize the data by component, date, and version to facilitate comparisons.

4. **Document the Baseline:**
   - Include in the documentation:
     - The commands/scripts used.
     - The location of stored baseline data.
     - Any observed anomalies or deviations in the baseline state.

5. **Validate the Baseline:**
   - Ensure all critical data has been collected and is accurate.
   - Cross-check with team members for completeness.

##### **Example Baseline Documentation Format:**
```markdown
### Baseline Data for Reverse Proxy
- **Date Collected:** YYYY-MM-DD
- **Collected By:** John Doe
- **Configuration File Path:** `/etc/nginx/nginx.conf`
- **Active Routes:**
  - Route 1: /api -> Backend A
  - Route 2: /web -> Backend B
- **SSL Certificates:** Verified, expiry 2025-01-01
- **Storage Location:** `gitlab-repo/baselines/reverse-proxy/YYYY-MM-DD/`
```

- **Prepare Test Environment:**
  - Ensure test scripts and automation tools (for example, Ansible, Postman) are ready.
  - Test the rollback process in case of failure.

---

### 2. **Define Test Scenarios**
For each component, define test scenarios relevant to the change:

#### Reverse Proxy
- Validate routing rules for HTTP/HTTPS traffic.
- Test SSL termination and certificate validation.
- Confirm backend server availability.

#### DNS
- Check resolution for updated DNS records.
- Validate DNSSEC configurations if applicable.
- Test failover behavior for DNS services.

#### Load Balancers
- Validate load distribution across backends.
- Test health checks for all backend servers.
- Verify session persistence and SSL termination.

#### Routers/Switches
- Test connectivity for critical VLANs or subnets.
- Validate routing updates and failover behavior.
- Verify QoS policies, if applicable.

#### Cisco CUBE
- Test call routing policies and dial plans.
- Validate SIP signaling and media flow.
- Confirm redundancy and failover scenarios.

#### SBC AudioCodes
- Validate inbound and outbound SIP traffic.
- Test codec negotiation and NAT traversal.
- Verify security features like TLS/SRTP.

#### Firewalls
- Validate newly implemented rules (for example, allow/deny).
- Test application-specific traffic (for example, HTTP, FTP).
- Verify NAT and VPN configurations.

#### Pexip or Expressway
- Validate conference call connections.
- Test endpoint registration and connectivity.
- Verify federation and traversal capabilities.

---

### 3. **Automated Testing**
- **Use Ansible for Automation:**
  - Write playbooks to perform repetitive testing tasks, such as:
    - Checking service status.
    - Validating configurations.
    - Sending test traffic.
  - Example:
    ```yaml
    - name: Validate reverse proxy routing
      uri:
        url: "http://proxy.test.com/path"
        status_code: 200
    ```
    ```yaml
    - name: Test Pexip Conference Call
        hosts: localhost
        tasks:
            - name: Authenticate with Pexip API
            uri:
                url: "https://<pexip-server>/api/admin/authentication/access_token"
                method: POST
                headers:
                Content-Type: "application/json"
                body: |
                {
                    "username": "<admin-username>",
                    "password": "<admin-password>"
                }
                body_format: json
                register: auth_response
            failed_when: auth_response.status != 200

            - name: Extract Access Token
            set_fact:
                access_token: "{{ auth_response.json.token }}"

            - name: Create a Test VMR
            uri:
                url: "https://<pexip-server>/api/admin/configuration/virtual_meeting_rooms/"
                method: POST
                headers:
                Authorization: "Bearer {{ access_token }}"
                Content-Type: "application/json"
                body: |
                {
                    "name": "TestConference",
                    "alias": "test-vmr",
                    "service_type": "conference",
                    "pin": "1234",
                    "allow_guests": true
                }
                body_format: json
                register: vmr_response
            failed_when: vmr_response.status not in [200, 201]

            - name: Validate Participant Join
            uri:
                url: "https://<pexip-server>/api/admin/status/participants"
                method: GET
                headers:
                Authorization: "Bearer {{ access_token }}"
                register: participants_response
            failed_when: participants_response.status != 200

            - name: Check Participants in Test VMR
            debug:
                msg: >
                Participants in the Test VMR: 
                {{ participants_response.json.objects | selectattr('conference_alias', 'equalto', 'test-vmr') | list }}

            - name: Remove Test VMR
            uri:
                url: "https://<pexip-server>/api/admin/configuration/virtual_meeting_rooms/{{ vmr_response.json.id }}/"
                method: DELETE
                headers:
                Authorization: "Bearer {{ access_token }}"
                register: delete_response
            failed_when: delete_response.status != 204
    ```
    ```yaml
    - name: Validate BGP Configuration
    hosts: routers
    gather_facts: no
    tasks:
        - name: Verify BGP Neighbor Configuration
        ios_command:
            commands:
            - "show running-config | include router bgp"
            - "show ip bgp summary"
        register: bgp_config

        - name: Display BGP Configuration
        debug:
            var: bgp_config.stdout

        - name: Validate BGP Neighbor State
        ios_facts:
        register: bgp_facts

        - name: Check BGP Peers State
        debug:
            msg: >
            BGP Peers: 
            {{ bgp_facts.ansible_net_neighbors | 
                selectattr('state', 'equalto', 'Established') | 
                list }}

        - name: Verify Received Routes from Simulated Peer
        ios_command:
            commands:
            - "show ip bgp neighbors <simulated-peer-ip> received-routes"
        register: bgp_received_routes

        - name: Validate Received Routes
        debug:
            msg: "Received Routes: {{ bgp_received_routes.stdout }}"
    ```
- **Leverage APIs:**
  - Use APIs provided by the devices/services for testing.
  - Example: DNS query validation using `dig` or RESTful API.
    ```bash
    curl -X GET "https://api.dnsserver.com/v1/resolve?name=test.example.com"
    ```

---

### 4. **Execute Tests**
- **Follow a Structured Sequence:**
  - Test scenarios should follow this order:
    1. Basic connectivity checks.
    2. Functional tests for affected features.
    3. Performance or stress tests (if applicable).

- **Log Results:**
  - Automate logging of test results for traceability.
  - Use tools like Ansible, Jenkins, or a custom script to save logs.

---

### 5. **Post-Test Validation**
- **Rollback Validation:**
  - Simulate a rollback scenario and validate the system returns to the baseline state.

- **Comparison with Baseline:**
  - Compare post-change metrics and configurations to the baseline collected during preparation.

---

### 6. **Test Plan Documentation**
Include the following in your test plan document:
- **Change Description:** Overview of the changes made.
- **Test Scenarios:** List of tests performed with steps.
- **Automation Scripts:** Links or references to Ansible playbooks, API scripts, or tools used.
- **Results:** Summary of test outcomes.
- **Recommendations:** Actions based on test results (for example, proceed, rollback, or further testing).

---

### 7. **Example Playbook for DNS Testing**
```yaml
- name: Test DNS resolution
  hosts: localhost
  tasks:
    - name: Check DNS record
      shell: dig +short test.example.com @8.8.8.8
      register: dns_output

    - name: Verify DNS resolution
      debug:
        msg: "DNS resolved to {{ dns_output.stdout }}"
```
### 8. **Tooling**

```mermaid
graph LR
    SoftwareClient["Software Client (Ansible/Node-RED)"]
    Service["Service"]
    PositiveResponse["Positive Response"]
    NegativeResponse["Negative Response"]

    SoftwareClient -->|Request| Service
    Service -->|Positive Answer| PositiveResponse
    Service -->|Negative Answer| NegativeResponse
```

### Explanation:
1. **Software Client (Ansible/Node-RED):**
   - Represents the initiating client.
2. **Service:**
   - Represents the target system that processes requests.
3. **Responses:**
   - **Positive Response:** Indicates successful processing of the request.
   - **Negative Response:** Indicates failure or an error in processing.

---

### Best Practices
1. **Automate Where Possible:**
   - Minimize manual tests by leveraging tools like Ansible and APIs.
2. **Version Control Test Plans:**
   - Store test plans and scripts in GitLab for consistency and collaboration.
3. **Collaborate with Teams:**
   - Involve architects and NNDOE for test plan review.
4. **Test Iteratively:**
   - Conduct tests in phases, starting with low-impact scenarios.

---