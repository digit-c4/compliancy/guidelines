# HOW TO: Create an RFC (Request for Change)

This guide provides step-by-step instructions for creating an RFC (Request for Change) using our RFC management tool. By following this guide, you’ll ensure consistency, clarity, and adherence to best practices.
[`RFC tool`](https://intragate.ec.europa.eu/snet/web/changemanagement/main/#)

---

## Overview of the RFC Process

The RFC process is divided into eight distinct steps:  

```mermaid
graph LR
    A[Initiate] --> B[Classify]
    B --> C[Assess]
    C --> D[Review]
    D --> E[Authorize]
    E --> F[Schedule]
    F --> G[Implement]
    G --> H[Complete]
```

Each step must be carefully completed to ensure the change is planned, approved, and implemented with minimal risk.
`Any RFC myst be schedlued at least 7 days in advance, communications are required and customers must evaluate our changes.`

---

## Steps to Create an RFC

### 1. **Initiate**
This is the first step where the change request is created. Fill out the following fields:
- **Proposed Date of Implementation:** The target date for executing the change.
- **Estimated Duration:** How long the change is expected to take.
- **Reason:** A concise explanation for why the change is required.
- **Description of the Change:** A detailed explanation of what the change entails.
- **Impact:** Describe the expected impact on services or users.
- **In case of impact:** Propose a draft communication intended for end-users.
- **Tested in Lab (Yes/No):** Indicate whether the change has been tested in a controlled environment.
- **Service Catalogue Entry Impacted:** Specify the affected service from the service catalog.
- **Rollback plan:** Outline steps to revert the change if issues arise. If no rollback possible note it.


**Recommendation:** Be as detailed and precise as possible to help stakeholders understand the change’s context.

---

### 2. **Classify**
Classify the change based on the following criteria:
- **Change Category:** Choose one of the following:
  - Emergency Change
  - Major Change
  - Normal Change
  - Standard Change
- **Change Type:** Select the appropriate type:
  - Backup Change
  - Corrective Change
  - Disaster Recovery
  - Enhancement Change
  - Incident/Problem
  - Project
  - Request
  - Vulnerability Change
- **Urgency:** Assign a level:
  - Critical
  - High
  - Medium
  - Low
- **Impact Level:** Determine the impact:
  - Extensive
  - Significant
  - Medium
  - Minor
  - No Impact
- **Priority:** Set based on urgency and impact.
- **Risk Analysis:** Provide a description of the risks involved.
- **Risk Impact Description:** Detail the potential consequences if the change fails.
- **Risk Mitigation:** Outline the measures to reduce risks.

**Recommendation:** Collaborate with your team to ensure accurate classification and risk analysis.

---

### 3. **Assess**
Document all technical steps required to execute the change, including:
- **Pre-Change Steps:** Activities to prepare for the change. `Backup is a mandatory step`, please reuse the automated backup strategy/script in place.
- **Change Steps:** Detailed instructions for executing the change.
- **Test Steps:** Detailed instructions for testing the change, the test plan must be executed (ideally a test plan must match monitoring activities).
- **Rollback Steps:** Clear and precise instructions to revert the change if needed.

**Key Requirement:**  
- Ensure the content is **actionable and clear** for anyone to execute without requiring specialized skills.
- Use **Ansible playbooks** wherever applicable to automate tasks, including backup and tests.

**Recommendation:** Test all procedures in a lab environment before finalizing this step.

---

### 4. **Review**
- Have a **colleague** or a technical peer review the steps documented in the **Assess** phase.
- Confirm that all procedures are:
  - Clear and actionable.
  - Technically accurate.
  - Aligned with organizational standards.

**Recommendation:** Encourage reviewers to provide constructive feedback and address their comments promptly.

---

### 5. **Authorize**
- Obtain formal approval from an **authorized official** (for example, a manager or a Change Advisory Board member).
- This step ensures that the change aligns with business priorities and compliance requirements.

**Recommendation:** Clearly communicate the importance and urgency of the change to the approver.

---

### 6. **Schedule**
- **Send Official Communications:** Notify stakeholders about the planned change, including:
  - Date and time.
  - Expected duration.
  - Potential impacts.
- **Final Validation:** Confirm that all steps and approvals are complete before scheduling implementation.

**Recommendation:** Use clear, concise language in communications to ensure all stakeholders understand the schedule and impact.

---

### 7. **Implement**
- Execute the change on the scheduled day using the steps documented in the **Assess** phase.
- Monitor the change process closely and keep stakeholders informed of progress.

**Recommendation:** Have a rollback plan ready and accessible in case the change does not proceed as planned. Propose a draft communication intended for NOCA in case of incident.

---

### 8. **Complete**
- Document the outcome of the change:
  - Was the change successful?
  - Were there any issues or deviations from the plan?
  - Are any follow-up actions required?
- Update the RFC status to **Complete**.
- Update the incident, problem with a clear comment and resolve if the change has successfully resolved the issue.
- Close the communication with a clear message about the expected outcome.
- If a new service has been put into production, create a card for the Proxy Service Owner to update the service catalogue.

**Recommendation:** Share the results with relevant stakeholders and document lessons learned for future reference.

---

## General Tips for Creating an RFC

1. **Be Detailed Yet Concise:** Include all necessary information without overwhelming the reader.
2. **Collaborate:** Involve team members in drafting, reviewing, and refining the RFC.
3. **Use Automation:** Leverage Ansible and other tools to minimize manual steps and errors.
4. **Focus on Clarity:** Ensure that anyone can understand and execute the documented steps, regardless of their expertise level.
5. **Test Thoroughly:** Always test changes in a lab environment before implementation. `Always test after your change` .

---
