# Integration with Logging And Monitoring services

## Preamble

This document provides an high level view on the supporting service of logging and monitoring to which all the other services should connect to be compliant.

This supporting service handle parts of the requirements regarding, log management and standardization and also monitoring.

Using this supporting service also facilitate implementation of capacity management and event management.

## Overview

### Supporting assets involved

| Asset | Description | Available for Legacy | Available for Bubbles |
| ------- | ------------- | ---------------------- | ----------------------- |
| RSYSLOG| Specialized log collector and forwarder for traces from Debian servers and dockers| Dedicated | Dedicated|
| SYSLOG NG (VSYSLOG) | Log collector and concentrator for the any log but more specifically system or applicative logs. Instances of the bubbles forward the flows to the legacy / central one except for traffic logs which are routed to the Traffic Logs forwarder. | Dedicated | Dedicated|
| SPLUNK | Ultimate repository for logs of any source, hosted and managed centrally by the digit ########| Central | Central |
| Traffic Logs Forwarder| Specific capability deployed within bubbles to intermediate and route traffic logs of bubbles directly to SPLUNK without any local retention | N/A | Dedicated|
| PROMETHEUS | Monitoring tool pooling IT resources to retrieve values of metrics | Dedicated | Dedicated|
| Alert Manager| Add-on component of PROMETHEUS of Bubbles to forward notifications and alerts to the central SMARTS tool (legacy) | N/A | Dedicated|
| GRAFANA| Analytic tool using raw data from PROMETHEUS | Dedicated | Dedicated|
| WEATHERMAP | Graphic tool modeling status of the infrastructure based on raw data from PROMETHEUS| Dedicated | N/A |
| VNAS (archives) | Data store use to archive logs (system, applicative, technical ...) | Central | Central |
| SMARTS | Alert management tool (mainly based on trap SNMP, logs from central/legacy SYSLOG NG and central/legacy PROMETHEUS) | Dedicated | N/A |
| Gateway log forwarder | Technical SYSLOG flow forwarder relaying log flows of bubble to the central log collector (AZX are instances specific to AWS environment) | N/A | Component of LAM bubble |
| Gateway Notification forwarder | Technical event forwarder relaying notifications and alerts from the Alert Managers of Bubbles to the central/legacy SMARTS console (AZX are instances specific to AWS environment) | N/A | Component of LAM bubble |
| THANOS | Long term and centralized storage of the data of the PROMETHEUS instances of the bubbles | N/A | Component of LAM bubble |

### Logical flows

```mermaid
%%{
    init: {
    'theme': 'base', 
    'themeVariables': { 
        'primaryColor': '#eff7f6', 
        'primaryTextColor': '#000000', 
        'primaryBorderColor': '#5a5a5a', 
        'lineColor': '#5a5a5a', 
        'secondaryColor': '#f6f9f9', 
        'tertiaryColor': '#f8fcfb' 
        }
    }
}%%
flowchart LR
    classDef Ember fill:#dea83e, color:black;
    classDef NotDef fill:#fff0f3, color:#87031b;
    classDef Bubbles_style fill:#fef0da, color:black;
    classDef B_LAM_style fill:#e5feda, color:black;
    classDef Legacy_style fill:#daf3fe, color:black;
    
    %% Nodes definition
    %% When applicable for "duplicated" nodes starting with "B_" are specific to bubbles and "L_" specific for Legacy

    B_IT_resource{{IT resource}}:::Ember
    L_IT_resource{{IT resource}}:::Ember
    B_Traffic_logs_forwarder([Traffic log forwarder])
    B_Docker([Docker]):::Ember
    L_Docker([Docker]):::Ember
    B_RSYSLOG_comp([RSYSLOG])
    L_RSYSLOG_comp([RSYSLOG])
    B_SYSLOG_comp([SYSLOG-Ng])
    SYSLOG_Central([Central SYSLOG-Ng])
    B_Grafana_Prod([Grafana production]):::NotDef
    B_Grafext([Grafext]):::NotDef
    B_Grafshoot([Grafshoot]):::NotDef
    L_Grafana_Prod([Grafana production])
    L_Grafext([Grafext])
    L_Grafshoot([Grafshoot])
    B_Alert_manager(Alert Manager)
    B_Prometheus(Prometheus)
    L_Prometheus(Prometheus)
    Gw_log_fwd{{Gateway log Forwarder}}
    Gw_not_fwd{{Gateway notification<BR>Forwarder}}:::NotDef
    long_t_Prom("Long Term" Prometheus):::NotDef
    SPLUNK[Splunk]
    WeatherMap[Weather Map]
    VNAS{{VNAS}}
    SMARTS{{SMARTS}}

%% Representation of bubbles "standard" content
    subgraph Bubbles[Bubbles]

        subgraph Grafan_bubble[Grafana]
            B_Grafana_Prod
            B_Grafext
            B_Grafshoot
        end
        subgraph "VSYSLOG"
            B_Traffic_logs_forwarder
            B_SYSLOG_comp
        end
        subgraph Bubble_VMS[VMS]
            B_RSYSLOG_comp
            B_Docker
        end
        subgraph "Monitoring"
            B_Alert_manager
            B_Prometheus
        end
        
        %%links 0 to 6
        B_IT_resource --o|Traffic logs| B_Traffic_logs_forwarder
        B_IT_resource --o|Security logs| B_Traffic_logs_forwarder
        Grafan_bubble -->|use|B_Prometheus
        Prometheus --o|Active monitoring flow| B_IT_resource
        B_Docker --o|Docker logs| B_RSYSLOG_comp
        B_IT_resource --o|System / Application logs| B_RSYSLOG_comp
        B_RSYSLOG_comp -->|transfert|B_SYSLOG_comp

        %% Traffic logs style
        linkStyle 0 stroke:#800080,stroke-width:3px;
        %% Security Logs style
        linkStyle 1 stroke:#b9a943,stroke-width:3px;
        %% Active Monitoring Flow style
        linkStyle 3 stroke:#06b010,stroke-width:3px;
        %% Docker / System / Application logs style
        linkStyle 4,5 stroke:#b00606,stroke-width:3px;
        
    end
    class Bubbles Bubbles_style

%% Representation of LAM bubble content managed by SYS
    subgraph Bubble_LAM[Bubble LAM]
        Gw_log_fwd
        Gw_not_fwd
        subgraph "THANOS"
            long_t_Prom
        end
    end
    class Bubble_LAM B_LAM_style

%% Representation of Legacy content
    subgraph Legacy[Legacy]
        subgraph Grafana_Legacy[Grafana]
            L_Grafana_Prod
            L_Grafext
            L_Grafshoot
        end
        subgraph Central_VSYSLOG[Central VSYSLOG]
            SYSLOG_Central
        end
                subgraph Legacy_VMS[VMS]
            L_Docker
            L_RSYSLOG_comp
        end
        L_Prometheus
        VNAS
        SPLUNK
        SMARTS
        WeatherMap
        L_IT_resource


        %%links 7 to 20
        Grafana_Legacy -->|Use|L_Prometheus
        WeatherMap -->|Use|L_Prometheus
        L_Prometheus -->|Feed|SMARTS
        Central_VSYSLOG -->|Feed|SMARTS
        Central_VSYSLOG -->|Archive|VNAS
        Central_VSYSLOG -->|Transfer|SPLUNK
        L_Docker --o|Docker logs| L_RSYSLOG_comp
        L_RSYSLOG_comp -->|Transfer|Central_VSYSLOG
        L_IT_resource --o|Traffic logs| Central_VSYSLOG
        L_IT_resource --o|Security logs| Central_VSYSLOG
        L_IT_resource --o|System / Application logs| Central_VSYSLOG
        L_IT_resource --o|Technical events| SMARTS
        SMARTS --o|Active Monitoring Flow| L_IT_resource
        L_Prometheus --o|Active Monitoring Flow| L_IT_resource

        %% Traffic logs style
        linkStyle 15 stroke:#800080,stroke-width:3px;
        %% Security Logs style
        linkStyle 16 stroke:#b9a943,stroke-width:3px;
        %% Active Monitoring Flow style
        linkStyle 19,20 stroke:#06b010,stroke-width:3px;
        %% Docker / System / Application logs style
        linkStyle 13,17 stroke:#b00606,stroke-width:3px;
        %% Technical events style
        linkStyle 18 stroke:#259aee,stroke-width:3px;

    end
    class Legacy Legacy_style

%%links 21 to 32
%%flows from Bubbles to "LAM Bubble"
%%21
B_Traffic_logs_forwarder -..-o|Traffic logs| Gw_log_fwd
B_Traffic_logs_forwarder -..-o|Security logs| Gw_log_fwd
B_Traffic_logs_forwarder -..->|Transfer to Splunk| Gw_log_fwd
B_SYSLOG_comp -->|Transfer to SYSLOG| Gw_log_fwd
%%25
B_SYSLOG_comp -..-o|System / Application logs| Gw_log_fwd
B_Alert_manager -->|Notifications, alerts,...|Gw_not_fwd
B_Prometheus -->|Hypervision / Archive|long_t_Prom

%%Flows from "Lam Bubble" to Legacy
%%28
Gw_log_fwd -..-o|Traffic logs| SPLUNK
Gw_log_fwd -..-o|Security logs| SPLUNK
Gw_log_fwd -->|Transfer|SPLUNK
Gw_log_fwd -->|Transfer|Central_VSYSLOG
%%32
Gw_log_fwd -..-o|System / Application logs|SPLUNK
Gw_not_fwd -->|Notifications, alerts,...|SMARTS

%% Traffic logs style
linkStyle 21,28 stroke:#800080,stroke-width:3px;
%% Security Logs style
linkStyle 22,29 stroke:#b9a943,stroke-width:3px;
%% Docker / System / Application logs style
linkStyle 25,32 stroke:#b00606,stroke-width:3px;
```

## Use-cases

### Logging

#### A- Common events to log via System / Application logs or Docker logs

- Modification or deletion of logging policy/configuration;
- Login or logoff attempts;
- Administration activities;
- Identity and access management activities (creation/modification/deletion of a user, account, group, policy, authorisation, password; account or user allocation to/remove from a group);
- Configuration change (system, application, software, IP assignation DHCP…);
- Changes to the time synchronization configuration of a system or failure of time synchronization;
- Software install/update/uninstall;
- Antivirus activities (when available;)
- System crash, shutdown, or restart;
- Backup and restore operations;
- Capacity or limit exhaustion events (for memory, disk, CPU, and other system resources);
- All actions related to keys/secrets changes and recovery;
- Changes on the architecture (loss of a node, activation/deactivation of a node …)

#### B- Specific events to be logged via traffic logs

##### B1- For firewalls

- Remote access communication shall be logged and monitored.
- Attempts both successful and failed to access assets
- All blocked outgoing attempts to connect to networks outside the control of the Commission
- File exchange including content type
- When possible: Access both successful and failed to concealment/dissimulation services (for example: TOR; NordVPN; …)

##### B2- For Proxies and Reverse Proxies

- The outbound proxy should log all users’ web requests unless properly justified on the basis a risk assessment.
- Attempts both successful and failed to access assets
- All blocked outgoing attempts to connect to networks outside the control of the Commission.
- Blocked proxy connections to prohibited content

##### B3- For DNS

- The DNS server shall log all DNS requests.

##### B4- For Remote access

- VPN and other remote access authentications (both successful and failed)

#### C- Security logs

- Specific logs requested by the SOC or DIGIT-S to be sent directly to SPLUNK

#### D- Exceptions

- When it is not possible to split traffic logs from the others logs, it should be done by the local "syslog server" (local to the bubble or local to the legacy) and pushed to SPLUNK (directly if legacy environment or via the ###SLUNK FORWARDER###).

### Monitoring

#### Passive monitoring

Only available for legacy via SNMP traps sent to SMARTS.

#### Active monitoring

Performed via service probing (PING, call HTTP, ....), simulating end-user connection or checking paths of the network
Metrics collection (SNMP-GET, Calls APIs, ...) via pulling the metrics configured in the IT resources
