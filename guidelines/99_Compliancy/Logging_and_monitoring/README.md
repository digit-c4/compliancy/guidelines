# Logging and Monitoring (LAM) Documentation

Welcome to the Logging and Monitoring (LAM) repository. This repository contains essential documentation and resources about LAM service. Below is an overview of the structure and content of this repository.

## Contents

### 1. Document [`Intorduction to LAM at SNET`]  *coming soon*

**Description**: Document providing the key information about the context, the concepts and the way to address Logging And Monitoring into SNET.

**Content**:

- coming soon

### 2. Document [`Ìntegration with Logging an monitoring services of SNET`](./02-Integration_with_LAM_services.md)

**Description**: Set of key items to integrate properly an IT resource with the transversal services of logging and monitoring managed by the squad SYS

**Content**:

- Supporting assets involved
- Logical flow
- Use-cases of logging
- Use-cases of monitoring

### 3. Folder [`Static_images`](./Static_images/)

**Description**: contains the library of static images like screenshots or pictures which are not build neither possible to change without redoing it.

**Content**: collection of screenshots used by the several documents of this repository

### 5. Additional content

- [`Guidelines`](https://eceuropaeu.sharepoint.com/:f:/t/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Ev2TgjKa6htCteYeqty8XAEB-cn4au57oGdwKnR0Ft4bXg)
- [`syslog-ng Architecture`](https://intragate.ec.europa.eu/snet/wiki/index.php/System/syslog-ng_Architecture)
- [`Log files inventory`](https://intragate.ec.europa.eu/snet/wiki/index.php/System/Syslog_file)
- [`SPLUNK Indexes`](https://intragate.ec.europa.eu/snet/wiki/index.php/System/Splunk)
- [`Monitoring solutions`](https://intragate.ec.europa.eu/snet/wiki/index.php/System/Monitoring_Solution)

## Contributing

Suggestions for improving these guides are welcome. Please create issues or Merge Request.
