## Description
Standard Operating Procedures for onboarding, off boarding, and staff mobility, with a focus on the important role of line managers such as PPO, OQM, and SDM.

## Content Highlights
- [**Workforce & Talent Management Objectives**](https://code.europa.eu/digit-c4/compliancy/guidelines/-/blob/main/guidelines/99_Compliancy/Workforce_and_Talent_Management/WFTM_Objectives.md)
- **Onboarding Steps and Actions**
- **Off boarding Steps and Actions**
- **Mobility Steps and Actions**

## Primary Goals
These procedures aim to be clear and practical by achieving the following:

1. **Effective Onboarding**  
   Providing orientation and meaningful mentorship to the newcomer.
2. **Transparent Communication**  
   Establishing clear performance expectations and criteria.
3. **Skill Development**  
   Offering targeted training programs to enhance capabilities.
4. **Efficient and Secure Access Rights Management**  
   Ensuring proper handling of system access during onboarding, off boarding, and general mobility.

## References
- [NMS-III-SOP Offboarding.docx](https://sharepoint.com)
- [NMS-III-Offboarding Plan & Checklist.xlsx](https://sharepoint.com)
- [SOP - Privilege Management in PUMA - Snet](https://europa.eu)
- [Skill Matrix](https://eceuropaeu.sharepoint.com/teams/GRP-NMSIII-Phaseinactivities2-WFTM-HRRestricted/Shared%20Documents/Forms/AllItems.aspx?csf=1&web=1&e=L0qIwQ&CID=f789feb0%2D2b93%2D4db2%2Dacaf%2D3947ee60c132&FolderCTID=0x0120009BBDDBAADD4C474CA4D85259704FE7B6&id=%2Fteams%2FGRP%2DNMSIII%2DPhaseinactivities2%2DWFTM%2DHRRestricted%2FShared%20Documents%2FWFTM%20%2D%20HR%20Restricted%2FSkill%20matrix)
- [Org Chart](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII/_layouts/15/Doc.aspx?sourcedoc=%7B75FF9B72-2A7B-4E7D-A28E-978ECCD6F64C%7D&file=NTX_Squads%26Programme%20presentation_Jul_2024.pptx&action=edit&mobileredirect=true&wdLOR=c48C07CF2-0D72-469F-AAA1-31EEF1764D2F)