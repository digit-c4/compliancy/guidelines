# Onboarding Initiation

The process kicks off with some essential administrative tasks managed by the **HR Officer** and the **SDM**. This includes:

- Setting the start date.
- Creating the newcomer dossier for the European Commission secretariat.
- Filling in the necessary forms in **PUMA** for user creation and access privileges.

For **line managers**, your role in the preparation phase is equally crucial. You’ll need to:

- Assign a buddy for the newcomer.
- Send a friendly welcome email.  
  Be sure to include:
  - The exact start date and time.
  - Contact information for the person who will greet them on their first day.

---

## **HR Officer + SDM Key Actions**

- Define start date.
- Request and validate administrative documents.
- Fill in the newcomer form in **PUMA**.
- Attribute privileges in **PUMA**.

---

## **PPO Key Actions**

- Designate a buddy/mentor.
- Send a welcome email to the newcomer.

## **First day of a newcomer**

The PPO or your designated person are requested to do necessary so that the newcomer feels comfortable and welcome. This includes:

- Meeting the newcomer and giving them a tour of the workspace introducing the various teams.
- Helping them install their PC and set up their account using the emails already in their inbox.

After this initial setup, PPO will also need to:

1. Arrange a virtual meeting or casual coffee break with the whole team to build rapport.
2. Discuss the team culture and daily workflow at a high level (remember it is their first day!).
3. Provide a brief overview of their role (if you're the PPO).
4. Finally, ensure they’re added to essential tools like MS Teams, SharePoint, and any other relevant systems.

Meanwhile, the HR Officer will send out an announcement about the newcomer’s arrival, helping everyone stay in the loop.

--

## **First week of the newcomer**

In the first week, it’s crucial to handle key administrative tasks that set the foundation for a successful onboarding. Take this time to: 
Update your squad’s skill matrix and training plan, ensuring it reflects current needs. Review and update the org chart, as necessary.
Assign relevant training to the newcomer—whether it’s squad-specific or through Udemy—so they can begin leveling up right away.
Additionally, the HR Officer will schedule the performance assessment meetings, set for the end of the first, third, and sixth months. This helps us stay on track with their progress and ensure the onboarding process is smooth.

--

# Training and Development

We want to ensure that every newcomer is well-prepared and confident in their role. Here’s how we break down the training process:

## **1. Basic Training or All NTX Members**
- The EC and NTX organization and structure
- The business services and objectives
- Compliance documentation, service management best practices, and foundational DevOps and Agile practices

## **2. On-the-Job Trainings (Designated by Each PPO)** such as:
- The overview of daily activities
- The definition of the newcomer’s responsibilities and work schedule
- Any squad-specific processes, workflows, and SOPs

## **3. Operational Role Trainings**
- Key troubleshooting techniques in practice
- Squad-specific SOPs and best practices

These onboarding trainings give access to **Continuous Learning**:
- Designated trainings as appropriate based on **EC** and **NTX** goals. This ensures that the newcomer continues to grow and adapt as the business evolves.

--

## References
- [Skill matrix training plan](https://eceuropaeu.sharepoint.com/teams/GRP-NMSIII-Phaseinactivities2-WFTM-HRRestricted/Shared%20Documents/Forms/AllItems.aspx?csf=1&web=1&e=gs1CJL&CID=c421af0a%2D22ba%2D4521%2Db2f8%2D178a4b1229cf&FolderCTID=0x0120009BBDDBAADD4C474CA4D85259704FE7B6&id=%2Fteams%2FGRP%2DNMSIII%2DPhaseinactivities2%2DWFTM%2DHRRestricted%2FShared%20Documents%2FWFTM%20%2D%20HR%20Restricted%2FSkill%20matrix)
- [Org Chart](https://eceuropaeu.sharepoint.com/:p:/r/teams/GRP-NMSIII/_layouts/15/Doc.aspx?sourcedoc=%7B75FF9B72-2A7B-4E7D-A28E-978ECCD6F64C%7D&file=NTX_Squads%26Programme%20presentation_Jul_2024.pptx&action=edit&mobileredirect=true&wdLOR=c48C07CF2-0D72-469F-AAA1-31EEF1764D2F)
- [Welcome] (https://code.europa.eu/digit-c4/compliancy/guidelines/-/tree/main/guidelines/01_Welcome)
- [List of Processes and reference documentation] (https://eceuropaeu.sharepoint.com/:x:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/_layouts/15/Doc.aspx?sourcedoc=%7BBBD99A7E-2F8A-4BE1-81BF-2346302D4DE1%7D&file=List%20of%20Processes%20and%20reference%20documentation.xlsx&wdLOR=cEB686D64-50F8-477F-A221-5A612CAD5F1F&action=default&mobileredirect=true)
- Learning Paths in Udemy Business (https://ntt.udemy.com/?next=%2Flearning-paths%2F)