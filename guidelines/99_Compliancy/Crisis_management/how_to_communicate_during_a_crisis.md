# HOW TO: How to communicate during a crisis

This document describes how you can and you have to communicate during a crisis.


---


## **<br>1. Context**
A communication plan was defined and can be accessed here: [NMS-III - BCM-crisis communication plan](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCM-crisis%20communication%20plan.pdf?csf=1&web=1&e=q2mPKk)

---

<br>

The goal of the communication plan is to:
- Act quickly without panicking.
- Take the right decisions immediately.
- Communicate appropriately without making hasty decisions.

It takes charge of official communication during a crisis, and defines how communication should be organised and managed during a crisis, as well as what should be communicated.

><br>
> Before making any communication, MAKE SURE YOU HAVE THE RIGHT TO DO SO by reading the following chapter.
><br><br>


## **<br>2. Crisis communication responsibilities**
Here is an overview of the communication responsibilities during a crisis:<br>

 | **Crisis Management**       | **OQM/SDM<br>(Coordinator)** | **TCMT *(1)*<br>(War Room)** | **PPO<br>(TCMT member)** | **NTX-RT *(2)*<br>(Recovery Team)** | **HoU<br>(TCMT co-lead)** | **PO<br>(TCMT member)** | **Others** |
 |-----------------------------|:----------------------------:|:--------------------------:|:------------------------:|:---------------------------------:|:-------------------------:|:-----------------------:|:----------:|
 | Communication during crisis | **Responsible**              | **Accountable**            | Informed                 | Informed                          | **Responsible**            | Informed                | Informed   |
 | |

 *(1)* **TCMT**: Technical Crisis Management Team (or Crisis team)<br>
 *(2)* **NTX-RT**: NTX Recovery Team
 
 <br>The OQM or SDM, as War Room Coordinator, is in charge (delegation of execution is of course possible) of sending communication every time it is needed using the relevant pre-formatted messages.<br>

> <br>TO SUM UP<br>

> - The **War Room Coordinator** handles **all official and internal communication** during the crisis.<br>
> - **NMT Head of Unit** handles communication with **other EC crisis teams**.
> <br><br>

<br>The communication to end-users will be handled by ISHS and is out-of-scope of this document.

---


## **<br>3. Allowed and forbidden communication**

><br>

> - Public communication (for example press or any other unauthorized) **is not** under the responsibility of NMS-III organization. 
> - Overall crisis communication towards the incident is the responsibility of **NMT** (via the Crisis coordinator).
> - **Do not talk about a crisis** outside the context of your work.
><br><br>

---


## **<br>4. Communication templates**

The communication strategy and messages communicated are prepared in advance and pre-formatted:
- Different messages will have to be send at different time during the crisis and depending on the target audience.

- The persons responsible for relaying the messages to the different target audiences are selected by the War room coordinator who keeps the responsibility of the communication.

- all the message templates are available in the Communication plan: [NMS-III - BCM-crisis communication plan](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS-III%20-%20BCM-crisis%20communication%20plan.pdf?csf=1&web=1&e=q2mPKk)

<br>

