# Crisis Management Documentation

Welcome to the Crisis Management How To section. This repository contains essential documentation and resources to know what a crisis is, to manage a situation escalated to a crisis, to communicate during a crisis and to trigger business continuity plans. 

Below is an overview of the structure and content of this repository.

<br>


## Contents

### 1. **The Big picture**
Here is a broad view of how a crisis can be triggered and is managed.

```mermaid

flowchart LR

classDef Pink fill:#fdebdd, color:black;
classDef Yellow fill:#fff2cc, color:black;
classDef Purple fill:#ccc2d9, color:black;
classDef LightGreen fill:#E2EBD1, color:black;
classDef MediumGreen fill:#C4D6A0, color:black;
classDef DarkGreen fill:#9DBB61, color:black;
classDef Blue fill:#4BACC6, color:black;

  node_1([P1 esalated<br>as a crisis]):::MediumGreen
  node_2([OQM or SDM<br>crisis notification]):::MediumGreen
  node_3([EC<br>crisis notification]):::MediumGreen
  node_7[**Crisis communication**]:::MediumGreen
  subgraph Crisis
  direction LR
  node_4[**Crisis team<br>activation**]:::Blue
  node_5[Crisis management]:::Blue
  node_6[Return to normal]:::Blue
  end
  
  
  node_1 & node_2 & node_3 ----> node_4 --> node_5 --> node_6
  Crisis --> node_7

   
  %% Traffic logs style
    linkStyle 3,4 stroke:white,stroke-width:1px;
  %% Security Logs style
    linkStyle 0,1,2,5 stroke:#C05046,stroke-width:1px;

```

<br>

### 1. **HOW TO:[`ESCALATE TO A CRISIS SITUATION`](./how_to_escalate_to_crisis.md)**
- **Description**: This document outlines the escalation procedure to follow during incidents.  
  Includes:
  - Clear steps for escalating issues.
  - Roles and responsibilities during escalations.
  - Contact details for internal teams and external providers.

### 2. **HOW TO:[`MANAGE A CRISIS SITUATION`](./how_to_manage_a_crisis_situation.md)**
- **Description**: This guide provides information on who is in charge of the management of a crisis.  
  Includes:
  - Crisis team details.
  - Crisis repository and documentation.
  - Overall responsibilities.

### 3. **HOW TO:[`COMMUNICATE DURING A CRISIS`](./how_to_communicate_during_a_crisis.md)**
- **Description**: This How-To describes how to handle crisis communication.  
  Includes:
  - Communication responsibilities.
  - Allowed and forbidden communication.
  - Communication templates.

### 4. **HOW TO:[`CONTACT PROVIDERS`](../Incident_management/how_to_contact_providers.md)**
- **Description**: This guide provides information on how to efficiently contact external providers for support during incidents.  
  Includes:
  - Provider contact details.
  - Guidelines for the initial outreach (for example, what details to include).
  - Follow-up protocols.

<br>

## How to Use This Repository
1. **Familiarize yourself with procedures**  
   Start by reviewing [`how_to_escalate_to_crisis.md`](how_to_escalate_to_crisis.md) then understand your role and responsibilities during a crisis reading[`how_to_manage_a_crisis_situation.md`](how_to_Manage_a_crisis_situation.md)

2. **Have a better understanding of the needs and possibilities for communication and contact in the event of a crisis**  
   Navigate into [`how_to_communicate_during_a_crisis`](how_to_communicate_during_a_crisis.md) to understand the rules for communicating during a crisis, and have a look to [`how_to_contact_providers.md`](../Incident_management/How_to_contact_providers.md) to explore external contact points available to help managing a crisis.

3. **Improve your knowledge**
   consulting the documents located in the Teams directory dedicated to crisis management and continuity: [GRP-NMS III - Crisis Kit](https://eceuropaeu.sharepoint.com/:f:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General?csf=1&web=1&e=TjblaU)

## <br>Contributions
Suggestions for improving these guides are welcome. Please create issues or Merge Request.
