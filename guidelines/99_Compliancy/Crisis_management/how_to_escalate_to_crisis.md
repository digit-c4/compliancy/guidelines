# HOW TO: How to escalate to a crisis

This document describes the various triggers that can lead to a crisis being declared, as well as how to escalate such a situation so that it is taken into account.

<br>

---

The Crisis management process can be accessed here: [NMS-III - Crisis Management](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/Framework/NMS-III%20-%20Crisis%20Management.pdf?csf=1&web=1&e=m6j2be)

---

## **<br>1. Clear steps for escalating issues**

### **1.1. When and how to escalate**

There can be several triggers to activate crisis management:
- An incident (P1) worsens and becomes major.
- NMT reports a crisis situation to the SDM or the OQM, who activates the crisis team.
- A data breach occurs, and a crisis team is automatically created by the SDM or the OQM.

<br>

```mermaid

flowchart TD

classDef Pink fill:#fdebdd, color:black;
classDef Yellow fill:#fff2cc, color:black;
classDef Purple fill:#ccc2d9, color:black;
classDef LightGreen fill:#E2EBD1, color:black;
classDef MediumGreen fill:#C4D6A0, color:black;
classDef DarkGreen fill:#9DBB61, color:black;
classDef Blue fill:#4BACC6, color:black;

  node_1([P1 esalated<br>as a crisis]):::MediumGreen
  node_2([OQM or SDM<br>crisis notification]):::MediumGreen
  node_3([EC<br>crisis notification]):::MediumGreen
  node_4[**Crisis team<br>activation**]:::Blue
   
  node_1 & node_2 & node_3 ----> node_4 
 
  %% Security Logs style
    linkStyle 0,1,2 stroke:#C05046,stroke-width:1px;


```
<br>

> <br>

> If you observe such an incident, you should **raise an alert**.<br>
> The information should be passed on to anyone likely to take charge of an incident (mainly **NOCA**) or directly to the **SDM** or **OQM**.
> <br><br>

<br>Outside office hours, the point of contact remains the NOCA, which will be able to route the information to the people involved.

It is therefore the escalation of a particular situation **to the OQM or SDM** that enables them to assess the incident and decide to activate the crisis team.

---

### **1.2. Crisis Memo Card**

It's a credit-card-format document containing the main instructions and contacts you need to know in the event of a crisis [NMS III - Crisis Memo Card](https://eceuropaeu.sharepoint.com/:w:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/NMS%20III%20-%20Crisis%20Memo%20Card.docx?d=w575917371ffe4740857593825e434d4b&csf=1&web=1&e=dyh2zO).

It contains:
- Office Security Contact
- Instructions for the convocation of the Crisis Team during NWH (`7am to 7pm`)
- Instructions for the convocation of the Crisis Team during OWH (`7pm to 7am`)
- OnCall Contact Lists
- Links to Crisis management documentation

---


## **<br>2. Roles and responsibilities during escalations**
The roles of the various members of the staff are as follows:

 |                            | **OQM/SDM<br>(Coordinator)** | **TCMT<br>(War Room)** | **PPO<br>(TCMT member)** | **NTX-RT<br>(Recovery Team)** | **HoU<br>(TCMT co-lead)** | **PO<br>(TCMT member)** | **Others**  |
 |----------------------------|:----------------------------:|:----------------------:|:------------------------:|:-----------------------------:|:-------------------------:|:-----------------------:|:-----------:|
 | **Escalation**             | Responsible                  | Responsible            | Responsible              | Responsible                   | Responsible               | Responsible             | Responsible |
 | **Crisis team activation** | Responsible<br>Accountable   | Informed               | Informed                 | Informed                      | Responsible               | Informed                | Informed    |
 | |

<br>

> <br>

> **Everyone is responsible** for reporting a crisis situation.<br>
> Operation and Quality Manager or Service Delivery Manager, and Head of Unit (or deputy) are **responsible for activating the crisis unit**
> <br><br>

<br><br>

---
