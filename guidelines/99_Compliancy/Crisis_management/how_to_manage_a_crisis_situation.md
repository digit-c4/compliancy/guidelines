# HOW TO: How to manage a crisis situation

This document describes in general terms how a crisis is managed at NMS-III, what is expected of staff and where to find all the documentation needed to manage a crisis and ensure business continuity.

<br>

---

The Crisis management process can be accessed here: [NMS-III - Crisis Management](https://eceuropaeu.sharepoint.com/:b:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General/Framework/NMS-III%20-%20Crisis%20Management.pdf?csf=1&web=1&e=m6j2be)

---


## **<br>1. Crisis responsibilities**
The roles of the various members of the team are as follows:

---

- **Service delivery manager** (SDM) and/or **Operation and Quality Manager** (OQM) or his representative:

  Leads the NMS-III Crisis team and validates decisions.<br>
  Is in charge of triggering the crisis team, coordination of the Crisis team, of crisis communication, and of crisis decisions.
  
  <br>

- NMT **Head of Unit** (HoU) or his representative:

  Co-leads the crisis team and participates to tactical decisions with the SDM/OQM.
  
  <br>

- **Product Owners** (PO), **Proxy Product Owners** (PPO) and **guests**:

  Are responsible for directing and coordinating their respective operational teams, monitoring the progress of actions to be taken and reporting information to the crisis unit.
  
  <br>
  
 ---

The following 2 structures are also created during a crisis:

- The **Tactical Crisis Management Team** (TCMT or Crisis team) made up of :
  - Service Delivery Manager (SDM)
  - Operation and Quality Manager (OQM)
  - The PPO and PO involved in the major incident
  - other guests if needed and decided by this war room
  - and NMT Head of Unit (HoU) or substitute

  <br>

- The **Recovery team** (RT):

  - Made up of the teams needed to analyse, find and implement solutions (NNDOE, Architects, etc.)
  - In charge of assessing the situation, proposition of remediation plans and implementation of the selected remediation plan

  
---


## **<br>2. Crisis management**

OQM/SDM opens a War Room for the Crisis team (TCMT), if not already done, by sending an invite on Teams.

This team, or War Room, is made up of the management of NMS-III and NMT.<br>
How is it positioned? :
- It supervises only the crisis operations of the activities for which it is responsible.
- If the crisis goes beyond the scope of the DIGIT C4, it reports to the European Commission's higher-level crisis units.
- It may also coordinate with crisis units at the same level in other DIGIT.

Links between crisis units at the same or higher level are exclusively via NMT management.

Outside office hours, the people notified when the crisis is declared are On-Call NMS-III managers and on-call officials.
Crisis Memo card and NMSIII Crisis directory are available to list needed contacts for crisis team activation.

<br>

During the Crisis team (TCMT) activation, a War Room Coordinator is designed (by default the Operation Quality Manager).<br>
The War Room Coordinator coordinates the War Room investigations by:
-	Ensuring that the right resources are present and request them if needed.
-	Giving direction to the investigation if needed.
-	Ensuring that the exchanges stay as brief and efficient as possible.

<br>

### **2.1. Process sequence**
The process is then very similar to that of incident management:
- The crisis situation is analysed by the Recovery team.
- Remediation plans are drawn up by the Recovery team.
- The Crisis team validates the implementation of a remediation plan.
- The Recovery team implements the remediation plan.

If the situation does not improve, the Crisis team asks the Recovery team to re-analyse the situation.
Otherwise, the situation is monitored until it returns to normal.

The Crisis team can then make the return to normal official.


### **2.2. Time management**
To make sure that the situation doesn't get out of hand, resolution time is monitored:
- To this end, timetables define the time objectives to be achieved to resolve the crisis effectively.
- There are multiple duration and time reference used for crisis management. The objective is to allocate reasonable time frames for all activities.
- The targeted time points and durations are described in the Crisis management process, located in the Crisis kit dedicated channel.

Time objectives must be monitored and respected. They are **managed by the crisis team**.

By default, the Crisis coordinator is the time keeper.

 
---


## **<br>3. Crisis management documentation**

All documentation relating to crisis management and business continuity can be found in a dedicated Teams directory: [GRP-NMS III - Crisis Kit](https://eceuropaeu.sharepoint.com/:f:/r/teams/GRP-NMSIII-CrisisDocumentation/Shared%20Documents/General?csf=1&web=1&e=TjblaU)


Crisis directory structure:
```
.
├── Framework
├──── NMS-III - BCM Framework                                                                                        # Business Continuity Management framework
├──── NMS-III - Crisis Management                                                                                    # Crisis Management process
├── NMS III - Crisis Memo Card                                                                                       # Overview of the directory and its contents
├── NMS-III - BCM-crisis communication plan                                                                          # Guide for writing Standard Operational Procedures
├── NMS-III - Crisis Followup - v2                                                                                   # Guide to handle vuln at Squad level
├── NMS-III - BCP - Loss of Building - Unavailability of building in which NMT's services are provided               # Dedicated BCP
├── NMS-III - BCP - Loss of Building - Unavailability of NMS III staff offices                                       # Dedicated BCP
├── NMS-III - BCP - Loss of Building - Unavailability of storehouses under the NMSIII contractor’s responsibility    # Dedicated BCP
├── NMS-III - BCP - Loss of IT Infrastructure -  Unavailability of services due to DDoS activities                   # Dedicated BCP
├── NMS-III - BCP - Loss of IT Infrastructure -  Unavailability of services due to uncontrollable behaviour of infra # Dedicated BCP
├── NMS-III - BCP - Loss of staff                                                                                    # Dedicated BCP
├── Crisis Directory
├──── Crisis Directory                                                                                               # Main internal and externel contact points
├──── NMS III - Crisis Memo Card                                                                                     # Instructions and contact points in a credit card format
├── CheatSheets
├──── NMS-III - Crisis Timer                                                                                         # Timers for monitoring crisis stages 
├──── NMS III - Crisis & BCM Awareness                                                                               # Crisis & BCM Awareness (pdf and video format) 
├──── NMS III - Crisis & BCM walkthough exercise                                                                     # Crisis & BCM walkthough exercise (pdf and video format)

```
