# How to Manage a Data Subject Right Requests

## Preamble

Under GDPR, data subjects have several rights designed to give them control over their personal data. 

These rights empower individuals to have greater control over their personal data and ensure transparency and accountability from organizations processing their data.

In the context of SNET the request to handle are:

- Request of access to fulfill the `Right of Access`
- Request of rectification to fulfill the `Right to Rectification`
- Request of erasure to fulfill the `Right to Erasure (Right to be Forgotten)`

**NOTA: In this document `DSRR` is use to mention `Data Subject Right Request`**

## Overall process

SNET is not accountable of all the process but only a contributor.
The following diagram shows an overview on the global flow of a DSRR through the European Commission.

```mermaid
%%{
    init: {
    'theme': 'base', 
    'themeVariables': { 
        'primaryColor': '#eff7f6', 
        'primaryTextColor': '#000000', 
        'primaryBorderColor': '#000000', 
        'lineColor': '#5a5a5a', 
        'secondaryColor': '#f6f9f9', 
        'tertiaryColor': '#f8fcfb' 
        }
    }
}%%
flowchart LR
    classDef Blue_commission_style fill:#034EA2, stroke:white, stroke-width:3px, color:white;
    classDef Ember_commission_style fill:#FFC000, color:black;
    classDef Blue_line_style fill:#FFF, stroke:#034EA2,stroke-width:3px;
    classDef Ember_line_style fill:#FFF, stroke:#FFC000,stroke-width:3px;
    classDef Light_Ember_commission_style fill:#FFF8E5, color:black;
    classDef ghost_L_Ember_com_style fill:#FFF8E5, stroke:#FFF8E5, color:black;
    
    %% Nodes definition

    Data_Subject(((Data<BR>Subject))):::Blue_commission_style
    DPO(((**D***ata*<BR>**P***rotection*<BR>**O***fficer*))):::Blue_commission_style
    DPC(((**D***ata*<BR>**P***rotection*<BR>**C***oordinator* ))):::Blue_commission_style
    DSRR[/**D***ata* **S***ubbject*<BR>**R***ight* **R***equest*/]:::Ember_line_style
    NMT[NMT]:::Blue_commission_style
    SNET[SNET]:::Blue_line_style
    Ghost1(...):::ghost_L_Ember_com_style
    Ghost2(...):::ghost_L_Ember_com_style
    DSRR_Tools{{DSRR tools}}:::Ember_commission_style

%% Representation of bubbles "standard" content
    subgraph EC_Global[European Commission]
        DPO
        DPC
        Digit_Cx[DIGIT C..]:::Light_Ember_commission_style
        Digit_x[DIGIT...]:::Light_Ember_commission_style

        subgraph Digit_C4[DIGIT C4]
            Ghost1
            Ghost2
            NMT
            SNET
            DSRR_Tools

        end
        class Digit_C4 Light_Ember_commission_style
     
    end
    class EC_Global Ember_commission_style

    %%links 0
    Data_Subject -..->|"(1)" A person raise <BR> a privacy request| DSRR
    DSRR -..->|"(2)" Request to <BR> be analysed| DPO -..-> DPC
    %%links 3 & 4
    DPC -..->|"(3)" Demand of execution| Digit_x & Digit_Cx 
    DPC -..->|"(3)" Demand of execution| NMT
    %%links 6 & 7
    NMT -..->|"(4)" specific execution| Ghost1 & Ghost2
    NMT -->|"(4)" specific execution<BR>within SNET| SNET
    SNET -->|"(5)" Perform<BR>Extract or Deletion| DSRR_Tools
    %%links 10
    DSRR_Tools -->|"(6)" Confirm execution and<BR>Provide evidences| SNET
    SNET -->|"(7)" Collect confirmations<BR>and evidences| NMT
    NMT -..->|"(8)" Consolidate confirmations<BR>and evidences| DPO
DPO -..->|"(9)" Respond to the Data Subject| Data_Subject

    %% Traffic logs style
    linkStyle 3,4,6,7 stroke:#999999,stroke-width:3px;
    linkStyle 5,8,9,10,11,12 stroke:#034EA2,stroke-width:3px;
```

## Prerequisites

Each owner of each service must implement DSRR tooling (script, procedure...) to support the execution of DSRR.
This document only provides how the execution of such a tool is triggered and connected to the global process.

## Sequence of execution of a DSRR

This sequence is applicable for the DSRR of access and of erasure and the `Deletion protocol` is the DSRR of erasure.

```mermaid
sequenceDiagram
    participant DPO as DPO/DPC
    participant QPC as NMT/QPC
    participant Compliancy as Compliancy Squad
    participant NOCA
    participant Tool as DSRR tools
    participant IT as IT Resource

Note over QPC,Tool:Digit C4
Note over Compliancy,Tool:SNET
par ITSM tool (Service NOW)
    DPO->>+QPC: Request execution
    QPC->>+NOCA: Requests of specific execution<BR>for the scope NMS III<BR>(without identifiers of the data subject)
end
    NOCA->>+Tool:Perform the operation for the DSRR
    Par Feed audit trail
        Tool->>IT: Proceed to list the information
            alt Case of deltion protocole
            Tool->>IT: Erase the data
            Tool->>IT: Proceed to list the information
            Tool->>Tool: Contol no data is returned
        end
    And Build evidences
        Tool->>Tool: Consolidate traces and audit trail
    end

    Tool--)NOCA: Return evidences<BR>(locatioin where to find the set of files)
    NOCA--)QPC: Provide evidences (Using SECEM)
    QPC--)DPO: 
    NOCA->>NOCA: Delete remaining evindence
    Tool->>-NOCA:Return full execution report
    NOCA--)Compliancy: Inform (by eMail)
    Compliancy->>Compliancy: Complet KPIs/reports

par ITSM tool (Service NOW)
    NOCA->>-QPC: Confirm execution
    QPC->>QPC: Close ticket
    QPC->>-DPO: Transmit confirmations
end
DPO->>DPO:Consolidarte confirmations from all parties
```

***Note: the request of rectification is handled specifically to each service.***
