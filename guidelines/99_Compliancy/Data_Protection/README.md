# Data Protection (DP) Documentation

Welcome to the Data Protection (DP) repository. This repository contains essential documentation and resources about DP concepts or operations. Below is an overview of the structure and content of this repository.

## Contents

### 1. Document [`Introduction to data protection`](./01-Intorduction_to_Data_Protection.md)

**Description**: Synthesis of applicable Data Protection constraints and principles within the context of SNET

**Content**:

- Basic notions as:
  - Glossary
  - Categories of personal data
- Is the processing activity impacted by data protection and compliant?
- Key controls of data protection applicable to the scope of SNET and who is accountable of it
- Rights of the data subjects
- Personal data protection in the context of SNET
  - Main processing activities identified int SNET
  - Main Personal Data used in the scope of SNET
  - Applicable rights of the data subject

### 2. Document [`Data Subject Right Requests -DSRR- to be handled by SNET`](./02-Management_of_DSRR.md)

**Description**: Managing procedure of request from data subject for access, erasure or correction of their data

**Content**:

- Overall process of management of a Data Subject Right Requests -DSRR-
- Sequence of execution of a Data Subject Right Requests -DSRR-

## Contributing

Suggestions for improving these guides are welcome. Please create issues or Merge Request.
