# what is expected about Data protection (DP)

## Preamble

GDPR at glance is not applicable to the European Commission, a specific instantiation of data protection exists to cover the personal data protection:
=> `REGULATION (EU) 2018/1725`
This regulation is very similar to the chapters of the GDPR, but sometimes it has differences, in some cases such as the processing of operational personal data.

The next chapters of this document are the synthesis of applicable constraints and principles within the context of SNET.

## Disclaimer

`It is allowed to process and store personal data if the purpose of the processing activity is Lawful, fair, transparent and limited.`

So stop considering that it is absolutely forbidden to collect, use or manipulate personal data.

## Glossary

The data protection context has specific terms and meanings used in the regulation.
This terminology must be understood to be sure to apply properly the requirements od the regulation.
The following definition are the main ones to integrate.

**Actors:**

- `Data Subject`:A natural person / an individual whose personal data is processed by a controller or processor.
- `Data Controler`:An entity that determines the purposes and means of processing personal data.
- `Data Processor`:An entity that processes personal data on behalf of the data controller.
- `Data Protection Officer (DPO)`:A person appointed to ensure an organization complies with GDPR requirements.
- `Data Protection Coordinator (DPC)`: Specific role in the European Commission appointed to collaborate with the DPO and ensure an organization complies with GDPR requirements.

**Concepts:**

- `Personal Data`:Any information relating to an identified or identifiable natural person (and so a Data Subject).
- `Personal Data Processing activities` or `Data Processing activities` or `Prossessing activities`:Any operation performed on personal data, such as collection, storage, use, or deletion.
- `Processing Purposes`:The purposes for which personal data is being processed.
- `Recipient`:To whom the personal data has been or will be disclosed, including recipients in third countries or international organizations.
- `Transfer`:Details of any transfers of personal data to third countries or international organizations, including the identification of those countries or organizations and the safeguards in place.
- `Retention Period`:The envisaged time limits for erasure of personal data.
- `Security Measures`:A general description of the technical and organizational security measures in place to protect the data.
- `Data Retention period`: The amount of time an organization keeps data before they are destroyed. This period is determined based on legal, regulatory, business, and historical considerations.

**Activities and events:**

- `Personal Data Breach` or `Data Breach`:A security incident that leads to the accidental or unlawful destruction, loss, alteration, unauthorized disclosure of, or access to personal data.
- `DPIA`:Data Protection Impact Assessment (DPIA) is a process designed to help organizations identify and minimize the data protection risks of a project.

## What to do to be compliant?

### 1- Self assessment about the processing

Use this [how-to](./02-How_To_Know_if_Impacted.md).

> This self assessment must be done each time the processing activity is modified, improve or extended.

### 2- Document the processing

Maintain a [`Record Of all data Processing activities`](https://eceuropaeu.sharepoint.com/:x:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Data%20Protection/00-Validated/03-Tooling/NMS-III%20-%20Data%20Protection%20-%20ROP%20May%202024.xlsx?d=w1b40966aece64e91a2580a7cbbe9c98a&csf=1&web=1&e=yiOgD2) (ROP), including the purposes of processing, data sharing, and retention periods.

This document must be co-build with compliancy according to the inputs from the squad.

See the [dedicated HowTo](./04-How_To_Document_DP.md) to prepare this completion

**The following processing activities are today identified for the scope SNET**

| Name of the processing | Description |
| ----------------------- | ------------------------------------ |
| Logging-user | Logs of monitoring multiple points: system, traffic, connexion|
| Logging-admin | Logging of admin activities / event logs |
| Logging-transfer | Transfer of logs to EC (outside Snet) |
| Identity Management | Management of identities for Snet network |
| External support | External support and troubleshooting (at vendor level) |
| VC activities | Video conferencing activities |
| HR onboarding | Recruitment and onboarding related operations |
| HR management | Communication billing, HR planning and other HR related activities |
| Documentation | Documentation of Snet activities |
| Tools & DEV | Tools used and SOFTWARES developed by Snet using or processing personal data |
| Data backup | Backup of Snet data (excluding configurations) |

***Note:*** See the ['Record of Processing -ROP-'](https://eceuropaeu.sharepoint.com/:x:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Data%20Protection/00-Validated/03-Tooling/NMS-III%20-%20Data%20Protection%20-%20ROP%20May%202024.xlsx?d=w1b40966aece64e91a2580a7cbbe9c98a&csf=1&web=1&e=yiOgD2) for more details about the processing activities.

### 3- Identify and document the data processes

Maintain a `Data Inventory and Mapping` to document all personal data the organization collects, processes, and stores. Understand where it comes from, how it is used, and who has access to it.

According to this data inventory implementation Data Protection Measures like encryption, access controls, and other security measures to protect personal data from unauthorized access and breaches.

Use the `COMPLIANCY MOSCOW TABLE` to know what to do.

**The following data are considered by the regulation:**

<u>Common categories:</u>

- Marital status, ID, identification data, images...
- Personal life (lifestyle, family situation, etc.)
- Economic and financial information (income, financial situation, tax situation, etc.)
- Connection data (IP address, logs, etc.)
- Location data (movements, GPS data, GSM, etc.)
- Social Security Number (or NIR)

<u>Special categories:</u>

- Data revealing racial or ethnic origin
- Data revealing political opinions
- Data revealing religious or philosophical beliefs
- Data revealing trade union membership
- Genetic data
- Biometric data for the purpose of uniquely identifying a natural person
- Data concerning health
- Data concerning a natural person's sex life or sexual orientation
- Data relating to criminal convictions and offences

**The following data are today identified for the scope SNET:**

- Name (first name and last name)
- Teams or squad name
- Phone number (professional and or personal)
- Emails (professional and or personal)
- MASC, IP, SIP addresses
- User ID (UID)
- AD, LDAP groups
- Device name

### 4- Ensure that Rights of the data subjects can be fulfilled

These rights empower individuals to have greater control over their personal data and ensure transparency and accountability from organizations processing their data:

- `Right to be Informed:` Individuals have the right to be informed about the collection and use of their personal data. This includes details about the purposes of processing, data retention periods, and who the data will be shared with.
- `Right of Access:` Data subjects can request access to their personal data and obtain information about how it is being processed. This is often referred to as a Subject Access Request (SAR).
- `Right to Rectification:` Individuals have the right to have inaccurate personal data corrected or completed if it is incomplete.
- `Right to Erasure (Right to be Forgotten):` Under certain conditions, individuals can request the deletion of their personal data, such as when the data is no longer necessary for the purposes it was collected.
- `Right to Restrict Processing:` Data subjects can request the restriction or suppression of their personal data under specific circumstances, such as when they contest the accuracy of the data.
- `Right to Data Portability:` Individuals have the right to obtain and reuse their personal data for their own purposes across different services. This allows them to move, copy, or transfer data easily from one IT environment to another.
- `Right to Object:` Data subjects can object to the processing of their personal data in certain situations, such as for direct marketing purposes.
- `Rights Related to Automated Decision-Making and Profiling:` Individuals have the right not to be subject to a decision based solely on automated processing, including profiling, which produces legal effects or significantly affects them.

In the context of SNET we must guaranty the following rights:

- **Right of Access:** Extraction of a consistent copy of data stored about a Data Subject.
- **Right to Rectification:** Correction of incorrect data about a Data Subject.
- **Right to Erasure (Right to be Forgotten):** Erasure of all the data (when technically possible and not restricted by legal obligations), this operation is also made automatically after the retention period.

The `Data Subjects` execute their rights via `Data Sujbect Right Request -DSRR-` and the European Commission is obliged to handle it by the regulation.

> **A [dedicated document](./03-How_To_Manage_a_DSRR.md) detail the process to handle mandatory DSRR within SNET**


### 5- Management of Data Breaches

This is implement via the Security Incident Management which is a specific case of the Incident Management Process
