# How to document data protection implementation

## Describe the processing activities

Before the meeting with compliancy, prepare the following elements:

| Item | Description |
| --- | --- |
|Description of the processing operation | Short description of the operation, like the name and the context of the processing activity and main operations done|
|Stakeholders|List who (entity) is the (business) owner of the processing activity, who (entity) is performing the processing activity |
|Purpose|Describe the reason why the processing activities is done, the finality and potential obligations (legal or organisational)|
|Categories of personal data|try to identify which type of personal data are processed or stored -see the list bellow-|
|Categories od data subjects|List the type of people concerned by the processing -see the list bellow-|
|Recipients|list of entities, teams or persons who have access to the data (squads, NOCA, PPO, PO, etc.)|
|External transfers|Describe if potential transfer or storage of the data is done outside the European Union|

**List of personal data:**

| Category | List of types |
| --- | --- |
| Common | Marital status, ID, identification data, images...<BR>Personal life (lifestyle, family situation, etc.)<BR>Economic and financial information (income, financial situation, tax situation, etc.)<BR>Connection data (IP address, logs, etc.)<BR>Location data (movements, GPS data, GSM, etc.)<BR>Social Security Number (or NIR) |
| Sensitive / Special | Data revealing racial or ethnic origin<BR>Data revealing political opinions<BR>Data revealing religious or philosophical beliefs<BR>Data revealing trade union membership<BR>Genetic data<BR>Biometric data for the purpose of uniquely identifying a natural person<BR>Data concerning health<BR>Data concerning a natural person's sex life or sexual orientation<BR>Data relating to criminal convictions and offences |

**Categories of Data Subjects:**

- Employees
- Internal services
- Customers
- Suppliers
- Service providers
- Potential customers
- Applicants

The objective is to document the processing activities the ['Record of Processing -ROP-'](https://eceuropaeu.sharepoint.com/:x:/r/teams/GRP-NMSIII-Phaseinactivities2-compliancyISMS/Shared%20Documents/compliancy%20ISMS/00-Data%20Protection/00-Validated/03-Tooling/NMS-III%20-%20Data%20Protection%20-%20ROP%20May%202024.xlsx?d=w1b40966aece64e91a2580a7cbbe9c98a&csf=1&web=1&e=yiOgD2)

## Describe the flow personal data

According to the following diagram, try to define each step:

```mermaid
%%{
    init: {
    'theme': 'base', 
    'themeVariables': { 
        'primaryColor': '#eff7f6', 
        'primaryTextColor': '#000000', 
        'primaryBorderColor': '#000000', 
        'lineColor': '#5a5a5a', 
        'secondaryColor': '#f6f9f9', 
        'tertiaryColor': '#f8fcfb' 
        }
    }
}%%
flowchart LR
    classDef Blue_commission_style fill:#034EA2, stroke:white, stroke-width:3px, color:white;
    classDef Ember_commission_style fill:#FFC000, color:black;
    classDef Blue_line_style fill:#FFF, stroke:#034EA2,stroke-width:3px;
    classDef Ember_line_style fill:#FFF, stroke:#FFC000,stroke-width:3px;
    classDef Light_Ember_commission_style fill:#FFF8E5, color:black;
    classDef Light_Blue_commission_style fill:#D3E8F9, color:black;
    classDef ghost_L_Ember_com_style fill:#FFF8E5, stroke:#FFF8E5, color:black;

    %% Nodes definition
    Data_Source[(1.Source)]:::Blue_commission_style
    Data_Collection>2.Collection flow]:::Ember_commission_style
    Data_Collected_Storage[(3.Storage before<BR>processing)]
    Data_Processing[[4.Processing /<BR>Treatment]]:::Light_Blue_commission_style
    Data_Processed_storage[(5.Storage after<BR>processing)]
    Data_Transfer>6.Transfer flow]:::Ember_commission_style
    Data_Destination[(7.Destination)]:::Blue_commission_style

    subgraph Main_scope[Processing ecosystem]
    
    Data_Collected_Storage:::Light_Blue_commission_style
    Data_Processing
    Data_Processed_storage:::Light_Blue_commission_style

    end
    class Main_scope Light_Ember_commission_style

    Data_Source---Data_Collection---Data_Collected_Storage
    Data_Collected_Storage---Data_Processing---Data_Processed_storage
    Data_Processed_storage---Data_Transfer---Data_Destination
```

| **Step** |1.Source|2.Collection flow|3.Storage before<BR>processing|4.Processing /<BR>Treatment|5.Storage after<BR>processing|6.Transfer flow|7.Destination|
| --- | --- | --- | --- | --- | --- | --- | --- |
| **Information expected** | The name and type of the source,<BR>it can be a system, a process,<BR>or even a user | How the data is retrieved and applied protections,<BR>It can be actively like a pull of API,<BR> passively like incoming transfer of syslog<BR>or via a dedicated method like an UI | how the data is stored before the processing activity,<BR>Temporary file or queue,<BR>in the memory, or other | Kind of processing,<BR>by a script, an app<BR>or a human | how the data is stored after the processing activity,<BR>File or database,<BR>in a queue, or other | -*if any*-<BR>How the data is transferred and applied protections,<BR>It can be actively like a push of API,<BR> Transfer of syslog<BR>or via a dedicated method like an UI<BR>**IMPORTANT: also precise if the transfer<BR>is done outside the organisation or EU** | -*if any*-<BR>The name and type of the destination,<BR>it can be a system, a process,<BR>or an external organization |

For each item detail the **Data set** like in the following example:

| **Step** |1.Source|2.Collection flow|3.Storage before<BR>processing|4.Processing /<BR>Treatment|5.Storage after<BR>processing|6.Transfer flow|7.Destination|
| --- | --- | --- | --- | --- | --- | --- | --- |
| **Data set** | Host name<BR>IP address<BR>Mac address<BR>UserID<BR>Password | Host name<BR>IP address<BR>Mac address<BR>UserID<BR>Password | Host name<BR>IP address<BR>Mac address<BR>UserID | Host name<BR>IP address<BR>Mac address<BR>UserID<BR>Password<BR>+<BR>timestamp<BR>Geographical source region | Host name<BR>IP address<BR>Mac address<BR>UserID<BR>Password<BR>timestamp<BR>Geographical source region | N.A. | N.A. |

Then communicate the information to the squad Compliancy
