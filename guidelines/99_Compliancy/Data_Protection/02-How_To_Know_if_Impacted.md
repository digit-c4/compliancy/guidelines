# How to know if you are impacted of not by Personal Data Protection

## Decision diagram to use for your self assessment

| Preliminary question | Answer |
|--|--|
| When this assessment should be done? | During the design of the service or the processing activity |
| Who should do this assessment? | the PO, the PPO and the architect are mandatory |
| Is this assessment mandatory? | Yes even if it looks useless |

```mermaid
%%{
    init: {
    'theme': 'base', 
    'themeVariables': { 
        'primaryColor': '#eff7f6', 
        'primaryTextColor': '#000000', 
        'primaryBorderColor': '#000000', 
        'lineColor': '#5a5a5a', 
        'secondaryColor': '#f6f9f9', 
        'tertiaryColor': '#f8fcfb' 
        }
    }
}%%
flowchart TD

    classDef Blue_commission_style fill:#034EA2, stroke:white, stroke-width:3px, color:white;
    classDef Ember_commission_style fill:#FFC000, color:black;
    classDef Blue_line_style fill:#FFF, stroke:#034EA2,stroke-width:3px;
    classDef Ember_line_style fill:#FFF, stroke:#FFC000,stroke-width:3px;
    classDef Light_Ember_commission_style fill:#FFF8E5, color:black;
    classDef Light_Blue_commission_style fill:#D3E8F9, color:black;
    classDef ghost_L_Ember_com_style fill:#FFF8E5, stroke:#FFF8E5, color:black;
    
    %% Nodes definition
    Start((( )))
    End((( ))):::Blue_commission_style
    Data_Processor{{Is the data<BR>**processed or stored**<BR>by my organisation?}}:::Light_Blue_commission_style
    Data_Controler{{Is the data<BR>**collected**<BR>by my organisation?}}:::Light_Blue_commission_style
    Data_Co_Processor{{Is traces of the data<BR>**stored or registered**<BR>by my organisation?}}:::Light_Blue_commission_style
    Personal_data{{Does the data contains<BR>one of the category/type<BR>of **personal data**?}}:::Light_Blue_commission_style
    Controler_accountability{{Is the processing of data<BR>jsutified and required<BR>by the PO or<BR>the service owner?}}:::Light_Ember_commission_style
    Purpose{{Is the purpose of the processing<BR>well defined and clear?}}:::Light_Ember_commission_style
    Minimization{{Is it mandatory to process<BR>all the data impacted?}}:::Light_Ember_commission_style
    Storage_limitation{{Is the retention period<BR>well defined and limited?}}:::Light_Ember_commission_style
    Data_Portected{{Is the data **well protected<BR>against disclosure**<BR>or corruption}}:::Light_Ember_commission_style
    Not_impacted([Not impacted]):::Light_Blue_commission_style
    Impacted([Impacted and<BR>well addressed]):::Blue_commission_style
    Not_Compliant([Impacted but<BR>**remediation needed**]):::Ember_commission_style
    Note_purpose[-Lawfulness<BR>-Fairness<BR>-Transparency<BR>-Purpose limitation]

    Start --> Data_Processor
    Data_Processor -->|No| Data_Controler
    Data_Controler -->|No| Data_Co_Processor
    Data_Co_Processor -->|No| Not_impacted
    Data_Processor -->|Yes| Personal_data
    Data_Controler -->|Yes| Personal_data
    Data_Co_Processor -->|Yes| Personal_data
    Personal_data -->|No| Not_impacted
    Personal_data -->|Yes| Controler_accountability
    Controler_accountability -->|No| Not_Compliant
    Controler_accountability -->|Yes| Purpose
    Purpose -->|No| Not_Compliant
    Purpose -->|Yes| Minimization
    Minimization -->|No| Not_Compliant
    Minimization -->|Yes| Storage_limitation
    Storage_limitation -->|No| Not_Compliant
    Storage_limitation -->|Yes| Data_Portected
    Data_Portected -->|No| Not_Compliant
    Data_Portected -->|Yes| Impacted
    Note_purpose --o Purpose
    Not_impacted & Not_Compliant & Impacted --> End

    linkStyle 1,2,3,7,9,11,13,15,17 stroke:#FFC000,stroke-width:2px;
    linkStyle 4,5,6,8,10,12,14,16,18 stroke:#034EA2,stroke-width:3px;
```

Note: this decision diagram permit to ensure that compliance of processing activity is assessed according to the following principles

- **1- Lawfulness, fairness, and transparency:** Personal data should be processed legally and transparently.
- **2- Purpose limitation:** Data should only be used for specific activities.
- **3- Data minimization:** Collect only the necessary data.
- **4- Accuracy:** Ensure data is accurate.
- **5- Storage limitation:** Keep data only for as long as necessary.
- **6- Integrity and confidentiality:** Protect data from unauthorized access.
- **7- Accountability:** Be responsible for compliance with the personal data protection regulation

> Each answer of the assessment must be justified by a proof or evidence.

## What to do according to the result of the self assessment?

### Not Impacted

According to the self assessment the processing activity is not impacted by the regulation just share the assessment with compliancy.

### Impacted and well addressed

Even if personal data is processed, the requirements of compliance are fulfilled.
Share the result with Compliancy Squad, a workshop will be organised by compliancy to complete the Record Of Processing (ROP).

### Impacted but remediation needed

Personal data is processed and one or more requirements are not complied.
Share the result with Compliancy Squad, a set of workshops will be organised by compliancy to:

- Complete the Record Of Processing (ROP)
- Identify the gaps
- define the remediation points
