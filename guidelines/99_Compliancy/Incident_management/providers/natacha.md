# HOW TO: Contact NATACHA

T
## **1. Service Details**
- **Service Name**: Any hardware/Software [CISCO, FORTINET, CHECKPOINT, BROADCOM, IVANTI]
- **Provider Name**: [NTT Belgium]

---

## **2. Provider Contact Information**
- **Contact Method**: [Primary way to contact the provider, for example, Phone, Email, Support Portal]
- **Provider Email**: [support@example.com]
- **Provider Phone Number**: [+1 (123) 456-7890]
- **Support Website URL**: [https://support.example.com]

---

## **3. Contract Details**
- **Service Contract Number**: [for example, 12345-ABCDE]
- **Support Type**: [for example, 24/7 Support, Next Business Day]
- **Response SLA**: [for example, Critical incidents: 30 minutes, Low priority: 4 hours]
- **Maintenance Windows**: [for example, Sundays, 2 AM to 4 AM UTC]

---

## **4. Incident Details (To be Filled During Contact)**
- **Incident ID**: [Internal incident or ticket number]
- **Date/Time of Incident**: [When the issue was detected]
- **Description of Issue**:
  - [Briefly describe the problem, for example, "Internet connection is down at HQ."]
  - [Include any error messages, logs, or observed symptoms.]
- **Actions Taken**:
  - [Document what has been done so far, for example, "Restarted router, checked cables."]

---

## **5. Additional Information**
- **Provider Escalation Path**:
  - Level 1 Support Contact: [Details]
  - Level 2 Escalation Contact: [Details]
  - Emergency Escalation Contact: [Details]
- **Material Replacement Instructions**:
  - **Serial Number**: [Enter the serial number of the affected hardware.]
  - **Location of Spare Parts**: [for example, Rack 3, Section B, Data Center.]
  - **RMA Number**: [If previously obtained.]

---

## **6. Example of Communication**
### **Email Template for Service Interruption**
```
Subject: [URGENT] Service Interruption - [Service Name]

Dear [Provider Name] Support Team,

We are experiencing a critical issue with [Service Name] (Contract No: [Service Contract Number]) as of [Date/Time]. The observed problem is as follows:
- Description: [Brief description of the issue]
- Impact: [for example, No internet access for 100 users]
- Actions Taken: [for example, Restarted modem, checked cabling]

Please respond at your earliest convenience. Contact us via [Your Contact Info].

Thank you,
[Your Name/Team]
```

### **Phone Script for Material Replacement**
```
1. Dial Provider’s Support Number: [+1 (123) 456-7890].
2. Provide the following details:
   - Service Name: [for example, Firewall Appliance]
   - Contract Number: [12345-ABCDE]
   - Serial Number of Device: [Enter Serial Number]
   - Issue Description: [for example, Device not powering on]
3. Request a replacement under the warranty/contract terms.
4. Ask for an RMA number and estimated replacement time.
5. Record the ticket/reference number provided by the support agent.
```

---

## **7. Checklist for On-Call Personnel**
- [ ] Verify the service affected and gather all relevant information.
- [ ] Check if the issue falls within SLA terms or requires escalation.
- [ ] Use the predefined contact method and log all interactions.
- [ ] Update the incident management system with communication and actions.

---

## **8. Notes**
- Escalate directly to on-call officials if:
  - The provider does not respond within the SLA.
  - The incident severity increases or impacts critical business functions.
```