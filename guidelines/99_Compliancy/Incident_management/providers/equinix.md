# HOW TO: Contact Providers for Incident Management

This template is designed to help on-call team members contact providers efficiently for service interruptions, bug reports, or material replacements. It ensures all necessary details are readily available.

---

## **1. Service Details**
- **Service Name**: Private Network Line to join AWS
- **Provider Name**: [EQUINIX]

---

## **2. Provider Contact Information**

- **Contact Method**: Email  
- **Provider Email**: [supportdesk.nl@eu.equinix.com](mailto:supportdesk.nl@eu.equinix.com)  
- **Provider Phone Number**: [+31 53 8080 116](tel:+31538080116)  
- **Support Website URL**: N/A  
- **Account**: European Commission

---

## **3. Contract Details**
- **Service Contract Number**: [N/A]
- **Support Type**: [?]
- **Response SLA**: [?]
- **Maintenance Windows**: [?]

---

## **4. Incident Details (To be Filled During Contact)**

- **Incident ID**: [Internal incident or ticket number]  
- **Date/Time of Incident**: [When the issue was detected]  
- **Description of Issue**:  
  - [Briefly describe the problem, for example, "Internet connection is down at HQ."]  
  - [Include any error messages, logs, or observed symptoms.]  

### **Asset Information (Mandatory)**

| **CC Serial**     | **Asset Number**       | **Supplier**         | **Connection Type**              | **Bandwidth**       | **Address**          |
|--------------------|------------------------|-----------------------|-----------------------------------|---------------------|----------------------|
| [Enter CC Serial]  | [Enter Asset Number]  | [Enter Supplier]     | [Enter Connection Type]          | [Enter Bandwidth]   | [Enter Address]      |

- **Actions Taken**:  
  - [Document what has been done so far, for example, "Restarted router, checked cables."]


---

## **5. Additional Information**
- **Provider Escalation Path**:
  - Level 1 Support Contact: [?]
  - Level 2 Escalation Contact: [?]
  - Emergency Escalation Contact: [?]
- **Material Replacement Instructions**:
**Asset Information (Mandatory)**

| **CC Serial**     | **Asset Number**       | **Supplier**         | **Connection Type**              | **Bandwidth**       | **Address**          |
|--------------------|------------------------|-----------------------|-----------------------------------|---------------------|----------------------|
| [Enter CC Serial]  | [Enter Asset Number]  | [Enter Supplier]     | [Enter Connection Type]          | [Enter Bandwidth]   | [Enter Address]      |
  - **RMA Number**: [If previously obtained.]

---

## **6. Example of Communication**
### **Email Template for Service Interruption**
```
Subject: [URGENT] Service Interruption - [Service Name]

Dear [Provider Name] Support Team,

We are experiencing a critical issue with [Service Name] (Contract No: [Service Contract Number]) as of [Date/Time]. The observed problem is as follows:
- Description: [Brief description of the issue]
- Impact: [for example, No internet access for 100 users]
- Actions Taken: [for example, Restarted modem, checked cabling]
- Asset information

Please respond at your earliest convenience. Contact us via [Your Contact Info].

Thank you,
[Your Name/Team]
```

### **Phone Script for Material Replacement**
```
1. Dial Provider’s Support Number: [+31 53 8080 116](tel:+31538080116).
2. Provide the following details:
   - Service Name: [for example, Firewall Appliance]
   - Contract Number: [?]
   - Serial Number of Device: [Enter Serial Number]
   - Issue Description: [for example, Device not powering on]
3. Request a replacement under the warranty/contract terms.
4. Ask for an RMA number and estimated replacement time.
5. Record the ticket/reference number provided by the support agent.
```

---

## **7. Checklist for On-Call Personnel**
- [ ] Verify the service affected and gather all relevant information.
- [ ] Check if the issue falls within SLA terms or requires escalation.
- [ ] Use the predefined contact method and log all interactions.
- [ ] Update the incident management system with communication and actions.

---

## **8. Notes**
- Escalate directly to on-call officials if:
  - The provider does not respond within the SLA.
  - The incident severity increases or impacts critical business functions.
```