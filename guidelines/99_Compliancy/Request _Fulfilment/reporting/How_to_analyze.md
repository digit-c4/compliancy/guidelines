### **Guideline for Request Weekly Analysis**

#### **1. Report Index Per Squad**

Please find your SQUAD report

| **SQUAD**               | **Report**          | 
|---------------------------|-----------------------|
| RPS     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/342d790d2b5a969896bafce85e91bfe9) |
| LB-DNS-PROXY     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/9c8aa1f0fbdaded8da93f12c6eefdc03) |
| FIREWALL     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/4ca20e012b9e969896bafce85e91bfb6) |
| TELEPHONY     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/f3f276413b12d2586ed91d24c3e45a70) |
| VIDEOCONF     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/1ae8c68d2b92d69896bafce85e91bf79) |
| WIFI-WORKPLACE     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/184fce012b1ad69896bafce85e91bf87) |
| RAS     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/724b0a052b56d69896bafce85e91bfd0) |
| TC/DC     | [View Details](https://digit.service-now.com/now/platform-analytics-workspace/dashboards/params/edit/false/sys-id/1908da8d2b5ed69896bafce85e91bf33) |

#### **2. Section Description**

 - Request Opened this week
 - Request in Progress or on Hold 
 - Request P1 and P2 in progress or on Hold
 - Request by assignee
 
#### **3. Day to Day Review**
 - Check Request has breached
 - Check Request Time left < 1 h

#### **4. Weekly Analysis Criteria**

  - Why an increase of number of Request is visible?
  - Why Request are on Hold ?
  - Why P1 or P2 still ongoing?
  - Why all Requests on single assignee?
  - Why so many Requests on a specific Service Offering?
  - Why Request are escalated to L2? Do you have a lack of automation ?

#### **5. Monthly Analysis Criteria**
  - Why breached tickets are present? Explanations per ticket are required
  - What are the in queries or request that to be automated next month?
