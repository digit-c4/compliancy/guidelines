# Triage Guidelines

This document outlines the conditions and processes for escalating tickets to ensure timely and effective resolution. Proper escalation ensures efficient use of resources and compliance with organizational workflows.

---

## **Escalation Conditions**

### **1. Escalation to Product Owner**
Escalate a ticket to the Product Owner or the **DIGIT Network Service Managers**, in the ticketing system, under the following conditions:

1. **Non-Standard Requests:**
   - The request does not align with predefined standard procedures or configurations.
   - Examples:
     - Requests for custom features or configurations outside the approved service catalog.
     - Unusual requirements that require design or architectural changes.
   - **Action:** Assign the ticket to the group `DIGIT Network Service Managers`.

2. **Policy Exceptions:**
   - The request violates established policies or requires an exception approval.

3. **Dependency on Other Teams:**
   - The request involves significant cross-team dependencies that need Product Owner coordination.

---

### **2. Escalation to Proxy Product Owner**
Escalate a ticket to the **Proxy Product Owner**, in the ticketing system, when cross-squad coordination is required, particularly in scenarios involving shared responsibilities across multiple squads AND/OR when an SOP is not available for NOCA (Level 1) to handle the request.

#### **Conditions for Escalation:**
1. **Cross-Squad Dependencies:**
   - The ticket requires collaboration between multiple squads to resolve.
   - Examples:
     - A change in firewall rules affecting load balancer configurations.
     - DNS updates impacting reverse proxy routing.

2. **Alignment Across Services:**
   - The issue spans multiple services managed by different squads (for example, DNS, Load Balancers, Reverse Proxies, etc.).
   - Coordination is required to align configurations, schedules, or testing.

3. **Coordination of Deliverables:**
   - The ticket involves deliverables that multiple squads need to contribute to or validate.

---

### **3. Escalation to SQUAD (Level 2)**
Escalate a ticket to SQUAD (Level 2) under the following conditions:
1. **Technical Complexity:**
   - Requires advanced troubleshooting or configuration expertise.
2. **Missing or Insufficient SOPs:**
   - SOPs do not exist or do not adequately cover the issue.
3. **Tooling Limitations:**
   - Available tools are not sufficient to address the scenario.
4. **Recurring Issues:**
   - Similar issues are occurring repeatedly and require root cause analysis.

---

## **How to Escalate**

### **1. Escalate to Product Owner**
1. Assign the ticket to the group **`DIGIT Network Service Managers`** in the ticketing system.
2. Provide the following details:
   - **Description of the Request:** Include all relevant information about the non-standard request.
   - **Reason for Escalation:** Specify why it cannot be resolved at Level 1 or Level 2.
   - **Supporting Documentation:** Attach relevant references, logs, or examples.

---

### **2. Escalate to Proxy Product Owner**
1. Assign the ticket to the relevant **Proxy Product Owner** in the ticketing system.
2. Provide the following details:
   - **Description of the Issue:** Outline the problem and its scope.
   - **Cross-Squad Dependencies:** Specify which squads are involved and their roles.
   - **Reason for Escalation:** Explain why coordination is required and why it cannot be resolved within a single squad.
   - **Supporting Information:** Attach relevant logs, SOPs, or references.

---

### **3. Escalate to SQUAD (Level 2)**
1. Assign the ticket to the **Proxy Product Owner** in the ticketing system, who will handle dispatching it to the appropriate squad member.
2. Include:
   - **Detailed Issue Description:** Explain the problem and its symptoms.
   - **Actions Taken:** Summarize actions performed by NOCA (Level 1).
   - **Gaps Identified:** Highlight missing SOPs or tooling limitations.
   - **Logs and Evidence:** Attach any supporting technical data.

---

## **Escalation Workflow**
```mermaid
flowchart TD
    A[Ticket Created] -->|Ticket Assigned| B(Checks)
    B --> G{is it well formatted?}
    G -->|Yes| C{is it a standard request}
    C --> K{SOP existing?}
    K -->|No| L[Assign Proxy Product Owner ]
    K -->|Yes| E
    G -->|No| H[Request NOT FORMATTED]
    C -->|No| D[Assign to NWT Service Manager]
    L --> E[Request EXECUTED]
    H --> F[Resolve the ticket]
    E --> F[Resolve the ticket]
    D -->|Approval| C
```

---