Here’s the updated **HOW TO: Write Documentation** guide incorporating the different documentation types and their characteristics:

---

# HOW TO: Write Documentation

This guide provides instructions for writing high-quality documentation for our repositories, ensuring consistency, readability, and adherence to organizational standards.

---

## Requirements and Standards

### 1. **Language**
- All documentation **must be written in English** to ensure accessibility across the organization.

### 2. **Format**
- Use **Markdown** (`.md`) as the standard and mandatory format for all documentation files.

### 3. **Diagrams**
- Diagrams must be created using **Mermaid** syntax for seamless rendering in GitLab.  
  Example:
  ```mermaid
  graph TD;
      A-->B;
      B-->C;
      C-->D;
  ```

### 4. **Style Management**
- **VALE** is integrated into the CI/CD pipeline and enforces the **Google Style Guide** for writing. Adhere to these guidelines to maintain consistency in tone and structure.

---

## Types of Documentation

Depending on the purpose and audience, documentation falls into one of four categories. Choose the right type to match your content needs.

### 1. **Tutorials**
- **What They Do:** Introduce, educate, and lead the user.
- **Answers the Question:** “Can you teach me to...?”
- **Orientation:** Learning.
- **Purpose:** To provide a learning experience.
- **Form:** A lesson.
- **Analogy:** Teaching a child how to cook.
- **Examples:** Getting Started with GitLab CI/CD or Introduction to Markdown Syntax.

### 2. **How-To Guides**
- **What They Do:** Guide the user through specific steps.
- **Answers the Question:** “How do I...?”
- **Orientation:** Goals.
- **Purpose:** To help achieve a particular goal.
- **Form:** A series of steps.
- **Analogy:** A recipe in a cookery book.
- **Examples:** How to Set Up Visual Studio Code, How to Configure a Proxy for Git.

### 3. **Reference**
- **What They Do:** State, describe, and inform.
- **Answers the Question:** “What is...?”
- **Orientation:** Information.
- **Purpose:** To describe the machinery.
- **Form:** A dry description.
- **Analogy:** Information on the back of a food packet.
- **Examples:** Git Commands Cheat Sheet, Mermaid Syntax Overview.

### 4. **Explanation**
- **What They Do:** Explain, clarify, and discuss.
- **Answers the Question:** “Why...?”
- **Orientation:** Understanding.
- **Purpose:** To illuminate a topic.
- **Form:** A discursive explanation.
- **Analogy:** An article on culinary social history.
- **Examples:** Why Use GitLab CI/CD?, The Role of MoSCoW in Prioritization.

---

## Step-by-Step Guide to Writing Documentation

### Step 1: **Identify the Documentation Type**
Determine whether your content is a tutorial, a how-to guide, a reference, or an explanation. This will guide its structure and tone.

### Step 2: **Plan Your Content**
- Define the purpose of the document.
- Outline key topics or sections.

### Step 3: **Follow Markdown Conventions**
- Use headings to organize content:
  ```markdown
  # Heading 1
  ## Heading 2
  ### Heading 3
  ```
- Format text with Markdown syntax:
  - **Bold**: `**text**`
  - *Italic*: `*text*`
  - `Code`: `` `text` ``
- Use tables where necessary:
  ```markdown
  | Column 1 | Column 2 |
  |----------|----------|
  | Data 1   | Data 2   |
  ```

### Step 4: **Add Diagrams with Mermaid**
- Use Mermaid for diagrams to simplify visualization.  
  Example:
  ```mermaid
  flowchart LR
      A[Start] --> B{Decision};
      B -->|Yes| C[Do Task];
      B -->|No| D[Stop];
  ```

### Step 5: **Run VALE for Style Checking**
- Before submitting, validate your document with **VALE** to ensure it adheres to the Google Style Guide.
- There is an existing template for **VALE** available on https://code.europa.eu/digit-c4/vale. To use it adapt your gitlab-ci.yml file :
```
stages:
  - vale-lint

include:
  - project: 'digit-c4/digitC4-template-cicd'
    file: 'gitlab-ci-ansible.yml'
    ref: main
  - project: 'digit-c4/vale'
    file: 'templates/vale.yml'
    ref: v0.0.5
```
- If issues are flagged, revise your text to resolve them.

### Step 6: **Test in GitLab**
- Preview your Markdown files and Mermaid diagrams in GitLab to ensure proper rendering.
- Fix any formatting or syntax errors.

---

## Additional Tips

1. **Use Clear, Consistent Language**
   - Avoid jargon unless necessary and define terms when used.
   - Keep sentences concise.

2. **Link Between Documents**
   - Use relative paths to link to other files in the repository:
     ```markdown
     [Related Document](../path/to/file.md)
     ```

3. **File Naming**
   - Use lowercase letters and underscores for filenames (for example, `how_to_write_docs.md`).


4. **Encourage Feedback**
   - Include a “Feedback” or “How to Contribute” section to engage readers.

---

## Example Documentation Outline

```markdown
# How to Use Service X

## Overview
This document explains how to configure and use Service X effectively.

## Prerequisites
- Access to the system.
- Administrative privileges.

## Steps
1. **Step 1**: Do this.
2. **Step 2**: Do that.

## Example Configuration
```yaml
key: value
```

## Troubleshooting
Refer to the [Troubleshooting Guide](troubleshooting.md) for common issues.


## Conclusion

By understanding the types of documentation and following this guide, you can create clear, actionable, and high-quality content that supports our organizational goals. Remember to keep your content concise, well-structured, and aligned with the type of document you are writing.
