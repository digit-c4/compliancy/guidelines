# HOWTO: Prepare the Weekly Meeting with the Service Delivery Manager

This document outlines the steps to prepare for the weekly meeting with the Service Delivery Manager (SDM). The purpose of this meeting is to review operational priorities, analyze team performance, and align on the roadmap for upcoming iterations.

---

## Goals of the Meeting

1. **Review Event Management SOP for NOCA:**
   - Identify updated SOPs from the previous week.
   - Ensure NOCA (Network Operating Center Agents) are progressing towards operational autonomy.

2. **Analyze ServiceNow Reports:**
   - Review incidents/requests in **Priority 1** and **Priority 2**.
   - Discuss incidents/requests currently in **HOLD** status.
   - Evaluate incidents/requests escalated from **Level 1 (NOCA)** to **Level 2 (NNDOE)**.

3. **Assess ITERATION Workload Distribution:**
   - Examine workload distribution across:
     - **Projects**
     - **Operations**
     - **Compliance Tasks**

4. **Review Automation KPIs:**
    - Number of Ansible (playbooks, role, collections) in GITLAB vs Vworker-dev
    - Number of CI implemented
    - Number of CD implemented
    - Number of AWX implemented

5. **Conclude and Update the Roadmap:**
   - Determine outcomes from the review.
   - Update the roadmap to reflect priorities for the next iterations.

---

## Preparation Steps

### 1. Review Updated SOPs
- **Responsibility:** NOCA squad.
- **Action Items:**
  - Compile a list of all **Event Management SOPs** updated in the past week.
  - Highlight key updates or new additions.
  - Be ready to discuss how these updates enhance NOCA’s autonomy.

---

### 2. Extract ServiceNow Reports
- **Responsibility:** Proxy Product Team.
- **Action Items:**
  - Generate ServiceNow reports for:
    - **Priority 1 and Priority 2 incidents/requests.**
    - **HOLD status incidents/requests.**
    - **Escalated incidents/requests from Level 1 to Level 2.**
  - Summarize findings:
    - Total number of items in each category.
    - Any recurring issues or bottlenecks.
    - Resolution time trends.

---

### 3. Analyze Workload Distribution
- **Responsibility:** Proxy Product Team with input from squad leads.
- **Action Items:**
  - Categorize team efforts into:
    - **Projects:** Major initiatives requiring focused project delivery.
    - **Operations:** Day-to-day tasks, including request, incident, monitoring and troubleshooting.
    - **Compliance:** Activities to meet security, regulatory, or organizational standards.
  - Identify workload imbalances or areas needing additional support.

---

### 4. Review Automation KPIs:**
- **Responsibility:** Proxy Product Team.
- **Action Items:**
    - **Number of Ansible:** figures of playbooks, role, collections published in GITLAB compare to the remaining numbers in stored in vworker-dev.
    - **Number of CI implemented:**  Number of projects with CI pipelines implemented to racks the adoption of CI across projects.
    - **Number of CD implemented:** Number of projects with CD pipelines implemented to racks the adoption of CD across projects.
    - **Number of AWX implemented:** Number of projects with AWX templates implemented to racks the adoption of AWX across projects.

### 5. Prepare Conclusions and Recommendations
- **Responsibility:** Proxy Product Team.
- **Action Items:**
  - Draft actionable conclusions based on the review of SOPs, reports, and workload analysis.
  - Propose updates to the **team roadmap** for upcoming iterations.
  - Highlight dependencies, risks, or resource needs that impact roadmap execution.

---

## During the Meeting

1. **Start with SOP Review:**
   - Present the updated SOPs.
   - Discuss any challenges or feedback from NOCA.

2. **ServiceNow Report Analysis:**
   - Share key insights from the reports.
   - Discuss actions for unresolved Priority 1 and 2 items.
   - Propose resolutions for HOLD status or escalated items.

3. **Workload Distribution:**
   - Present the breakdown of efforts across Projects, Operations, and Compliance.
   - Identify areas needing additional focus or reallocation of resources.

4. **Conclude and Update the Roadmap:**
   - Agree on roadmap updates based on findings.
   - Define key objectives and deliverables for the next iteration.

---

## Follow-Up Actions

1. **Document Meeting Outcomes:**
   - Summarize decisions, assigned actions, and updated roadmap items.
   - Share the meeting notes with the team.

2. **Track Progress:**
   - Use the updated roadmap to guide the next iteration's planning and execution.
   - Review outstanding actions in the following meeting.

---