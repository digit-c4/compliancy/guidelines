# Contributing Guidelines

Thank you for your interest in contributing to the IT Operations and Compliance Guidelines repository! Your contributions help us maintain a robust and practical resource for our teams.

---

## How to Contribute

### 1. Create an Issue
- If you have identified a gap, an improvement, or any other suggestion:
  1. Go to the **Issues** tab.
  2. Create a new issue with a clear title and description.
  3. Tag the appropriate labels (e.g., "bug," "enhancement," "documentation").

### 2. Create a Merge Request (MR)
- If you have a solution or content to propose:
  1. Create a branch for your changes.
  2. Add or modify content in your branch.
  3. Submit a **Merge Request (MR)** with a detailed explanation of your changes.
  4. Notify relevant stakeholders by tagging them in the MR.

---

## Review and Approval Process

1. All contributions are reviewed by one of the following:
   - Compliance Squad
   - Operation Quality Manager
   - Service Delivery Manager

2. Reviewers will:
   - Check for alignment with ITIL processes, security policies, and BCDR principles.
   - Ensure adherence to the repository’s vision and objectives.
   - Provide feedback for any necessary revisions.

3. Approved changes will be merged into the repository by the reviewer.

## Release Management

1. Use a consistent versioning scheme, typically Semantic Versioning (SemVer):

MAJOR.MINOR.PATCH
  - MAJOR: Incompatible changes or significant new functionality.
  - MINOR: Backward-compatible features or improvements.
  - PATCH: Bug fixes or small updates.

Examples:

  - 1.0.0: Initial release.
  - 1.1.0: New HOW TO, Guidelines.
  - 1.1.1: Content fixes.
 
---

## Contribution Tips

- Follow a **practical and actionable tone** when writing guidelines.
- Use clear and concise language to make content easy to understand.
- Provide examples or references where possible.
- Ensure your changes do not conflict with the overall vision and objectives of the repository.

---

## Governance

### Permissions
- While **all squad members** are encouraged to contribute via issues or MRs, only the following roles can approve and merge changes:
  - Compliance Squad
  - Operation Quality Manager
  - Service Delivery Manager

### Expectations
- Contributions should be well-documented and aligned with our strategic goals.
- Collaborate with other team members to ensure consistency and clarity.

---

## Thank You

Your efforts to improve this repository are deeply appreciated. Together, we can maintain a valuable resource for driving compliance and operational excellence. If you have any questions about contributing, please reach out to one of the reviewers.

Happy contributing!

---